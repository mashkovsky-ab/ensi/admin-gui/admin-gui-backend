<?php

namespace Tests\Helpers\Catalog;

use Ensi\PimClient\Dto\PropertyTypeEnum;
use Faker\Generator;
use Illuminate\Support\Facades\Date;

class PropertyValueGenerator
{
    public function __construct(private Generator $faker, private string $type)
    {
    }

    public static function new(Generator $faker, ?string $type = null): self
    {
        return new self($faker, $type ?? $faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()));
    }

    public function type(): string
    {
        return $this->type;
    }

    public function value(): string
    {
        return match ($this->type) {
            PropertyTypeEnum::STRING => $this->faker->word,
            PropertyTypeEnum::BOOLEAN => $this->faker->boolean ? 'true' : 'false',
            PropertyTypeEnum::COLOR => '#' . dechex($this->faker->numberBetween(0x100000, 0xFFFFFF)),
            PropertyTypeEnum::DATETIME => Date::make($this->faker->dateTime)->toJSON(),
            PropertyTypeEnum::DOUBLE => (string)$this->faker->randomFloat(4),
            PropertyTypeEnum::INTEGER => (string)$this->faker->randomNumber(),
            PropertyTypeEnum::IMAGE => '/var/www/public/image_' . $this->faker->numberBetween(1, 1000) . '.jpg',
        };
    }
}
