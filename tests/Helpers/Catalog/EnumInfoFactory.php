<?php

namespace Tests\Helpers\Catalog;

use App\Http\ApiV1\Support\Tests\Factories\PromiseFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Ensi\PimClient\Dto\StringEnumInfo;
use Ensi\PimClient\Dto\StringEnumInfoResponse;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Foundation\Testing\WithFaker;

class EnumInfoFactory
{
    use WithFaker;

    public function __construct(private array $keys, private string $itemClass, private string $responseClass)
    {
        $this->setUpFaker();
    }

    public static function propertyTypes(): self
    {
        return new self(
            PropertyTypeEnum::getAllowableEnumValues(),
            StringEnumInfo::class,
            StringEnumInfoResponse::class
        );
    }

    public function make(): array
    {
        $items = [];
        foreach ($this->keys as $id) {
            $items[] = new ($this->itemClass)(['id' => $id, 'name' => $this->faker->sentence(2)]);
        }

        return $items;
    }

    public function makeResponse()
    {
        return new ($this->responseClass)(['data' => $this->make()]);
    }

    public function makeAsyncResponse(): PromiseInterface
    {
        return PromiseFactory::make($this->makeResponse());
    }
}
