<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\EditProductAttributeValue;
use Ensi\PimClient\Dto\PatchProductAttributesRequest;

class PatchAttributeValuesAction
{
    public function __construct(private ProductsApi $api)
    {
    }

    public function execute(int $productId, array $attributes): array
    {
        $attributes['values'] = collect($attributes['values'] ?? [])
            ->mapInto(EditProductAttributeValue::class)
            ->all();

        $request = new PatchProductAttributesRequest($attributes);

        return $this->api->patchProductAttributes($productId, $request)->getData();
    }
}
