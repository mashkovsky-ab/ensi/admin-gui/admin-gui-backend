<?php

namespace App\Domain\Catalog\Actions\Products;

use App\Domain\Catalog\Actions\Offers\SetDefaultOfferDataAction;
use App\Domain\Catalog\Data\Products\ProductData;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\CreateProductRequest;

class CreateProductAction
{
    public function __construct(
        private readonly ProductsApi $api,
        private readonly SetDefaultOfferDataAction $offerAction
    ) {
    }

    public function execute(array $fields): ProductData
    {
        $request = new CreateProductRequest($fields);

        $product = $this->api->createProduct($request)->getData();

        $offer = $this->offerAction->execute($product->getId(), [
            'sale_status' => OfferSaleStatusEnum::ON_SALE,
            'base_price' => $fields['base_price'] ?? null,
        ]);

        return new ProductData($product, $offer);
    }
}
