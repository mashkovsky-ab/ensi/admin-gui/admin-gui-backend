<?php

namespace App\Domain\Catalog\Actions\Products;

use App\Domain\Catalog\Actions\Offers\SetDefaultOfferDataAction;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\PimClient\Api\ProductsApi;

class DeleteProductAction
{
    public function __construct(
        private readonly ProductsApi $api,
        private readonly SetDefaultOfferDataAction $offerAction
    ) {
    }

    public function execute(int $productId): void
    {
        $this->api->deleteProduct($productId);

        $this->offerAction->execute($productId, ['sale_status' => OfferSaleStatusEnum::OUT_SALE]);
    }
}
