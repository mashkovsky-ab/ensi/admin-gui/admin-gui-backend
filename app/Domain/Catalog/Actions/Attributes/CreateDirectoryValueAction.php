<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\CreateDirectoryValueRequest;
use Ensi\PimClient\Dto\DirectoryValue;

class CreateDirectoryValueAction
{
    public function __construct(private PropertiesApi $api)
    {
    }

    public function execute(int $propertyId, array $fields): DirectoryValue
    {
        $request = new CreateDirectoryValueRequest($fields);

        return $this->api->createDirectoryValue($propertyId, $request)->getData();
    }
}
