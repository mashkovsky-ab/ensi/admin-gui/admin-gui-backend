<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\DirectoryValue;
use Ensi\PimClient\Dto\ReplaceDirectoryValueRequest;

class ReplaceDirectoryValueAction
{
    public function __construct(private PropertiesApi $api)
    {
    }

    public function execute(int $directoryValueId, array $fields): DirectoryValue
    {
        $request = new ReplaceDirectoryValueRequest($fields);

        return $this->api->replaceDirectoryValue($directoryValueId, $request)->getData();
    }
}
