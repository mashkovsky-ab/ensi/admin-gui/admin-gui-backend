<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;

class DeleteProductAttributeAction
{
    public function __construct(private PropertiesApi $api)
    {
    }

    public function execute(int $attributeId): void
    {
        $this->api->deleteProperty($attributeId);
    }
}
