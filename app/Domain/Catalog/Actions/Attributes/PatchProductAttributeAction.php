<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\PatchPropertyRequest;
use Ensi\PimClient\Dto\Property;

class PatchProductAttributeAction
{
    public function __construct(private PropertiesApi $api)
    {
    }

    public function execute(int $attributeId, array $fields): Property
    {
        $request = new PatchPropertyRequest($fields);

        return $this->api->patchProperty($attributeId, $request)->getData();
    }
}
