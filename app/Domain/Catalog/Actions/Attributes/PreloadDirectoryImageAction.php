<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\PreloadFileData;
use Illuminate\Http\UploadedFile;

class PreloadDirectoryImageAction
{
    public function __construct(private readonly PropertiesApi $api)
    {
    }

    public function execute(UploadedFile $file): PreloadFileData
    {
        return $this->api->preloadDirectoryValueImage($file)->getData();
    }
}
