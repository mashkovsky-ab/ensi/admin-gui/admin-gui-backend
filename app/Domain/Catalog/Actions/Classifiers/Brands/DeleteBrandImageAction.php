<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Brand;

class DeleteBrandImageAction
{
    public function __construct(
        private BrandsApi $brandsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $brandId): Brand
    {
        return $this->brandsApi->deleteBrandImage($brandId)->getData();
    }
}
