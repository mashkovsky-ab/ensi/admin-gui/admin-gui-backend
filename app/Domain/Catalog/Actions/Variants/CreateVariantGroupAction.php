<?php

namespace App\Domain\Catalog\Actions\Variants;

use Ensi\PimClient\Api\VariantsApi;
use Ensi\PimClient\Dto\CreateVariantGroupRequest;
use Ensi\PimClient\Dto\VariantGroup;

class CreateVariantGroupAction
{
    public function __construct(private VariantsApi $api)
    {
    }

    public function execute(array $fields): VariantGroup
    {
        $request = new CreateVariantGroupRequest($fields);

        return $this->api->createVariantGroup($request)->getData();
    }
}
