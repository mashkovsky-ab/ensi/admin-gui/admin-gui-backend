<?php

namespace App\Domain\Catalog\Actions\Variants;

use Ensi\PimClient\Api\VariantsApi;

class DeleteVariantGroupAction
{
    public function __construct(private VariantsApi $api)
    {
    }

    public function execute(int $groupId): void
    {
        $this->api->deleteVariantGroup($groupId);
    }
}
