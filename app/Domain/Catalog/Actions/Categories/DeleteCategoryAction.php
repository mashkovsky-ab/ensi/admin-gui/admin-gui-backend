<?php

namespace App\Domain\Catalog\Actions\Categories;

use Ensi\PimClient\Api\CategoriesApi;

class DeleteCategoryAction
{
    public function __construct(private CategoriesApi $api)
    {
    }

    public function execute(int $categoryId): void
    {
        $this->api->deleteCategory($categoryId);
    }
}
