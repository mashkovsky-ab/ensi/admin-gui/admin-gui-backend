<?php

namespace App\Domain\Catalog\Actions\Categories;

use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Dto\BindCategoryPropertiesRequest;
use Ensi\PimClient\Dto\Category;

class BindCategoryPropertiesAction
{
    public function __construct(private CategoriesApi $api)
    {
    }

    public function execute(int $categoryId, array $data): Category
    {
        $request = new BindCategoryPropertiesRequest($data);

        return $this->api->bindCategoryProperties($categoryId, $request)->getData();
    }
}
