<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\PatchOfferRequest;

class PatchOfferAction
{
    public function __construct(private readonly OffersApi $api)
    {
    }

    public function execute(int $offerId, array $fields): Offer
    {
        $request = new PatchOfferRequest($fields);

        return $this->api->patchOffer($offerId, $request)->getData();
    }
}
