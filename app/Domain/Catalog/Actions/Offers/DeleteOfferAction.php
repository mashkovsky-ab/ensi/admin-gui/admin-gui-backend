<?php
namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;

class DeleteOfferAction
{
    public function __construct(private OffersApi $api)
    {
    }

    public function execute(int $offerId): void
    {
        $this->api->deleteOffer($offerId);
    }
}
