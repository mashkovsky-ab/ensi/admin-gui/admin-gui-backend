<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\CreateOfferRequest;
use Ensi\OffersClient\Dto\Offer;

class CreateOfferAction
{
    public function __construct(private OffersApi $api)
    {
    }

    public function execute(array $fields): Offer
    {
        $request = new CreateOfferRequest($fields);

        return $this->api->createOffer($request)->getData();
    }
}
