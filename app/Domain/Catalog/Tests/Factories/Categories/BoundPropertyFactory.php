<?php

namespace App\Domain\Catalog\Tests\Factories\Categories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\CategoryBoundProperty;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class BoundPropertyFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'property_id' => $this->requiredId(),
            'name' => $this->faker->word,
            'display_name' => $this->faker->word,
            'code' => $this->faker->slug(2),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'is_multiple' => $this->faker->boolean,
            'is_filterable' => $this->faker->boolean,
            'is_public' => $this->faker->boolean,
            'is_active' => true,
            'is_required' => $this->faker->boolean,
            'is_inherited' => $this->faker->boolean,
            'is_common' => $this->faker->boolean,
            'hint_value' => $this->faker->sentence,
            'hint_value_name' => $this->faker->sentence,
            'has_directory' => $this->faker->boolean,
            'is_system' => false,
            'is_moderated' => $this->faker->boolean,
        ];
    }

    public function make(array $extra = []): CategoryBoundProperty
    {
        return new CategoryBoundProperty($this->makeArray($extra));
    }
}
