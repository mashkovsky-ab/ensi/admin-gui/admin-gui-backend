<?php

namespace App\Domain\Catalog\Tests\Factories\Categories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\Property;
use Ensi\PimClient\Dto\PropertyResponse;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Ensi\PimClient\Dto\SearchPropertiesResponse;
use Illuminate\Support\Arr;

class PropertyFactory extends BaseApiFactory
{
    public array $directory = [];

    protected function definition(): array
    {
        return [
            'id' => $this->requiredId(),
            'name' => $this->faker->word,
            'display_name' => $this->faker->word,
            'code' => $this->faker->slug(2),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'is_system' => false,
            'is_common' => $this->faker->boolean,

            'is_multiple' => $this->faker->boolean,
            'is_filterable' => $this->faker->boolean,
            'is_public' => $this->faker->boolean,
            'is_active' => true,
            'is_required' => $this->faker->boolean,
            'is_moderated' => $this->faker->boolean,

            'hint_value' => $this->faker->sentence,
            'hint_value_name' => $this->faker->sentence,
            'has_directory' => $this->faker->boolean,

            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,

            'directory' => $this->executeNested($this->directory),
        ];
    }

    public function withDirectory(): self
    {
        return $this->immutableSet('directory', Arr::prepend($this->directory, DirectoryValueFactory::new()));
    }

    public function make(array $extra = []): Property
    {
        return new Property($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): PropertyResponse
    {
        return new PropertyResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchPropertiesResponse
    {
        return $this->generateResponseSearch(SearchPropertiesResponse::class, $extra, $count);
    }
}
