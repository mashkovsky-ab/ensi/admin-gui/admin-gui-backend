<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\ProductAttributesResponse;
use Ensi\PimClient\Dto\ProductAttributeValue;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Faker\Generator;
use Illuminate\Support\Facades\Date;

class ProductAttributeValueFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $type = $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues());

        return [
            'property_id' => $this->faker->randomNumber(),
            'value' => self::generateValue($type, $this->faker),
            'name' => $this->faker->optional->sentence,
            'type' => $type,
            'directory_value_id' => $this->faker->optional->numberBetween(1, 999),
        ];
    }

    public function make(array $extra = []): ProductAttributeValue
    {
        return new ProductAttributeValue($this->makeArray($extra));
    }

    public function makeResponse(int $count = 1, array $extra = []): ProductAttributesResponse
    {
        return new ProductAttributesResponse([
            'data' => $this->makeSeveral($count, $extra)->all(),
        ]);
    }

    public static function generateValue(string $type, Generator $faker): mixed
    {
        return match ($type) {
            PropertyTypeEnum::STRING => $faker->word,
            PropertyTypeEnum::BOOLEAN => $faker->boolean === true ? 'true' : 'false',
            PropertyTypeEnum::COLOR => '#' . dechex($faker->numberBetween(0x100000, 0xFFFFFF)),
            PropertyTypeEnum::DATETIME => Date::make($faker->dateTime)->toJSON(),
            PropertyTypeEnum::DOUBLE => $faker->randomFloat(4),
            PropertyTypeEnum::INTEGER => $faker->randomNumber(),
            PropertyTypeEnum::IMAGE => '/var/www/public/image_' . $faker->numberBetween(1, 1000) . '.jpg',
        };
    }
}
