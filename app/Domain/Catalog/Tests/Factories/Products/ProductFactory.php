<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductImage;
use Ensi\PimClient\Dto\ProductResponse;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\SearchProductsResponse;

class ProductFactory extends BaseApiFactory
{
    protected array $productImages = [];
    protected ?Category $category = null;
    protected ?Brand $brand = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->requiredId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'external_id' => $this->faker->uuid(),
            'category_id' => $this->faker->randomNumber(),
            'brand_id' => $this->faker->randomNumber(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->text(50),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'allow_publish' => $this->faker->boolean,
            'vendor_code' => $this->faker->numerify('######'),
            'barcode' => $this->faker->ean13(),

            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'is_adult' => $this->faker->boolean(),

            'main_image' => $this->faker->url,
        ];

        if ($this->productImages) {
            $definition['images'] = $this->productImages;
        }
        if ($this->category) {
            $definition['category'] = $this->category;
        }
        if ($this->brand) {
            $definition['brand'] = $this->brand;
        }

        return $definition;
    }

    public function withImages(?ProductImage $productImage = null): self
    {
        $this->productImages[] = $productImage ?: ProductImageFactory::new()->make();

        return $this;
    }

    public function withCategory(?Category $category = null): self
    {
        $this->category = $category ?: CategoryFactory::new()->make();

        return $this;
    }

    public function withBrand(?Brand $brand = null): self
    {
        $this->brand = $brand ?: BrandFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): ProductResponse
    {
        return new ProductResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchProductsResponse
    {
        return $this->generateResponseSearch(SearchProductsResponse::class, $extra, $count);
    }
}
