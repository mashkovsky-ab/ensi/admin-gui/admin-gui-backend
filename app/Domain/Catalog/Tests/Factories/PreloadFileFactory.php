<?php

namespace App\Domain\Catalog\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\PreloadFile;
use Ensi\PimClient\Dto\PreloadFileData;

class PreloadFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'preload_file_id' => $this->faker->randomNumber(5),
            'url' => $this->faker->url,
        ];
    }

    public function make(array $extra = []): PreloadFile
    {
        $data = $this->makeArray($extra);

        return new PreloadFile(['data' => new PreloadFileData($data)]);
    }
}
