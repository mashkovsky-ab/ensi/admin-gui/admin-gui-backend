<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use App\Domain\Orders\Tests\Factories\Orders\Data\DeliveryAddressFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderFile;
use Ensi\OmsClient\Dto\OrderResponse;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\PaymentSystemEnum;
use Ensi\OmsClient\Dto\SearchOrdersResponse;

class OrderFactory extends BaseApiFactory
{
    protected array $files = [];
    protected array $deliveries = [];

    protected function definition(): array
    {
        $deliveryMethod = $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY;

        $price = $this->faker->numberBetween(1000, 10000);
        $deliveryPrice = $this->faker->numberBetween(0, 200);

        $definition = [
            'id' => $this->faker->randomNumber(),
            'number' => $this->faker->unique()->numerify('######'),
            'source' => $this->faker->randomElement(OrderSourceEnum::getAllowableEnumValues()),
            'customer_id' => $this->faker->randomNumber(),
            'responsible_id' => $this->faker->optional()->randomNumber(),
            'customer_email' => $this->faker->email(),
            'price' => $price,
            'cost' => $this->faker->numberBetween($price, $price + 10000),
            'spent_bonus' => $this->faker->numberBetween(0, 10),
            'added_bonus' => $this->faker->numberBetween(0, 10),
            'promo_code' => $this->faker->optional()->word(),
            'delivery_method' => $deliveryMethod,
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'delivery_tariff_id' => $this->faker->randomNumber(),
            'delivery_point_id' => $isDelivery ? null : $this->faker->randomNumber(),
            'delivery_address' => $isDelivery ? DeliveryAddressFactory::new()->make() : null,
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
            'receiver_name' => $this->faker->name(),
            'receiver_phone' => $this->faker->numerify('+7##########'),
            'receiver_email' => $this->faker->email(),
            'status' => $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'payment_status' => $this->faker->randomElement(PaymentStatusEnum::getAllowableEnumValues()),
            'payment_status_at' => $this->faker->dateTime(),
            'payed_at' => $this->faker->optional()->dateTime(),
            'payment_expires_at' => $this->faker->optional()->dateTime(),
            'payment_method' => $this->faker->randomElement(PaymentMethodEnum::getAllowableEnumValues()),
            'payment_system' => $this->faker->randomElement(PaymentSystemEnum::getAllowableEnumValues()),
            'payment_link' => $this->faker->optional()->url(),
            'payment_external_id' => $this->faker->optional()->uuid(),
            'is_expired' => $this->faker->boolean(),
            'is_expired_at' => $this->faker->optional()->dateTime(),
            'is_return' => $this->faker->boolean(),
            'is_return_at' => $this->faker->optional()->dateTime(),
            'is_partial_return' => $this->faker->boolean(),
            'is_partial_return_at' => $this->faker->optional()->dateTime(),
            'is_problem' => $this->faker->boolean(),
            'is_problem_at' => $this->faker->optional()->dateTime(),
            'problem_comment' => $this->faker->optional()->text(50),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
            'delivery_comment' => $this->faker->optional()->text(50),
            'client_comment' => $this->faker->optional()->text(50),
        ];

        if ($this->files) {
            $definition['files'] = $this->files;
        }
        if ($this->deliveries) {
            $definition['deliveries'] = $this->deliveries;
        }

        return $definition;
    }

    public function withFile(?OrderFile $file = null): self
    {
        $this->files[] = $file ?: OrderFileFactory::new()->make();

        return $this;
    }

    public function withDelivery(?Delivery $delivery = null): self
    {
        $this->deliveries[] = $delivery ?: DeliveryFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Order
    {
        return new Order($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): OrderResponse
    {
        return new OrderResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchOrdersResponse
    {
        return $this->generateResponseSearch(SearchOrdersResponse::class, $extra, $count);
    }
}
