<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderItem;

class OrderItemFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $pricePerOne = $this->faker->numberBetween(1, 10000);
        $costPerOne = $this->faker->numberBetween($pricePerOne, $pricePerOne + 1000);

        $qty = $this->faker->randomFloat(2, 1, 100);

        return [
            'id' => $this->faker->randomNumber(),
            'order_id' => $this->faker->randomNumber(),
            'offer_id' => $this->faker->randomNumber(),
            'shipment_id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(50),
            'qty' => $qty,
            'price' => round($pricePerOne * $qty),
            'price_per_one' => $pricePerOne,
            'cost' => round($costPerOne * $qty),
            'cost_per_one' => $costPerOne,
            'refund_qty' => $this->faker->randomFloat(2, 0, $qty),
            'product_weight' => $this->faker->numberBetween(1, 100),
            'product_weight_gross' => $this->faker->numberBetween(1, 100),
            'product_width' => $this->faker->numberBetween(1, 100),
            'product_height' => $this->faker->numberBetween(1, 100),
            'product_length' => $this->faker->numberBetween(1, 100),
            'product_barcode' => $this->faker->optional()->numerify('############'),
            'offer_external_id' => $this->faker->numerify('###-###'),
            'offer_storage_address' => $this->faker->optional()->numerify('###'),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): OrderItem
    {
        return new OrderItem($this->makeArray($extra));
    }
}
