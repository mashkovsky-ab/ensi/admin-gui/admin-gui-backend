<?php

namespace App\Domain\Orders\Tests\Factories\Orders\Enums;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\ShipmentStatus;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;
use Ensi\OmsClient\Dto\ShipmentStatusesResponse;

class ShipmentStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(ShipmentStatusEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): ShipmentStatus
    {
        return new ShipmentStatus($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): ShipmentStatusesResponse
    {
        return new ShipmentStatusesResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
