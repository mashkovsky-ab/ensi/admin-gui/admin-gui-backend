<?php

namespace App\Domain\Orders\Tests\Factories\Orders\Enums;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderStatus;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\OrderStatusesResponse;

class OrderStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): OrderStatus
    {
        return new OrderStatus($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): OrderStatusesResponse
    {
        return new OrderStatusesResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
