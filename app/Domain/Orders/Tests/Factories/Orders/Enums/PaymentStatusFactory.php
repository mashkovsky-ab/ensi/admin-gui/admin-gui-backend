<?php

namespace App\Domain\Orders\Tests\Factories\Orders\Enums;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\PaymentStatus;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\PaymentStatusesResponse;

class PaymentStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(PaymentStatusEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): PaymentStatus
    {
        return new PaymentStatus($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): PaymentStatusesResponse
    {
        return new PaymentStatusesResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
