<?php

namespace App\Domain\Orders\Tests\Factories\Orders\Enums;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\PaymentMethod;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentMethodsResponse;

class PaymentMethodFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(PaymentMethodEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): PaymentMethod
    {
        return new PaymentMethod($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): PaymentMethodsResponse
    {
        return new PaymentMethodsResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
