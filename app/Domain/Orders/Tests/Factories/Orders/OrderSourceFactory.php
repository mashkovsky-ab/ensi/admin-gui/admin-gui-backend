<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderSource;
use Ensi\OmsClient\Dto\OrderSourcesResponse;

class OrderSourceFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): OrderSource
    {
        return new OrderSource($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): OrderSourcesResponse
    {
        return new OrderSourcesResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
