<?php

namespace App\Domain\Orders\Tests\Factories\BasketsCommon;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\BasketsClient\Dto\Setting;
use Ensi\BasketsClient\Dto\SettingsResponse;

class SettingFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'code' => $this->faker->unique()->text(50),
            'name' => $this->faker->text(50),
            'value' => $this->faker->numerify('##'),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Setting
    {
        return new Setting($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): SettingsResponse
    {
        return new SettingsResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
