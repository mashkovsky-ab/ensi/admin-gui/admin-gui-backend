<?php

namespace App\Domain\Orders\Tests\Factories\Refunds;

use App\Domain\Orders\Tests\Factories\Orders\OrderFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderItemFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\Refund;
use Ensi\OmsClient\Dto\RefundFile;
use Ensi\OmsClient\Dto\RefundReason;
use Ensi\OmsClient\Dto\RefundResponse;
use Ensi\OmsClient\Dto\RefundStatusEnum;
use Ensi\OmsClient\Dto\SearchRefundsResponse;

class RefundFactory extends BaseApiFactory
{
    protected ?Order $order = null;
    protected array $orderItems = [];
    protected array $reasons = [];
    protected array $files = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'order_id' => $this->faker->randomNumber(),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'responsible_id' => $this->faker->optional()->randomNumber(),
            'source' => $this->faker->randomElement(OrderSourceEnum::getAllowableEnumValues()),
            'status' => $this->faker->randomElement(RefundStatusEnum::getAllowableEnumValues()),
            'price' => $this->faker->numberBetween(1000, 10000),
            'is_partial' => $this->faker->boolean(),
            'user_comment' => $this->faker->text(50),
            'rejection_comment' => $this->faker->optional()->text(50),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->order) {
            $definition['order'] = $this->order;
        }
        if ($this->orderItems) {
            $definition['items'] = $this->orderItems;
        }
        if ($this->reasons) {
            $definition['reasons'] = $this->reasons;
        }
        if ($this->files) {
            $definition['files'] = $this->files;
        }

        return $definition;
    }

    public function withOrder(?Order $order = null): self
    {
        $this->order = $order ?: OrderFactory::new()->make();

        return $this;
    }

    public function withItem(?OrderItem $orderItem = null): self
    {
        $this->orderItems[] = $orderItem ?: OrderItemFactory::new()->make();

        return $this;
    }

    public function withReason(?RefundReason $reason = null): self
    {
        $this->reasons[] = $reason ?: RefundReasonFactory::new()->make();

        return $this;
    }

    public function withFile(?RefundFile $file = null): self
    {
        $this->files[] = $file ?: RefundFileFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Refund
    {
        return new Refund($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchRefundsResponse
    {
        return $this->generateResponseSearch(SearchRefundsResponse::class, $extra, $count);
    }

    public function makeResponseOne(array $extra = []): RefundResponse
    {
        return new RefundResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }
}
