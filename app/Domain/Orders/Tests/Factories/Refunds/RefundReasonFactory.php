<?php

namespace App\Domain\Orders\Tests\Factories\Refunds;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\RefundReason;
use Ensi\OmsClient\Dto\RefundReasonsResponse;
use Ensi\OmsClient\Dto\RefundResponse;

class RefundReasonFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'code' => $this->faker->text(10),
            'name' => $this->faker->text(10),
            'description' => $this->faker->optional()->text(50),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): RefundReason
    {
        return new RefundReason($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): RefundResponse
    {
        return new RefundResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }

    public function makeResponseSearch(array $extra = []): RefundReasonsResponse
    {
        return new RefundReasonsResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
