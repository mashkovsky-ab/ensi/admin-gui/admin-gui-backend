<?php


namespace App\Domain\Orders\Actions\Refunds;

use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\RefundFile;
use Illuminate\Http\UploadedFile;

class AttachRefundFileAction
{
    public function __construct(protected RefundsApi $refundsApi)
    {
    }

    public function execute(int $id, UploadedFile $file): RefundFile
    {
        return $this->refundsApi->attachRefundFile($id, $file)->getData();
    }
}
