<?php

namespace App\Domain\Orders\Actions\Refunds;

use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\RefundReason;
use Ensi\OmsClient\Dto\RefundReasonForCreate;
use Ensi\PimClient\ApiException;

class CreateRefundReasonAction
{
    public function __construct(
        private EnumsApi $enumsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): RefundReason
    {
        $request = new RefundReasonForCreate($fields);

        return $this->enumsApi->createRefundReason($request)->getData();
    }
}
