<?php

namespace App\Domain\Orders\Actions\Refunds;

use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\RefundReason;
use Ensi\OmsClient\Dto\RefundReasonForPatch;
use Ensi\PimClient\ApiException;

class PatchRefundReasonAction
{
    public function __construct(
        private EnumsApi $enumsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): RefundReason
    {
        $request = new RefundReasonForPatch($fields);

        return $this->enumsApi->patchRefundReason($id, $request)->getData();
    }
}
