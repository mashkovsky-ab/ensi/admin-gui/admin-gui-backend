<?php

namespace App\Domain\Orders\Actions\Refunds;

use App\Domain\Orders\Data\Refunds\RefundData;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\RefundForPatch;
use Ensi\PimClient\ApiException;

class PatchRefundAction
{
    public function __construct(
        private RefundsApi $refundsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): RefundData
    {
        $request = new RefundForPatch($fields);

        $refund = $this->refundsApi->patchRefund($id, $request)->getData();

        return new RefundData($refund);
    }
}
