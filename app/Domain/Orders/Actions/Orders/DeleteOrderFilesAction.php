<?php


namespace App\Domain\Orders\Actions\Orders;

use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderDeleteFilesRequest;

class DeleteOrderFilesAction
{
    public function __construct(protected OrdersApi $ordersApi)
    {
    }

    public function execute(int $orderId, array $fileIds): void
    {
        $this->ordersApi->deleteOrderFiles($orderId, (new OrderDeleteFilesRequest())->setFileIds($fileIds));
    }
}
