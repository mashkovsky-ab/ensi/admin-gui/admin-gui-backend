<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderChangePaymentSystemRequest;

class ChangeOrderPaymentSystemAction
{
    public function __construct(protected OrdersApi $ordersApi)
    {
    }

    public function execute(int $orderId, int $paymentSystem): OrderData
    {
        $order = $this->ordersApi
            ->changeOrderPaymentSystem($orderId, (new OrderChangePaymentSystemRequest())->setPaymentSystem($paymentSystem))
            ->getData();

        return new OrderData($order);
    }
}
