<?php


namespace App\Domain\Orders\Actions\Orders;

use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderFile;
use Illuminate\Http\UploadedFile;

class AttachOrderFileAction
{
    public function __construct(protected OrdersApi $ordersApi)
    {
    }

    public function execute(int $orderId, UploadedFile $file): OrderFile
    {
        return $this->ordersApi->attachOrderFile($orderId, $file)->getData();
    }
}
