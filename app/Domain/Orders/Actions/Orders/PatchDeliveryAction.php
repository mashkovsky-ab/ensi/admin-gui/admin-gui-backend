<?php


namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\DeliveryData;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Dto\DeliveryForPatch;

class PatchDeliveryAction
{
    public function __construct(protected DeliveriesApi $deliveriesApi)
    {
    }

    public function execute(int $deliveryId, array $fields): DeliveryData
    {
        $delivery = $this->deliveriesApi->patchDelivery($deliveryId, new DeliveryForPatch($fields))->getData();

        return new DeliveryData($delivery);
    }
}
