<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderDeliveryForPatch;

class ChangeOrderDeliveryAction
{
    public function __construct(protected OrdersApi $ordersApi)
    {
    }

    public function execute(int $orderId, array $fields): OrderData
    {
        $order = $this->ordersApi->changeOrderDelivery($orderId, new OrderDeliveryForPatch($fields))->getData();

        return new OrderData($order);
    }
}
