<?php


namespace App\Domain\Orders\Actions\OmsCommon;

use Ensi\OmsClient\Api\CommonApi;
use Ensi\OmsClient\Dto\PatchSeveralSettingsRequest;

class PatchSeveralSettingsAction
{
    public function __construct(protected CommonApi $commonApi)
    {
    }

    public function execute(array $fields): array
    {
        return $this->commonApi->patchSettings(new PatchSeveralSettingsRequest($fields))->getData();
    }
}
