<?php


namespace App\Domain\Orders\Data\Orders;

use App\Domain\Catalog\Data\Offers\OfferData;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\PimClient\Dto\Product;

class OrderItemData
{
    public ?OfferData $offerData = null;
    public ?Product $product = null;

    public function __construct(public OrderItem $orderItem)
    {
    }
}
