<?php


namespace App\Domain\Orders\Data\Orders;

use App\Domain\Catalog\Data\Offers\OfferData;
use App\Domain\Customers\Data\CustomerData;
use App\Domain\Units\Data\SellerData;
use Ensi\AdminAuthClient\Dto\User as AdminUser;
use Ensi\BuClient\Dto\Store;
use Ensi\OmsClient\Dto\Order;
use Ensi\PimClient\Dto\Product;

class OrderData
{
    public ?CustomerData $customer = null;
    public ?AdminUser $responsible = null;

    /** @var OfferData[] */
    public array $offersData = [];
    /** @var Product[] */
    public array $products = [];
    /** @var SellerData[] */
    public array $sellersData = [];
    /** @var Store[] */
    public array $stores = [];

    public function __construct(public Order $order)
    {
    }

    /**
     * @return DeliveryData[]|null
     */
    public function getDeliveries(): ?array
    {
        if (is_null($this->order->getDeliveries())) {
            return null;
        }
        $deliveriesData = [];
        foreach ($this->order->getDeliveries() as $delivery) {
            $deliveryData = new DeliveryData($delivery);
            $deliveryData->offersData = $this->offersData;
            $deliveryData->products = $this->products;
            $deliveryData->sellersData = $this->sellersData;
            $deliveryData->stores = $this->stores;
            $deliveriesData[] = $deliveryData;
        }

        return $deliveriesData;
    }
}
