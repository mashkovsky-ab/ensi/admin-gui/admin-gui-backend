<?php


namespace App\Domain\Orders\Data\Orders;

use App\Domain\Catalog\Data\Offers\OfferData;
use App\Domain\Units\Data\SellerData;
use Ensi\BuClient\Dto\Store;
use Ensi\OmsClient\Dto\Shipment;
use Ensi\PimClient\Dto\Product;

class ShipmentData
{
    /** @var OfferData[] */
    public array $offersData = [];
    /** @var Product[] */
    public array $products = [];
    public ?SellerData $sellerData = null;
    public ?Store $store = null;

    public function __construct(public Shipment $shipment)
    {
    }

    public function getDelivery(): ?DeliveryData
    {
        if (!$this->shipment->getDelivery()) {
            return null;
        }

        $deliveryData = new DeliveryData($this->shipment->getDelivery());
        $deliveryData->offersData = $this->offersData;
        $deliveryData->products = $this->products;

        return $deliveryData;
    }

    /**
     * @return OrderItemData[]|null
     */
    public function getOrderItems(): ?array
    {
        if (is_null($this->shipment->getOrderItems())) {
            return null;
        }
        $orderItemsData = [];
        $offersData = collect($this->offersData)->keyBy('offer.id');
        $products = collect($this->products)->keyBy('id');
        foreach ($this->shipment->getOrderItems() as $orderItem) {
            $orderItemData = new OrderItemData($orderItem);

            $orderItemData->offerData = $offersData->get($orderItem->getOfferId());
            if ($orderItemData->offerData) {
                $orderItemData->product = $products->get($orderItemData->offerData->offer->getProductId());
            }

            $orderItemsData[] = $orderItemData;
        }

        return $orderItemsData;
    }
}
