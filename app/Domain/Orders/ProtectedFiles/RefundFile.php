<?php


namespace App\Domain\Orders\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFile;
use Ensi\OmsClient\Dto\RefundFile as OmsRefundFile;

class RefundFile extends ProtectedFile
{
    public static function entity(): string
    {
        return 'oms/refund-file';
    }

    public static function createFromModel(OmsRefundFile $refundFile): static
    {
        $object = new static();
        $object->entity_id = $refundFile->getRefundId();
        $object->file = $refundFile->getId();

        return $object;
    }
}
