<?php


namespace App\Domain\Orders\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFileLoader;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderFileLoader extends ProtectedFileLoader
{
    public function __construct(protected OrdersApi $ordersApi)
    {
    }

    public function getRootPath(): ?string
    {
        $order = $this->loadOrder();

        // Сейчас любой пользователь может просматривать инфо по файлу в админке, далее нужна будет проверка по роли/доступу к чату в соответствии с ФЗ

        foreach ($order->getFiles() as $file) {
            if ($file->getId() == $this->file->file) {
                return $file->getFile()->getRootPath();
            }
        }

        return null;
    }

    protected function loadOrder(): Order
    {
        $request = new SearchOrdersRequest();
        $request->setFilter((object)['id' => $this->file->entity_id]);
        $request->setInclude(['files']);
        $orders = $this->ordersApi->searchOrders($request)->getData();
        if ($orders) {
            return current($orders);
        }

        throw new ModelNotFoundException();
    }
}
