<?php

namespace App\Domain\Logistic\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFileLoader;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryServiceDocument;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsFilter;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest;

class DeliveryServiceDocumentFileLoader extends ProtectedFileLoader
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function getRootPath(): ?string
    {
        $deliveryServiceDocument = $this->loadDeliveryServiceDocument();

        $file = $deliveryServiceDocument->getFile();
        if ($file && $file->getPath() == $this->file->file) {
            return $file->getRootPath();
        }

        return null;
    }

    protected function loadDeliveryServiceDocument(): DeliveryServiceDocument
    {
        $request = new SearchDeliveryServiceDocumentsRequest();
        $request->setFilter((new SearchDeliveryServiceDocumentsFilter())->setId($this->file->entity_id));

        return $this->deliveryServicesApi->searchOneDeliveryServiceDocument($request)->getData();
    }
}
