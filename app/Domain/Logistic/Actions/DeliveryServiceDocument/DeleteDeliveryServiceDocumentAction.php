<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocument;

use Ensi\LogisticClient\Api\DeliveryServicesApi;

class DeleteDeliveryServiceDocumentAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceDocumentId)
    {
        $this->deliveryServicesApi->deleteDeliveryServiceDocument($deliveryServiceDocumentId);
    }
}
