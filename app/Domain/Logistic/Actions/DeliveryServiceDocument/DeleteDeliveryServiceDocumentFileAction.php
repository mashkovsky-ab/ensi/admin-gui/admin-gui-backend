<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocument;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;

class DeleteDeliveryServiceDocumentFileAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceDocumentId): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData(
            $this->deliveryServicesApi->deleteDeliveryServiceDocumentFile($deliveryServiceDocumentId)->getData()
        );
    }
}
