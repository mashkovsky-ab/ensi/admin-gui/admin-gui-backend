<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocument;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\ReplaceDeliveryServiceDocumentRequest;

class ReplaceDeliveryServiceDocumentAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceDocumentId, array $fields): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData($this->deliveryServicesApi->replaceDeliveryServiceDocument(
            $deliveryServiceDocumentId,
            new ReplaceDeliveryServiceDocumentRequest($fields)
        )->getData());
    }
}
