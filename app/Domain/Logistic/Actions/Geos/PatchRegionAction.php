<?php


namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\PatchRegionRequest;
use Ensi\LogisticClient\Dto\Region;

/**
 * Class PatchRegionAction
 * @package App\Domain\Logistic\Actions\Geos
 */
class PatchRegionAction
{
    protected GeosApi $geosApi;

    /**
     * PatchRegionAction constructor.
     * @param  GeosApi  $geosApi
     */
    public function __construct(GeosApi $geosApi)
    {
        $this->geosApi = $geosApi;
    }

    /**
     * @param  int  $regionId
     * @param  array  $fields
     * @return Region
     * @throws ApiException
     */
    public function execute(int $regionId, array $fields): Region
    {
        $request = new PatchRegionRequest();

        if (isset($fields['federal_district_id'])) {
            $request->setFederalDistrictId($fields['federal_district_id']);
        }
        if (isset($fields['name'])) {
            $request->setName($fields['name']);
        }
        if (isset($fields['guid'])) {
            $request->setGuid($fields['guid']);
        }

        return $this->geosApi->patchRegion($regionId, $request)->getData();
    }
}
