<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateRegionRequest;
use Ensi\LogisticClient\Dto\Region;

/**
 * Class CreateRegionAction
 * @package App\Domain\Logistic\Actions\Geos
 */
class CreateRegionAction
{
    protected GeosApi $geosApi;

    /**
     * CreateRegionAction constructor.
     * @param  GeosApi  $geosApi
     */
    public function __construct(GeosApi $geosApi)
    {
        $this->geosApi = $geosApi;
    }

    /**
     * @param  array  $fields
     * @return Region
     * @throws ApiException
     */
    public function execute(array $fields): Region
    {
        $request = new CreateRegionRequest();
        $request->setFederalDistrictId($fields['federal_district_id']);
        $request->setName($fields['name']);
        $request->setGuid($fields['guid']);

        return $this->geosApi->createRegion($request)->getData();
    }
}
