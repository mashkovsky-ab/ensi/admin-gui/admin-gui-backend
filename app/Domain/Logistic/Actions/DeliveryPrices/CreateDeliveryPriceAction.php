<?php

namespace App\Domain\Logistic\Actions\DeliveryPrices;

use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateDeliveryPriceRequest;
use Ensi\LogisticClient\Dto\DeliveryPrice;

/**
 * Class CreateDeliveryPriceAction
 * @package App\Domain\Logistic\Actions\DeliveryPrices
 */
class CreateDeliveryPriceAction
{
    public function __construct(protected DeliveryPricesApi $deliveryPricesApi)
    {
    }

    /**
     * @param  array  $fields
     * @return DeliveryPrice
     * @throws ApiException
     */
    public function execute(array $fields): DeliveryPrice
    {
        $request = new CreateDeliveryPriceRequest();
        $request->setFederalDistrictId($fields['federal_district_id']);
        $request->setRegionId($fields['region_id']);
        $request->setRegionGuid($fields['region_guid']);
        $request->setDeliveryService($fields['delivery_service']);
        $request->setDeliveryMethod($fields['delivery_method']);
        $request->setPrice($fields['price']);

        return $this->deliveryPricesApi->createDeliveryPrice($request)->getData();
    }
}
