<?php

namespace App\Domain\Logistic\Actions\DeliveryPrices;

use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryPrice;
use Ensi\LogisticClient\Dto\ReplaceDeliveryPriceRequest;

/**
 * Class ReplaceDeliveryPriceAction
 * @package App\Domain\Logistic\Actions\DeliveryPrices
 */
class ReplaceDeliveryPriceAction
{
    public function __construct(protected DeliveryPricesApi $deliveryPricesApi)
    {
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  array  $fields
     * @return DeliveryPrice
     * @throws ApiException
     */
    public function execute(int $deliveryPriceId, array $fields): DeliveryPrice
    {
        $request = new ReplaceDeliveryPriceRequest();

        $request->setFederalDistrictId($fields['federal_district_id']);
        $request->setRegionId($fields['region_id']);
        $request->setRegionGuid($fields['region_guid']);
        $request->setDeliveryService($fields['delivery_service']);
        $request->setDeliveryMethod($fields['delivery_method']);
        $request->setPrice($fields['price']);

        return $this->deliveryPricesApi->replaceDeliveryPrice($deliveryPriceId, $request)->getData();
    }
}
