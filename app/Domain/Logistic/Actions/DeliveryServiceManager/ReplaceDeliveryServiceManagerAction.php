<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceManager;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryServiceManager;
use Ensi\LogisticClient\Dto\ReplaceDeliveryServiceManagerRequest;

class ReplaceDeliveryServiceManagerAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceManagerId, array $fields): DeliveryServiceManager
    {
        return $this->deliveryServicesApi->replaceDeliveryServiceManager(
            $deliveryServiceManagerId,
            new ReplaceDeliveryServiceManagerRequest($fields)
        )->getData();
    }
}
