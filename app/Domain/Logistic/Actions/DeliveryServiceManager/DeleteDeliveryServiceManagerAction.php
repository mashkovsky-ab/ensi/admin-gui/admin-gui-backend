<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceManager;

use Ensi\LogisticClient\Api\DeliveryServicesApi;

class DeleteDeliveryServiceManagerAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceManagerId)
    {
        $this->deliveryServicesApi->deleteDeliveryServiceManager($deliveryServiceManagerId);
    }
}
