<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateDeliveryKpiCtRequest;
use Ensi\LogisticClient\Dto\DeliveryKpiCt;

/**
 * Class CreateDeliveryKpiCtAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class CreateDeliveryKpiCtAction
{
    /**
     * CreateDeliveryKpiCtAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param int $sellerId
     * @param array $fields
     * @return DeliveryKpiCt
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiCt
    {
        $request = new CreateDeliveryKpiCtRequest();
        $request->setCt($fields['ct']);

        return $this->kpiApi->createDeliveryKpiCt($sellerId, $request)->getData();
    }
}
