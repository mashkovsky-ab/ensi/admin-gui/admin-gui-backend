<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpi;
use Ensi\LogisticClient\Dto\ReplaceDeliveryKpiRequest;

/**
 * Class ReplaceDeliveryKpiAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class ReplaceDeliveryKpiAction
{
    /**
     * ReplaceDeliveryKpiAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param array $fields
     * @return DeliveryKpi
     * @throws ApiException
     */
    public function execute(array $fields): DeliveryKpi
    {
        $request = new ReplaceDeliveryKpiRequest($fields);

        return $this->kpiApi->replaceDeliveryKpi($request)->getData();
    }
}
