<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiPpt;
use Ensi\LogisticClient\Dto\ReplaceDeliveryKpiPptRequest;

/**
 * Class ReplaceDeliveryKpiPptAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class ReplaceDeliveryKpiPptAction
{
    /**
     * ReplaceDeliveryKpiPptAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param int $sellerId
     * @param array $fields
     * @return DeliveryKpiPpt
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiPpt
    {
        $request = new ReplaceDeliveryKpiPptRequest();
        $request->setPpt($fields['ppt']);

        return $this->kpiApi->replaceDeliveryKpiPpt($sellerId, $request)->getData();
    }
}
