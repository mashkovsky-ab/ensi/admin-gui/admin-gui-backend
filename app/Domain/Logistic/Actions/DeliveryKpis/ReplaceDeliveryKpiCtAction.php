<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiCt;
use Ensi\LogisticClient\Dto\ReplaceDeliveryKpiCtRequest;

/**
 * Class ReplaceDeliveryKpiCtAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class ReplaceDeliveryKpiCtAction
{
    /**
     * ReplaceDeliveryKpiCtAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param int $sellerId
     * @param array $fields
     * @return DeliveryKpiCt
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiCt
    {
        $request = new ReplaceDeliveryKpiCtRequest();
        $request->setCt($fields['ct']);

        return $this->kpiApi->replaceDeliveryKpiCt($sellerId, $request)->getData();
    }
}
