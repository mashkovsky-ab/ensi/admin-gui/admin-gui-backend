<?php

namespace App\Domain\Logistic\Tests\CargoOrders\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticCargoStatusEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\Cargo;
use Ensi\LogisticClient\Dto\CargoResponse;
use Ensi\LogisticClient\Dto\SearchCargoResponse;

class CargoFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
            'store_id' => $this->faker->randomNumber(),
            'delivery_service_id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(LogisticCargoStatusEnum::cases()),
            'status_at' => $this->faker->dateTime(),
            'is_problem' => $this->faker->boolean(),
            'is_problem_at' => $this->faker->dateTime(),
            'shipping_problem_comment' => $this->faker->text(200),
            'is_canceled' => $this->faker->boolean(),
            'is_canceled_at' => $this->faker->dateTime(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
        ];
    }

    public function make(array $extra = []): Cargo
    {
        return new Cargo($this->makeArray($extra));
    }


    public function makeResponseOne(array $extra = []): CargoResponse
    {
        return new CargoResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchCargoResponse
    {
        return $this->generateResponseSearch(SearchCargoResponse::class, $extra, $count);
    }
}
