<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\DeliveryServiceDocument;
use Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse;
use Ensi\LogisticClient\Dto\File;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsResponse;

class DeliveryServiceDocumentFactory extends BaseApiFactory
{
    protected ?DeliveryService $deliveryService = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'delivery_service_id' => $this->faker->randomElement(LogisticDeliveryServiceEnum::cases()),
            'name' => $this->faker->text(20),
            'file' => new File(EnsiFileFactory::new()->make()),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->deliveryService) {
            $definition['delivery_service'] = $this->deliveryService;
        }

        return $definition;
    }

    public function withDeliveryService(?DeliveryService $deliveryService = null): self
    {
        $this->deliveryService = $deliveryService ?: DeliveryServiceFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): DeliveryServiceDocument
    {
        return new DeliveryServiceDocument($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): DeliveryServiceDocumentResponse
    {
        return new DeliveryServiceDocumentResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], $count = 1): SearchDeliveryServiceDocumentsResponse
    {
        return $this->generateResponseSearch(SearchDeliveryServiceDocumentsResponse::class, $extra, $count);
    }
}
