<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticPointTypeEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\MetroStation;
use Ensi\LogisticClient\Dto\Point;
use Ensi\LogisticClient\Dto\SearchPointsResponse;

class PointFactory extends BaseApiFactory
{
    protected array $metroStations = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
            'delivery_service' => $this->faker->randomElement(LogisticDeliveryServiceEnum::cases()),
            'type' => $this->faker->randomElement(LogisticPointTypeEnum::cases()),
            'name' => $this->faker->text(20),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'apiship_external_id' => $this->faker->unique()->numerify('#########'),
            'has_payment_card' => $this->faker->boolean(),
            'address' => AddressFactory::new()->make(),
            'city_guid' => $this->faker->uuid(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'timetable' => $this->faker->text(),
            'active' => $this->faker->boolean(),
        ];

        if ($this->metroStations) {
            $definition['metro_stations'] = $this->metroStations;
        }

        return $definition;
    }

    public function withMetroStation(?MetroStation $metroStation = null): self
    {
        $this->metroStations[] = $metroStation ?: MetroStationFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Point
    {
        return new Point($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchPointsResponse
    {
        return $this->generateResponseSearch(SearchPointsResponse::class, $extra, $count);
    }
}
