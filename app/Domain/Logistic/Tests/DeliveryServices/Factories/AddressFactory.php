<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseAddressFactory;
use Ensi\LogisticClient\Dto\Address;

class AddressFactory extends BaseAddressFactory
{
    public function make(array $extra = []): Address
    {
        return new Address($this->makeArray($extra));
    }
}
