<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories\Enums;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceStatusEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryServiceStatus;
use Ensi\LogisticClient\Dto\GetDeliveryServiceStatusesResponse;

class DeliveryServicesStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(LogisticDeliveryServiceStatusEnum::cases()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): DeliveryServiceStatus
    {
        return new DeliveryServiceStatus($this->makeArray($extra));
    }

    public function makeResponseGet(array $extra = []): GetDeliveryServiceStatusesResponse
    {
        return new GetDeliveryServiceStatusesResponse(['data' => [$this->make($extra)]]);
    }
}
