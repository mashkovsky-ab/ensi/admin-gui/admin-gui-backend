<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories\Enums;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticShipmentMethodEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\GetShipmentMethodsResponse;
use Ensi\LogisticClient\Dto\ShipmentMethod;

class ShipmentMethodFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(LogisticShipmentMethodEnum::cases()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): ShipmentMethod
    {
        return new ShipmentMethod($this->makeArray($extra));
    }

    public function makeResponseGet(array $extra = []): GetShipmentMethodsResponse
    {
        return new GetShipmentMethodsResponse(['data' => [$this->make($extra)]]);
    }
}
