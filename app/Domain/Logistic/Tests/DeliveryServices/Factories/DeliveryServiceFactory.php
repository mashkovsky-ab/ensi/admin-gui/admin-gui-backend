<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceStatusEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\DeliveryServiceResponse;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesResponse;

class DeliveryServiceFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(20),
            'registered_at' => $this->faker->dateTime(),

            'legal_info_company_name' => $this->faker->company(),
            'legal_info_company_address' => AddressFactory::new()->make(),
            'legal_info_inn' => $this->faker->unique()->numerify('############'),
            'legal_info_payment_account' => $this->faker->unique()->numerify('####################'),
            'legal_info_bik' => $this->faker->unique()->numerify('04#######'),
            'legal_info_bank' => $this->faker->company(),
            'legal_info_bank_correspondent_account' => $this->faker->unique()->numerify('301#################'),

            'general_manager_name' => $this->faker->name(),

            'contract_number' => $this->faker->unique()->numerify('############'),
            'contract_date' => $this->faker->dateTime(),

            'vat_rate' => $this->faker->numberBetween(1, 20),
            'taxation_type' => $this->faker->numberBetween(1, 5),

            'status' => $this->faker->randomElement(LogisticDeliveryServiceStatusEnum::cases()),

            'comment' => $this->faker->text(20),
            'apiship_key' => $this->faker->text(20),

            'priority' => $this->faker->numberBetween(1, 5),
            'max_shipments_per_day' => $this->faker->numberBetween(100, 200),
            'max_cargo_export_time' => $this->faker->time(),

            'do_consolidation' => $this->faker->boolean(),
            'do_deconsolidation' => $this->faker->boolean(),
            'do_zero_mile' => $this->faker->boolean(),
            'do_express_delivery' => $this->faker->boolean(),
            'do_return' => $this->faker->boolean(),
            'do_dangerous_products_delivery' => $this->faker->boolean(),
            'do_transportation_oversized_cargo' => $this->faker->boolean(),

            'add_partial_reject_service' => $this->faker->boolean(),
            'add_insurance_service' => $this->faker->boolean(),
            'add_fitting_service' => $this->faker->boolean(),
            'add_return_service' => $this->faker->boolean(),
            'add_open_service' => $this->faker->boolean(),

            'pct' => $this->faker->randomNumber(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): DeliveryService
    {
        return new DeliveryService($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): DeliveryServiceResponse
    {
        return new DeliveryServiceResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], $count = 1): SearchDeliveryServicesResponse
    {
        return $this->generateResponseSearch(SearchDeliveryServicesResponse::class, $extra, $count);
    }
}
