<?php

namespace App\Domain\Logistic\Tests\DeliveryKpis\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryKpi;
use Ensi\LogisticClient\Dto\DeliveryKpiResponse;

class DeliveryKpiFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'rtg' => $this->faker->randomNumber(),
            'ct' => $this->faker->randomNumber(),
            'ppt' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): DeliveryKpi
    {
        return new DeliveryKpi($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): DeliveryKpiResponse
    {
        return new DeliveryKpiResponse(['data' => $this->make($extra)]);
    }
}
