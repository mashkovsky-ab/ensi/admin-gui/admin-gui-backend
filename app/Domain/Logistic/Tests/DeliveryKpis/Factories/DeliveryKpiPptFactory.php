<?php

namespace App\Domain\Logistic\Tests\DeliveryKpis\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryKpiPpt;
use Ensi\LogisticClient\Dto\DeliveryKpiPptResponse;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiPptResponse;

class DeliveryKpiPptFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'ppt' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): DeliveryKpiPpt
    {
        return new DeliveryKpiPpt($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): DeliveryKpiPptResponse
    {
        return new DeliveryKpiPptResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchDeliveryKpiPptResponse
    {
        return $this->generateResponseSearch(SearchDeliveryKpiPptResponse::class, $extra, $count);
    }
}
