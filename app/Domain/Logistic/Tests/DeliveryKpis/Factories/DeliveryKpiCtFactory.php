<?php

namespace App\Domain\Logistic\Tests\DeliveryKpis\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryKpiCt;
use Ensi\LogisticClient\Dto\DeliveryKpiCtResponse;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiCtResponse;

class DeliveryKpiCtFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'ct' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): DeliveryKpiCt
    {
        return new DeliveryKpiCt($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): DeliveryKpiCtResponse
    {
        return new DeliveryKpiCtResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchDeliveryKpiCtResponse
    {
        return $this->generateResponseSearch(SearchDeliveryKpiCtResponse::class, $extra, $count);
    }
}
