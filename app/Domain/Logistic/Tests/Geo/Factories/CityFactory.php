<?php

namespace App\Domain\Logistic\Tests\Geo\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\City;

class CityFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
            'region_id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(20),
            'guid' => $this->faker->uuid(),
        ];
    }

    public function make(array $extra = []): City
    {
        return new City($this->makeArray($extra));
    }
}
