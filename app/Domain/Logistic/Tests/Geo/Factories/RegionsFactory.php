<?php

namespace App\Domain\Logistic\Tests\Geo\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\City;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\RegionResponse;
use Ensi\LogisticClient\Dto\SearchRegionsResponse;

class RegionsFactory extends BaseApiFactory
{
    protected array $cities = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'federal_district_id' => $this->faker->randomNumber(),
            'name' => $this->faker->name(),
            'guid' => $this->faker->uuid(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->cities) {
            $definition['cities'] = $this->cities;
        }

        return $definition;
    }

    public function withCity(?City $city = null): self
    {
        $this->cities[] = $city ?: CityFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Region
    {
        return new Region($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): RegionResponse
    {
        return new RegionResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchRegionsResponse
    {
        return $this->generateResponseSearch(SearchRegionsResponse::class, $extra, $count);
    }
}
