<?php

namespace App\Domain\Logistic\Tests\Geo\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Ensi\LogisticClient\Dto\FederalDistrictResponse;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\SearchFederalDistrictsResponse;

class FederalDistrictsFactory extends BaseApiFactory
{
    protected array $regions = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'name' => $this->faker->name(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->regions) {
            $definition['regions'] = $this->regions;
        }

        return $definition;
    }

    public function withRegion(?Region $region = null): self
    {
        $this->regions[] = $region ?: RegionsFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): FederalDistrict
    {
        return new FederalDistrict($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): FederalDistrictResponse
    {
        return new FederalDistrictResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchFederalDistrictsResponse
    {
        return $this->generateResponseSearch(SearchFederalDistrictsResponse::class, $extra, $count);
    }
}
