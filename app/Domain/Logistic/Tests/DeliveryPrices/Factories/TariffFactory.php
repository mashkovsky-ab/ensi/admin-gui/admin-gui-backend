<?php

namespace App\Domain\Logistic\Tests\DeliveryPrices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticShipmentMethodEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\SearchTariffsResponse;
use Ensi\LogisticClient\Dto\Tariff;

class TariffFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(20),
            'description' => $this->faker->text(),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'apiship_external_id' => $this->faker->unique()->numerify('#########'),
            'weight_min' => (int)$this->faker->randomFloat(500, 10000),
            'weight_max' => (int)$this->faker->randomFloat(10000, 20000),
            'delivery_service' => $this->faker->randomElement(LogisticDeliveryServiceEnum::cases()),
            'delivery_method' => $this->faker->randomElement(LogisticDeliveryMethodEnum::cases()),
            'shipment_method' => $this->faker->randomElement(LogisticShipmentMethodEnum::cases()),
        ];
    }

    public function make(array $extra = []): Tariff
    {
        return new Tariff($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchTariffsResponse
    {
        return $this->generateResponseSearch(SearchTariffsResponse::class, $extra, $count);
    }
}
