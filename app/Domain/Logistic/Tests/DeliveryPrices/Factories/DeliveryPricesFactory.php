<?php

namespace App\Domain\Logistic\Tests\DeliveryPrices\Factories;

use App\Domain\Logistic\Tests\Geo\Factories\FederalDistrictsFactory;
use App\Domain\Logistic\Tests\Geo\Factories\RegionsFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryPrice;
use Ensi\LogisticClient\Dto\DeliveryPriceResponse;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesResponse;

class DeliveryPricesFactory extends BaseApiFactory
{
    protected ?FederalDistrict $federalDistrict = null;
    protected ?Region $region = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'federal_district_id' => $this->faker->randomNumber(),
            'region_id' => $this->faker->randomNumber(),
            'region_guid' => $this->faker->uuid(),
            'delivery_service' => $this->faker->randomElement(LogisticDeliveryServiceEnum::cases()),
            'delivery_method' => $this->faker->randomElement(LogisticDeliveryMethodEnum::cases()),
            'price' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->region) {
            $definition['region'] = $this->region;
        }

        if ($this->federalDistrict) {
            $definition['federal_district'] = $this->federalDistrict;
        }

        return $definition;
    }

    public function withRegion(?Region $region = null): self
    {
        $this->region = $region ?: RegionsFactory::new()->make();

        return $this;
    }

    public function withFederalDistrict(?FederalDistrict $federalDistrict = null): self
    {
        $this->federalDistrict = $federalDistrict ?: FederalDistrictsFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): DeliveryPrice
    {
        return new DeliveryPrice($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): DeliveryPriceResponse
    {
        return new DeliveryPriceResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchDeliveryPricesResponse
    {
        return $this->generateResponseSearch(SearchDeliveryPricesResponse::class, $extra, $count);
    }
}
