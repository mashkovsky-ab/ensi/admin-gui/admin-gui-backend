<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\BuClient\Dto\StoreContactForCreate;

class CreateStoreContactAction
{
    public function __construct(
        private StoreContactsApi $storeContactsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): StoreContact
    {
        $storeContact = new StoreContactForCreate($fields);

        return $this->storeContactsApi->createStoreContact($storeContact)->getData();
    }
}
