<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\EmptyDataResponse;

class DeleteStoreContactAction
{
    public function __construct(
        private StoreContactsApi $storeContactsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): EmptyDataResponse
    {
        return $this->storeContactsApi->deleteStoreContact($id);
    }
}
