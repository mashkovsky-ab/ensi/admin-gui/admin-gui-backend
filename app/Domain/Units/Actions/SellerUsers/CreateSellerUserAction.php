<?php

namespace App\Domain\Units\Actions\SellerUsers;

use App\Domain\Units\Data\SellerUserData;
use Ensi\AdminAuthClient\ApiException as AdminAuthApiException;
use Ensi\AdminAuthClient\Dto\CreateUserRequest;
use Ensi\BuClient\ApiException as BuApiException;
use Ensi\BuClient\Dto\OperatorForCreate;

/**
 * Class CreateSellerUserAction
 * @package App\Domain\Units\Actions\SellerUsers
 */
class CreateSellerUserAction extends SellerUserAction
{
    /**
     * @param  array  $fields
     * @return SellerUserData
     * @throws BuApiException
     * @throws AdminAuthApiException
     */
    public function execute(array $fields): SellerUserData
    {
        $userRequest = new CreateUserRequest($fields);
        $user =  $this->usersApi->createUser($userRequest)->getData();

        $operator= new OperatorForCreate(array_merge($fields, ['user_id' => $user->getId()]));

        return $this->prepareSellerUser($this->operatorsApi->createOperator($operator)->getData(), $user);
    }
}
