<?php

namespace App\Domain\Units\Actions\SellerUsers;

use Ensi\AdminAuthClient\Dto\AddRolesToUserRequest;

class AddRolesToSellerUserAction extends SellerUserAction
{
    public function execute(int $operatorId, array $data): void
    {
        $operator = $this->operatorsApi->getOperator($operatorId)->getData();

        $request = new AddRolesToUserRequest($data);
        $this->usersApi->addRolesToUser($operator->getUserId(), $request);
    }
}
