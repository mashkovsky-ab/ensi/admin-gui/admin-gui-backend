<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;

class RefreshPasswordTokenAction
{
    public function __construct(
        protected AdminUsersApi $adminUsersApi,
    ) {
    }

    public function execute(int $userId): void
    {
        $this->adminUsersApi->refreshPasswordToken($userId);
    }
}
