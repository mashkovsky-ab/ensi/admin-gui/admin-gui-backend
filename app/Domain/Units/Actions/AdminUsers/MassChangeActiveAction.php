<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\MassChangeActive;

class MassChangeActiveAction
{
    public function __construct(
        protected AdminUsersApi $adminUsersApi,
    ) {
    }

    public function execute(array $data): void
    {
        $this->adminUsersApi->massChangeActive(new MassChangeActive($data));
    }
}
