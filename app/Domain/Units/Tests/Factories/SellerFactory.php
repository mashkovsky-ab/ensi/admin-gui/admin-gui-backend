<?php

namespace App\Domain\Units\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\BuClient\Dto\SearchSellersResponse;
use Ensi\BuClient\Dto\Seller;
use Ensi\BuClient\Dto\SellerStatusEnum;

class SellerFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'status_at' => $this->faker->date(), // todo dateTime()
            'legal_name' => $this->faker->company(),
            'external_id' => $this->faker->word(),
            'inn' => $this->faker->numerify('##########'),
            'kpp' => $this->faker->numerify('#########'),
            'legal_address' => $this->faker->address(),
            'fact_address' => $this->faker->address(),
            'payment_account' => $this->faker->numerify('####################'),
            'bank' => $this->faker->company,
            'bank_address' => $this->faker->address,
            'bank_bik' => $this->faker->numerify('#########'),
            'status' => $this->faker->randomElement(SellerStatusEnum::getAllowableEnumValues()),
            'manager_id' => $this->faker->randomNumber(),
            'correspondent_account' => $this->faker->numerify('####################'),
            'storage_address' => $this->faker->address(),
            'site' => $this->faker->url(),
            'can_integration' => $this->faker->boolean(),
            'sale_info' => $this->faker->text(),
            'city' => $this->faker->city(),
            'created_at' => $this->faker->date(), // todo dateTime()
            'updated_at' => $this->faker->date(), // todo dateTime()
        ];
    }

    public function make(array $extra = []): Seller
    {
        return new Seller($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchSellersResponse
    {
        return $this->generateResponseSearch(SearchSellersResponse::class, $extra, $count);
    }
}
