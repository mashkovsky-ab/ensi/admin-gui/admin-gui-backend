<?php

namespace App\Domain\Units\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\Store;

class StoreFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
            'xml_id' => $this->faker->word(),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => StoreAddressFactory::new()->make(),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->date(), // todo dateTime()
            'updated_at' => $this->faker->date(), // todo dateTime()
        ];
    }

    public function make(array $extra = []): Store
    {
        return new Store($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchStoresResponse
    {
        return $this->generateResponseSearch(SearchStoresResponse::class, $extra, $count);
    }
}
