<?php

namespace App\Domain\Units\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\BuClient\Dto\StoreFillablePropertiesAddress;

class StoreAddressFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'address_string' => $this->faker->address,
            'country_code' => $this->faker->countryCode,
            'post_index' => $this->faker->postcode,
            'region' => $this->faker->city,
            'region_guid' => $this->faker->uuid(),
            'area' => $this->faker->city,
            'area_guid' => $this->faker->uuid(),
            'city' => $this->faker->city,
            'city_guid' => $this->faker->uuid(),
            'street' => $this->faker->streetName,
            'house' => $this->faker->numerify('##'),
            'block' => $this->faker->numerify('##'),
            'flat' => $this->faker->numerify('##'),
            'porch' => $this->faker->numerify('##'),
            'floor' => $this->faker->numerify('##'),
            'intercom' => $this->faker->numerify('k####c####'),
            'comment' => $this->faker->text,
            'geo_lat' => (string)$this->faker->latitude,
            'geo_lon' => (string)$this->faker->longitude,
        ];
    }

    public function make(array $extra = []): StoreFillablePropertiesAddress
    {
        return new StoreFillablePropertiesAddress($this->makeArray($extra));
    }
}
