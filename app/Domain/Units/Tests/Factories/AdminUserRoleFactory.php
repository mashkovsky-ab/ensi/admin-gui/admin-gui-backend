<?php

namespace App\Domain\Units\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\AdminAuthClient\Dto\SearchRolesResponse;
use Ensi\AdminAuthClient\Dto\UserRole;

class AdminUserRoleFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'title' => $this->faker->title(),
        ];
    }

    public function make(array $extra = []): UserRole
    {
        return new UserRole($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchRolesResponse
    {
        return $this->generateResponseSearch(SearchRolesResponse::class, $extra, $count);
    }
}
