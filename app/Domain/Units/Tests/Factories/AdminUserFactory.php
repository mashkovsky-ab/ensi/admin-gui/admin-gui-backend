<?php

namespace App\Domain\Units\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\AdminAuthClient\Dto\SearchUsersResponse;
use Ensi\AdminAuthClient\Dto\User;
use Ensi\AdminAuthClient\Dto\UserResponse;

class AdminUserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'login' => $this->faker->email(),
            'active' => $this->faker->boolean(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->lastName(),
            'full_name' => $this->faker->name(),
            'short_name' => $this->faker->name(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): User
    {
        return new User($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchUsersResponse
    {
        return $this->generateResponseSearch(SearchUsersResponse::class, $extra, $count);
    }

    public function makeResponseOne(array $extra = []): UserResponse
    {
        return new UserResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }
}
