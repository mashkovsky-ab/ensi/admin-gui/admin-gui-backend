<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountConditionsApi;

class DeleteDiscountConditionAction
{
    public function __construct(protected DiscountConditionsApi $discountConditionsApi)
    {
    }

    public function execute(int $discountConditionId): void
    {
        $this->discountConditionsApi->deleteDiscountCondition($discountConditionId);
    }
}
