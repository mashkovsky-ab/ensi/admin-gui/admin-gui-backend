<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountOffersApi;
use Ensi\MarketingClient\Dto\DiscountOffer;
use Ensi\MarketingClient\Dto\DiscountOfferUpdatableProperties;

class PatchDiscountOfferAction
{
    public function __construct(protected DiscountOffersApi $discountsApi)
    {
    }

    public function execute(int $discountOfferId, array $data): DiscountOffer
    {
        $requestDiscountOffersApi  = new DiscountOfferUpdatableProperties($data);
        $responseDiscountOffersApi = $this->discountsApi->patchDiscountOffer(
            $discountOfferId,
            $requestDiscountOffersApi
        );

        return $responseDiscountOffersApi->getData();
    }
}
