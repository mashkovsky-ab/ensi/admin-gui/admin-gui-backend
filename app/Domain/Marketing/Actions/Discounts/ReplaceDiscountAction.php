<?php

namespace App\Domain\Marketing\Actions\Discounts;

use App\Domain\Auth\Models\User;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\Discount;
use Ensi\MarketingClient\Dto\DiscountForReplace;

class ReplaceDiscountAction
{
    public function __construct(protected DiscountsApi $discountsApi)
    {
    }

    public function execute(int $discountId, array $data): Discount
    {
        /** @var User $user */
        $user = auth()->user();

        $data['user_id'] = $user->getId();
        $data['seller_id'] = null;

        $requestDiscountsApi = new DiscountForReplace($data);
        $responsePromoCodesApi = $this->discountsApi->replaceDiscount($discountId, $requestDiscountsApi);

        return $responsePromoCodesApi->getData();
    }
}
