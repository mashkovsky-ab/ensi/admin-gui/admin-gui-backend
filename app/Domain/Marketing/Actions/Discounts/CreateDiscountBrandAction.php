<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountBrandsApi;
use Ensi\MarketingClient\Dto\DiscountBrand;
use Ensi\MarketingClient\Dto\DiscountBrandRaw;

class CreateDiscountBrandAction
{
    public function __construct(protected DiscountBrandsApi $discountBrandsApi)
    {
    }

    public function execute(array $data): DiscountBrand
    {
        $requestDiscountBrandsApi = new DiscountBrandRaw($data);
        $responseDiscountBrandsApi = $this->discountBrandsApi->createDiscountBrand($requestDiscountBrandsApi);

        return $responseDiscountBrandsApi->getData();
    }
}
