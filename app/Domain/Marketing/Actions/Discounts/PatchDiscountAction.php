<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\Discount;
use Ensi\MarketingClient\Dto\DiscountUpdatableProperties;

class PatchDiscountAction
{
    public function __construct(protected DiscountsApi $discountsApi)
    {
    }

    public function execute(int $discountId, array $data): Discount
    {
        $requestDiscountsApi = new DiscountUpdatableProperties($data);
        $responseDiscountsApi = $this->discountsApi->patchDiscount($discountId, $requestDiscountsApi);

        return $responseDiscountsApi->getData();
    }
}
