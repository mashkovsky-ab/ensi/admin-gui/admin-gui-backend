<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountSegmentsApi;
use Ensi\MarketingClient\Dto\DiscountSegment;
use Ensi\MarketingClient\Dto\DiscountSegmentRaw;

class CreateDiscountSegmentAction
{
    public function __construct(protected DiscountSegmentsApi $discountSegmentsApi)
    {
    }

    public function execute(array $data): DiscountSegment
    {
        $requestDiscountSegmentsApi = new DiscountSegmentRaw($data);
        $responseDiscountSegmentsApi = $this->discountSegmentsApi->createDiscountSegment($requestDiscountSegmentsApi);

        return $responseDiscountSegmentsApi->getData();
    }
}
