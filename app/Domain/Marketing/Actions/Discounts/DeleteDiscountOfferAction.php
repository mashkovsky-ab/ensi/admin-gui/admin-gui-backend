<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountOffersApi;

class DeleteDiscountOfferAction
{
    public function __construct(protected DiscountOffersApi $discountOffersApi)
    {
    }

    public function execute(int $discountOfferId): void
    {
        $this->discountOffersApi->deleteDiscountOffer($discountOfferId);
    }
}
