<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountCategoriesApi;

class DeleteDiscountCategoryAction
{
    public function __construct(protected DiscountCategoriesApi $discountCategoriesApi)
    {
    }

    public function execute(int $discountCategoryId): void
    {
        $this->discountCategoriesApi->deleteDiscountCategory($discountCategoryId);
    }
}
