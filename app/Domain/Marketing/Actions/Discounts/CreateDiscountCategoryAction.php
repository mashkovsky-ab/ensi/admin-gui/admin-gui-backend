<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountCategoriesApi;
use Ensi\MarketingClient\Dto\DiscountCategory;
use Ensi\MarketingClient\Dto\DiscountCategoryRaw;

class CreateDiscountCategoryAction
{
    public function __construct(protected DiscountCategoriesApi $discountCategoriesApi)
    {
    }

    public function execute(array $data): DiscountCategory
    {
        $requestDiscountCategoriesApi = new DiscountCategoryRaw($data);
        $responseDiscountCategoriesApi = $this->discountCategoriesApi->createDiscountCategory($requestDiscountCategoriesApi);

        return $responseDiscountCategoriesApi->getData();
    }
}
