<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use Ensi\MarketingClient\Api\PromoCodesApi;

class DeletePromoCodeAction
{
    public function __construct(protected PromoCodesApi $promoCodesApi)
    {
    }

    public function execute(int $promoCodeId)
    {
        $this->promoCodesApi->deletePromoCode($promoCodeId);
    }
}
