<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\PromoCode;
use Ensi\MarketingClient\Dto\PromoCodeUpdatableProperties;

class PatchPromoCodeAction
{
    public function __construct(protected PromoCodesApi $promoCodesApi)
    {
    }

    public function execute(int $promoCodeId, array $data): PromoCode
    {
        $requestPromoCodesApi = new PromoCodeUpdatableProperties($data);
        $responsePromoCodesApi = $this->promoCodesApi->patchPromoCode($promoCodeId, $requestPromoCodesApi);

        return $responsePromoCodesApi->getData();
    }
}
