<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductGroupsApi;
use Ensi\CmsClient\Dto\ProductGroupFile;
use Illuminate\Http\UploadedFile;

class UploadProductGroupFileAction
{
    public function __construct(
        protected ProductGroupsApi $productGroupApi
    ) {
    }

    public function execute(int $id, UploadedFile $file): ProductGroupFile
    {
        $responseProductGroupsApi = $this->productGroupApi->uploadProductGroupFiles($id, $file);

        return $responseProductGroupsApi->getData();
    }
}
