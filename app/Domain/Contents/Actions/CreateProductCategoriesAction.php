<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductCategoriesApi;
use Ensi\CmsClient\Dto\ProductCategory;
use Ensi\CmsClient\Dto\ProductCategoryForCreate;

class CreateProductCategoriesAction
{
    public function __construct(
        protected ProductCategoriesApi $productCategoriesApi
    ) {
    }

    public function execute(array $data): ProductCategory
    {
        $requestProductCategoriesApi = new ProductCategoryForCreate($data);
        $responseProductCategoriesApi =  $this->productCategoriesApi->createProductCategory($requestProductCategoriesApi);

        return $responseProductCategoriesApi->getData();
    }
}
