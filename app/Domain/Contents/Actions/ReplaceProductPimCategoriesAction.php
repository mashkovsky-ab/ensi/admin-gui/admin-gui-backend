<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductPimCategoriesApi;
use Ensi\CmsClient\Dto\ProductPimCategory;
use Ensi\CmsClient\Dto\ProductPimCategoryForReplace;

class ReplaceProductPimCategoriesAction
{
    public function __construct(
        protected ProductPimCategoriesApi $productPimCategoriesApi
    ) {
    }

    public function execute(int $id, array $data): ProductPimCategory
    {
        $requestProductPimCategoriesApi = new ProductPimCategoryForReplace($data);
        $responseProductPimCategoriesApi =  $this->productPimCategoriesApi->replaceProductPimCategory($id, $requestProductPimCategoriesApi);

        return $responseProductPimCategoriesApi->getData();
    }
}
