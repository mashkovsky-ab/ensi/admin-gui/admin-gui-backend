<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductPimCategoriesApi;
use Ensi\CmsClient\Dto\ProductPimCategory;
use Ensi\CmsClient\Dto\ProductPimCategoryForCreate;

class CreateProductPimCategoriesAction
{
    public function __construct(
        protected ProductPimCategoriesApi $productPimCategoriesApi
    ) {
    }

    public function execute(array $data): ProductPimCategory
    {
        $requestProductPimCategoriesApi = new ProductPimCategoryForCreate($data);
        $responseProductPimCategoriesApi =  $this->productPimCategoriesApi->createProductPimCategory($requestProductPimCategoriesApi);

        return $responseProductPimCategoriesApi->getData();
    }
}
