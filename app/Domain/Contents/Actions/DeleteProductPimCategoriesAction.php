<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductPimCategoriesApi;

class DeleteProductPimCategoriesAction
{
    public function __construct(
        private ProductPimCategoriesApi $productPimCategoriesApi
    ) {
    }

    public function execute(int $id): void
    {
        $this->productPimCategoriesApi->deleteProductPimCategory($id);
    }
}
