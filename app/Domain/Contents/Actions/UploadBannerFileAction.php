<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\BannerFile;
use Illuminate\Http\UploadedFile;

class UploadBannerFileAction
{
    public function __construct(
        private BannersApi $bannersApi
    ) {
    }

    public function execute(int $id, string $type, UploadedFile $file): BannerFile
    {
        $responseProductGroupsApi = $this->bannersApi->uploadBannerFiles($id, $type, $file);

        return $responseProductGroupsApi->getData();
    }
}
