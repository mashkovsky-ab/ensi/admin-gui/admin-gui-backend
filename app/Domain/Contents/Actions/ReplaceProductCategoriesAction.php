<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductCategoriesApi;
use Ensi\CmsClient\Dto\ProductCategoryForReplace;

class ReplaceProductCategoriesAction
{
    public function __construct(
        protected ProductCategoriesApi $productCategoriesApi
    ) {
    }

    public function execute(int $id, $data)
    {
        $requestProductCategoriesApi = new ProductCategoryForReplace($data);
        $responseProductCategoriesApi =  $this->productCategoriesApi->replaceProductCategory($id, $requestProductCategoriesApi);

        return $responseProductCategoriesApi->getData();
    }
}
