<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductGroupsApi;

class DeleteProductGroupFileAction
{
    public function __construct(
        protected ProductGroupsApi $productGroupApi
    ) {
    }

    public function execute(int $id): void
    {
        $this->productGroupApi->deleteProductGroupFiles($id);
    }
}
