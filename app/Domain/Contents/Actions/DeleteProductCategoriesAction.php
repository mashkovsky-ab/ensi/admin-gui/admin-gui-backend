<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductCategoriesApi;

class DeleteProductCategoriesAction
{
    public function __construct(
        private ProductCategoriesApi $productCategoriesApi
    ) {
    }

    public function execute(int $id): void
    {
        $this->productCategoriesApi->deleteProductCategory($id);
    }
}
