<?php

namespace App\Domain\Customers\Actions\ProductSubscribes;

use Ensi\CrmClient\Api\ProductSubscribesApi;

abstract class ProductSubscribesAction
{
    public function __construct(protected ProductSubscribesApi $productSubscribesApi)
    {
    }
}
