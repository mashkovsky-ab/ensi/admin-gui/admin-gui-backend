<?php

namespace App\Domain\Customers\Actions\Attributes;

class DeleteCustomerAttributeAction extends CustomerAttributeAction
{
    /**
     * @param int $attributeId
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $attributeId): void
    {
        $this->attributesApi->deleteCustomerAttribute($attributeId);
    }
}
