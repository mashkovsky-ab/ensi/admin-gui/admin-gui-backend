<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use Ensi\CrmClient\Api\BonusOperationsApi;

abstract class BonusOperationsAction
{
    public function __construct(protected BonusOperationsApi $bonusOperationsApi)
    {
    }
}
