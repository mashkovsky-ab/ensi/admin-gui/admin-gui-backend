<?php

namespace App\Domain\Customers\Actions\BonusOperations;

class DeleteBonusOperationsAction extends BonusOperationsAction
{
    /**
     * @param int $bonusOperationId
     * @throws \Ensi\CrmClient\ApiException
     */
    public function execute(int $bonusOperationId): void
    {
        $this->bonusOperationsApi->deleteBonusOperation($bonusOperationId);
    }
}
