<?php

namespace App\Domain\Customers\Actions\Favourites;

use Ensi\CrmClient\Dto\CustomerFavorite;
use Ensi\CrmClient\Dto\CustomerFavoriteForCreate;

class CreateCustomerFavouritesAction extends CustomerFavouritesAction
{
    public function execute(array $fields): CustomerFavorite
    {
        return $this->customerFavoritesApi->createCustomerFavorite(new CustomerFavoriteForCreate($fields))->getData();
    }
}
