<?php

namespace App\Domain\Customers\Actions\Favourites;

use Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest;

class ClearCustomerFavouritesAction extends CustomerFavouritesAction
{
    public function execute(array $fields): void
    {
        $this->customerFavoritesApi->clearCustomerFavorites(new ClearCustomerFavoritesRequest($fields));
    }
}
