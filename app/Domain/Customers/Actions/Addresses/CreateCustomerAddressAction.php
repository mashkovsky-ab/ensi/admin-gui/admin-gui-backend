<?php

namespace App\Domain\Customers\Actions\Addresses;

use Ensi\CustomersClient\Dto\CustomerAddress;
use Ensi\CustomersClient\Dto\CustomerAddressForCreate;

/**
 * Class CreateCustomerAddressAction
 * @package App\Domain\Customers\Actions\Addresses
 */
class CreateCustomerAddressAction extends CustomerAddressAction
{
    /**
     * @param array $fields
     * @return CustomerAddress
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(array $fields): CustomerAddress
    {
        return $this->addressesApi->createCustomerAddress(new CustomerAddressForCreate($fields))->getData();
    }
}
