<?php

namespace App\Domain\Customers\Actions\Addresses;

use Ensi\CustomersClient\Api\AddressesApi;

/**
 * Class CustomerAddressAction
 * @package App\Domain\Customers\Actions\Addresses
 */
abstract class CustomerAddressAction
{
    /**
     * CustomerAddressAction constructor.
     * @param  AddressesApi  $addressesApi
     */
    public function __construct(protected AddressesApi $addressesApi)
    {
    }
}
