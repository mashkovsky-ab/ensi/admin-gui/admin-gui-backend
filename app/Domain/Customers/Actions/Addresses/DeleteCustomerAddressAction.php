<?php

namespace App\Domain\Customers\Actions\Addresses;

/**
 * Class DeleteCustomerAddressAction
 * @package App\Domain\Customers\Actions\Addresses
 */
class DeleteCustomerAddressAction extends CustomerAddressAction
{
    /**
     * @param int $addressId
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $addressId): void
    {
        $this->addressesApi->deleteCustomerAddress($addressId);
    }
}
