<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\PatchUserRequest;
use Ensi\CustomerAuthClient\Dto\User;

/**
 * Class ReplaceCustomerUserAction
 * @package App\Domain\Customers\Actions\Users
 */
class ReplaceCustomerUserAction extends CustomerUserAction
{
    /**
     * @param  int  $customerUserId
     * @param  array  $fields
     * @return User
     * @throws ApiException
     */
    public function execute(int $customerUserId, array $fields): User
    {
        return $this->usersApi->patchUser($customerUserId, new PatchUserRequest($fields))->getData();
    }
}
