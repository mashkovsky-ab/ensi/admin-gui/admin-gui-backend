<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\CreateUserRequest;
use Ensi\CustomerAuthClient\Dto\User;

/**
 * Class CreateCustomerUserAction
 * @package App\Domain\Customers\Actions\Users
 */
class CreateCustomerUserAction extends CustomerUserAction
{
    /**
     * @param  array  $fields
     * @return User
     * @throws ApiException
     */
    public function execute(array $fields): User
    {
        return $this->usersApi->createUser(new CreateUserRequest($fields))->getData();
    }
}
