<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Illuminate\Http\UploadedFile;

/**
 * Class UploadAvatarAction
 * @package App\Domain\Customers\Actions\Customers
 */
class UploadAvatarAction extends CustomerAction
{
    /**
     * @param int $customerId
     * @param UploadedFile $file
     * @return CustomerData
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $customerId, UploadedFile $file): CustomerData
    {
        return $this->prepareCustomer($this->customersApi->uploadCustomerAvatar($customerId, $file)->getData());
    }
}
