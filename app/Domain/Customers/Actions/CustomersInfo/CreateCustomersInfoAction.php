<?php

namespace App\Domain\Customers\Actions\CustomersInfo;

use Ensi\CrmClient\Dto\CustomerInfo;
use Ensi\CrmClient\Dto\CustomerInfoForCreate;

class CreateCustomersInfoAction extends CustomersInfoAction
{
    /**
     * @param array $fields
     * @return CustomerInfo
     * @throws \Ensi\CrmClient\ApiException
     */
    public function execute(array $fields): CustomerInfo
    {
        return $this->customersInfoApi->createCustomerInfo(new CustomerInfoForCreate($fields))->getData();
    }
}
