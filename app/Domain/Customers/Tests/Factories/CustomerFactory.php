<?php

namespace App\Domain\Customers\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\CustomerGenderEnum;
use Ensi\CustomersClient\Dto\File;
use Ensi\CustomersClient\Dto\SearchCustomersResponse;

class CustomerFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'user_id' => $this->faker->randomNumber(),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'yandex_metric_id' => $this->faker->optional()->uuid(),
            'google_analytics_id' => $this->faker->optional()->uuid(),
            'status_id' => $this->faker->randomNumber(),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->phoneNumber(),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'middle_name' => $this->faker->firstName(),
            'gender' => $this->faker->optional()->randomElement(CustomerGenderEnum::getAllowableEnumValues()),
            'create_by_admin' => $this->faker->boolean(),
            'avatar' => $this->faker->boolean() ? new File(EnsiFileFactory::new()->make()) : null,
            'city' => $this->faker->optional()->city(),
            'birthday' => $this->faker->optional()->dateTime(),
            'last_visit_date' => $this->faker->dateTime(),
            'comment_status' => $this->faker->optional()->text(50),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Customer
    {
        return new Customer($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchCustomersResponse
    {
        return $this->generateResponseSearch(SearchCustomersResponse::class, $extra, $count);
    }
}
