<?php


namespace App\Domain\Common\ProtectedFiles;

use JsonSerializable;

abstract class ProtectedFile implements JsonSerializable
{
    /** @var null|int Конкретная сущность, файл которой мы пытаемся скачать, например ID категории */
    public ?int $entity_id = null;
    /** @var null|string Если у сущности несколько видов файлов, то тут указывается тип (по сути это обозначение поля в БД), например preview */
    public ?string $file_type = null;
    /** @var null|string Если файлов одного типа много, то можно указать path|id на конкретный файл */
    public ?string $file = null;

    /**
     * Сущность, файл которой мы пытаемся скачать, например pim/category
     * @return string
     */
    abstract public static function entity(): string;

    /**
     * Заполнить данными из массива, который соответствует jsonSerialize
     * @param array $data
     * @return static
     */
    public function fillFromArray(array $data): static
    {
        $this->entity_id = $data['entity_id'] ?? null;
        $this->file_type = $data['file_type'] ?? null;
        $this->file = $data['file'] ?? null;

        return $this;
    }

    public function jsonSerialize()
    {
        return array_filter([
            'entity' => $this->entity(),
            'entity_id' => $this->entity_id,
            'file_type' => $this->file_type,
            'file' => $this->file,
        ]);
    }
}
