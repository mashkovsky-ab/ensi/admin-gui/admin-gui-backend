<?php

namespace App\Domain\Common\Data\Meta;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use App\Domain\Common\Data\Meta\Fields\BoolField;
use App\Domain\Common\Data\Meta\Fields\DatetimeField;
use App\Domain\Common\Data\Meta\Fields\EnumField;
use App\Domain\Common\Data\Meta\Fields\IdField;
use App\Domain\Common\Data\Meta\Fields\IntField;
use App\Domain\Common\Data\Meta\Fields\StringField;

class Field
{
    public static function id(string $code = 'id', string $name = 'ID'): IdField
    {
        return new IdField($code, $name);
    }

    public static function text(string $code, string $name): StringField
    {
        return new StringField($code, $name);
    }

    public static function string(string $code, string $name): StringField
    {
        return self::text($code, $name)->resetFilter();
    }

    public static function keyword(string $code, string $name): StringField
    {
        return self::text($code, $name)->filter();
    }

    public static function boolean(string $code, string $name): BoolField
    {
        return new BoolField($code, $name);
    }

    public static function enum(string $code, string $name, AbstractEnumInfo $enumInfo): EnumField
    {
        return new EnumField($code, $name, $enumInfo);
    }

    public static function datetime(string $code, string $name): DatetimeField
    {
        return new DatetimeField($code, $name);
    }

    public static function integer(string $code, string $name): IntField
    {
        return new IntField($code, $name);
    }
}
