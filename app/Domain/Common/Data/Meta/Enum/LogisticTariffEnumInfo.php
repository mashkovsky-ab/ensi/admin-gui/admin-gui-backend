<?php

namespace App\Domain\Common\Data\Meta\Enum;

class LogisticTariffEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'logistic.searchTariffEnumValues';
    }
}
