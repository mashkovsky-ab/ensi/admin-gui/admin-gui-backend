<?php

namespace App\Domain\Common\Data\Meta\Enum;

class CustomerUserEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'customers.searchCustomerEnumValues';
    }
}
