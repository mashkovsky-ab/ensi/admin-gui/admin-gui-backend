<?php

namespace App\Domain\Common\Data\Meta\Enum;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\PaymentMethodsResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrdersPaymentMethodEnumInfo extends AbstractEnumInfo implements AsyncLoader
{
    public function __construct(protected EnumsApi $enumsApi)
    {
    }

    public function requestAsync(): PromiseInterface
    {
        return $this->enumsApi->getPaymentMethodsAsync();
    }

    /**
     * @param PaymentMethodsResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $method) {
            $this->addValue($method->getId(), $method->getName());
        }
    }
}
