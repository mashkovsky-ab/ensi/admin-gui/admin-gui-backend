<?php

namespace App\Domain\Common\Data\Meta\Enum;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesResponse;
use GuzzleHttp\Promise\PromiseInterface;

class LogisticDeliveryServiceEnumInfo extends AbstractEnumInfo implements AsyncLoader
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function requestAsync(): PromiseInterface
    {
        return $this->deliveryServicesApi->searchDeliveryServicesAsync(new SearchDeliveryServicesRequest());
    }

    /**
     * @param SearchDeliveryServicesResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $deliveryService) {
            $this->addValue($deliveryService->getId(), $deliveryService->getName());
        }
    }
}
