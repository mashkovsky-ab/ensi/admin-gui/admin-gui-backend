<?php

namespace App\Domain\Common\Data\Meta\Enum;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\PaymentSystemsResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrdersPaymentSystemEnumInfo extends AbstractEnumInfo implements AsyncLoader
{
    public function __construct(protected EnumsApi $enumsApi)
    {
    }

    public function requestAsync(): PromiseInterface
    {
        return $this->enumsApi->getPaymentSystemsAsync();
    }

    /**
     * @param PaymentSystemsResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $system) {
            $this->addValue($system->getId(), $system->getName());
        }
    }
}
