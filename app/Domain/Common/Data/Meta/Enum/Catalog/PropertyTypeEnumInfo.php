<?php

namespace App\Domain\Common\Data\Meta\Enum\Catalog;

use App\Domain\Common\Data\AsyncLoader;
use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\PimClient\Api\EnumsApi;
use GuzzleHttp\Promise\PromiseInterface;

class PropertyTypeEnumInfo extends AbstractEnumInfo implements AsyncLoader
{
    public function __construct(private EnumsApi $api)
    {
    }

    public function requestAsync(): ?PromiseInterface
    {
        return $this->api->getPropertyTypesAsync();
    }

    public function processResponse($response)
    {
        $this->addValues($response->getData());
    }
}
