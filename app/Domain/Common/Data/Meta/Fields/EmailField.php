<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class EmailField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::EMAIL;
    }

    protected function init()
    {
        $this->filterLike()->sort();
    }
}
