<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class BoolField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::BOOL;
    }

    protected function init()
    {
        $this->sort()->filter();
    }
}
