<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class DateField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::DATE;
    }

    protected function init()
    {
        $this->sort()->filterRange();
    }
}
