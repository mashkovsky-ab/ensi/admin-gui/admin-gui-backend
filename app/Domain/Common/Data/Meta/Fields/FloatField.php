<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class FloatField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::FLOAT;
    }

    protected function init()
    {
        $this->sort()->filterRange();
    }
}
