<?php

namespace App\Domain\Common\Data;

use GuzzleHttp\Promise\PromiseInterface;

interface AsyncLoader
{
    public function requestAsync(): ?PromiseInterface;

    public function processResponse($response);
}
