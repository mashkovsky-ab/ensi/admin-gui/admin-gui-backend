<?php

namespace App\Domain\Auth;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\ApiException;
use Ensi\AdminAuthClient\Dto\User as UserDto;
use Illuminate\Log\LogManager;

class RemoteUserProvider
{
    public function __construct(
        private UsersApi $usersApi,
        private LogManager $logger
    ) {
    }

    /**
     * Запрашивает данные пользователя по идентификатору.
     *
     * @param int $identifier
     * @return User|null
     */
    public function retrieveById($identifier): ?User
    {
        return $this->retrieveUser($identifier);
    }

    /**
     * Загружает данные текущего пользователя.
     *
     * @param string $token
     * @return User|null
     */
    public function retrieveCurrent(string $token): ?User
    {
        try {
            $this->usersApi->getConfig()->setAccessToken($token);
            $response = $this->usersApi->getCurrentUser();

            $user = $this->constructUser($response->getData());
            $user->setRememberToken($token);

            return $user;
        } catch (ApiException $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }

    /**
     * Загружает данные пользователя.
     *
     * @param int $identifier
     * @return User|null
     */
    private function retrieveUser(int $identifier): ?User
    {
        try {
            $response = $this->usersApi->getUser($identifier);

            return $this->constructUser($response->getData());
        } catch (ApiException $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }

    private function constructUser(UserDto $userDto): User
    {
        $fields = [
            'id' => $userDto->getId(),
            'full_name' => $userDto->getFullName(),
            'short_name' => $userDto->getShortName(),
            'active' => $userDto->getActive(),
            'login' => $userDto->getLogin(),
            'last_name' => $userDto->getLastName(),
            'first_name' => $userDto->getFirstName(),
            'middle_name' => $userDto->getMiddleName(),
            'email' => $userDto->getEmail(),
            'phone' => $userDto->getPhone(),
            'roles' => collect($userDto->getRoles())->pluck('id')->values()->all(),
        ];

        return new User($fields);
    }
}
