<?php

namespace App\Domain\Auth\UserProviders;

use App\Domain\Auth\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as UserProviderContract;

abstract class UserProviderAbstract implements UserProviderContract
{
    protected ?User $user;

    /**
     * Возвращает пользователя по идентификатору.
     * @param  mixed  $identifier
     * @return User|Authenticatable|null
     */
    public function retrieveById($identifier): User|Authenticatable|null
    {
        if ($this->user !== null && $this->user->getAuthIdentifier() === $identifier) {
            return $this->user;
        }

        return $this->remoteProvider->retrieveById($identifier);
    }

    /**
     * Возвращает пользователя по токену.
     * @param  mixed $identifier
     * @param  string $token
     * @return User|Authenticatable|null
     */
    public function retrieveByToken($identifier, $token): User|Authenticatable|null
    {
        $user = $this->retrieveById($identifier);
        if ($user === null) {
            return null;
        }

        $rememberToken = $user->getRememberToken();

        return $rememberToken && hash_equals($rememberToken, $token) ? $user : null;
    }

    /**
     * Устанавливает текущего пользователя по авторизационной паре.
     * @param  array  $credentials
     * @return User|Authenticatable|null
     */
    abstract public function retrieveByCredentials(array $credentials): User|Authenticatable|null;
}
