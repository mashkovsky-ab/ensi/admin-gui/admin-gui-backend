<?php

namespace App\Domain\Communication\Actions;

use App\Domain\Communication\Data\ChatData;
use Ensi\InternalMessenger\Api\ChatsApi;
use Ensi\InternalMessenger\Dto\ChatForPatch;

class PatchChatAction
{
    public function __construct(protected ChatsApi $chatsApi)
    {
    }

    public function execute(int $chatId, array $fields): ChatData
    {
        $chatForPatch = new ChatForPatch($fields);

        $chat = $this->chatsApi->patchChat($chatId, $chatForPatch)->getData();

        return new ChatData($chat);
    }
}
