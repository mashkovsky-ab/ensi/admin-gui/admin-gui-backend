<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\StatusesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\Status;
use Ensi\CommunicationManagerClient\Dto\StatusForCreate;

class CreateStatusAction
{
    public function __construct(
        private StatusesApi $statusesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Status
    {
        $status = new StatusForCreate($fields);

        return $this->statusesApi->createStatus($status)->getData();
    }
}
