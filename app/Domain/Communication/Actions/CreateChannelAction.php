<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ChannelsApi;
use Ensi\CommunicationManagerClient\Dto\ChannelForCreate;

/**
 * Class CreateChannelAction
 * @package App\Domain\Communication\Actions
 */
class CreateChannelAction
{
    public function __construct(protected ChannelsApi $channelsApi)
    {
    }

    public function execute(array $fields)
    {
        $channel = new ChannelForCreate();
        $channel->setName($fields['name']);

        return $this->channelsApi->createChannel($channel)->getData();
    }
}
