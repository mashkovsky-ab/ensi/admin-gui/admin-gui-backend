<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ThemesApi;
use Ensi\CommunicationManagerClient\ApiException;

class DeleteThemeAction
{
    public function __construct(
        private ThemesApi $themesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $themeId)
    {
        return $this->themesApi->deleteTheme($themeId);
    }
}
