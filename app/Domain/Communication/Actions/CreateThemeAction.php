<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ThemesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\Theme;
use Ensi\CommunicationManagerClient\Dto\ThemeForCreate;

class CreateThemeAction
{
    public function __construct(
        private ThemesApi $themesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Theme
    {
        $theme = new ThemeForCreate($fields);

        return $this->themesApi->createTheme($theme)->getData();
    }
}
