<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\StatusesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\Status;
use Ensi\CommunicationManagerClient\Dto\StatusForPatch;

class PatchStatusAction
{
    public function __construct(
        private StatusesApi $statusesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $statusId, array $fields): Status
    {
        $status = new StatusForPatch($fields);

        return $this->statusesApi->patchStatus($statusId, $status)->getData();
    }
}
