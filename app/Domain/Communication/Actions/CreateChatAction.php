<?php

namespace App\Domain\Communication\Actions;

use App\Domain\Communication\Data\ChatData;
use Ensi\InternalMessenger\Api\ChatsApi;
use Ensi\InternalMessenger\Dto\ChatDirectionEnum;
use Ensi\InternalMessenger\Dto\ChatForCreate;

class CreateChatAction
{
    public function __construct(protected ChatsApi $chatsApi)
    {
    }

    public function execute(array $fields): ChatData
    {
        $chatForCreate = new ChatForCreate($fields);
        $chatForCreate->setDirection(ChatDirectionEnum::FROM_ADMIN_TO_USER);
        $chatForCreate->setMuted(false);

        $chat = $this->chatsApi->createChat($chatForCreate)->getData();

        return new ChatData($chat);
    }
}
