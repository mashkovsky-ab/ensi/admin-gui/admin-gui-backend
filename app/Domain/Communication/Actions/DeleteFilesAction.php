<?php

namespace App\Domain\Communication\Actions;

use Ensi\InternalMessenger\Api\AttachmentsApi;
use Ensi\InternalMessenger\Dto\DeleteAttachmentRequest;

class DeleteFilesAction
{
    public function __construct(private AttachmentsApi $attachmentsApi)
    {
    }

    public function execute($fileIds)
    {
        $deleteRequest = new DeleteAttachmentRequest();
        $deleteRequest->setFiles($fileIds);
        $this->attachmentsApi->deleteAttachment($deleteRequest);
    }
}
