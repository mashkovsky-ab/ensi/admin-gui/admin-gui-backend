<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ChannelsApi;
use Ensi\CommunicationManagerClient\Dto\ChannelForPatch;

/**
 * Class PatchChannelAction
 * @package App\Domain\Communication\Actions
 */
class PatchChannelAction
{
    public function __construct(protected ChannelsApi $channelsApi)
    {
    }

    public function execute(int $channelId, array $fields)
    {
        $channel = new ChannelForPatch();
        $channel->setName($fields['name']);

        return $this->channelsApi->patchChannel($channelId, $channel)->getData();
    }
}
