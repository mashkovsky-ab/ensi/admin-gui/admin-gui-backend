<?php


namespace App\Domain\Communication\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFileLoader;
use Ensi\InternalMessenger\Api\MessagesApi;
use Ensi\InternalMessenger\Dto\Message;
use Ensi\InternalMessenger\Dto\SearchMessagesFilter;
use Ensi\InternalMessenger\Dto\SearchMessagesRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MessageAttachmentLoader extends ProtectedFileLoader
{
    public function __construct(protected MessagesApi $messagesApi)
    {
    }

    public function getRootPath(): ?string
    {
        $message = $this->loadMessage();

        // Сейчас любой пользователь может просматривать инфо по файлу в админке, далее нужна будет проверка по роли/доступу к чату в соответствии с ФЗ

        foreach ($message->getFiles() as $file) {
            if ($file->getPath() == $this->file->file) {
                return $file->getRootPath();
            }
        }

        return null;
    }

    protected function loadMessage(): Message
    {
        $request = new SearchMessagesRequest();
        $request->setFilter((new SearchMessagesFilter())->setId($this->file->entity_id));
        $messages = $this->messagesApi->searchMessages($request)->getData();
        if ($messages) {
            return current($messages);
        }

        throw new ModelNotFoundException();
    }
}
