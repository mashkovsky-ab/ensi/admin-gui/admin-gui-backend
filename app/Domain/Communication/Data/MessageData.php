<?php


namespace App\Domain\Communication\Data;

use App\Domain\Communication\ProtectedFiles\MessageAttachment;
use Ensi\InternalMessenger\Dto\Message;

class MessageData
{
    public function __construct(public Message $message)
    {
    }

    public function getFiles(): array
    {
        $files = [];
        foreach ($this->message->getFiles() as $attach) {
            $files[] = MessageAttachment::createFromModel($this->message, $attach);
        }

        return $files;
    }
}
