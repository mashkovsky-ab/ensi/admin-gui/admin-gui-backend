<?php


namespace App\Domain\Communication\Data;

use Ensi\InternalMessenger\Dto\Chat;

class ChatData
{
    public function __construct(public Chat $chat)
    {
    }

    /**
     * @return MessageData[]|null
     */
    public function getMessages(): ?array
    {
        if (is_null($this->chat->getMessages())) {
            return null;
        }

        $messages = [];
        foreach ($this->chat->getMessages() as $message) {
            $messages[] = new MessageData($message);
        }

        return $messages;
    }
}
