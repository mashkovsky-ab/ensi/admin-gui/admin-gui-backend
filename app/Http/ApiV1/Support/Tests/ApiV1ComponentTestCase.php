<?php

namespace App\Http\ApiV1\Support\Tests;

use Ensi\AdminAuthClient\Api\OauthApi;
use Ensi\AdminAuthClient\Api\UsersApi as AdminAuthUsersApi;
use Ensi\BasketsClient\Api\CommonApi as BasketsCommonApi;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\CustomerAuthClient\Api\UsersApi as CustomerAuthUsersApi;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Api\StocksApi;
use Ensi\OmsClient\Api\CommonApi as OmsCommonApi;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Api\EnumsApi as OmsEnumsApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Api\EnumsApi as PimEnumsApi;
use Ensi\PimClient\Api\ProductFieldsApi;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Api\PropertiesApi;
use Mockery\MockInterface;
use Tests\ComponentTestCase;

abstract class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }

    // region service customers
    public function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }

    public function mockCustomersAuthUsersApi(): MockInterface|CustomerAuthUsersApi
    {
        return $this->mock(CustomerAuthUsersApi::class);
    }

    // endregion

    // region service oauth
    public function mockOauthApi(): MockInterface|OauthApi
    {
        return $this->mock(OauthApi::class);
    }

    // endregion

    // region service admin auth
    public function mockAdminAuthUsersApi(): MockInterface|AdminAuthUsersApi
    {
        return $this->mock(AdminAuthUsersApi::class);
    }

    // endregion

    // region service oms
    public function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }

    public function mockOmsDeliveriesApi(): MockInterface|DeliveriesApi
    {
        return $this->mock(DeliveriesApi::class);
    }

    public function mockOmsRefundsApi(): MockInterface|RefundsApi
    {
        return $this->mock(RefundsApi::class);
    }

    public function mockOmsEnumsApi(): MockInterface|OmsEnumsApi
    {
        return $this->mock(OmsEnumsApi::class);
    }

    public function mockOmsCommonApi(): MockInterface|OmsCommonApi
    {
        return $this->mock(OmsCommonApi::class);
    }

    // endregion

    // region service baskets
    public function mockBasketsCommonApi(): MockInterface|BasketsCommonApi
    {
        return $this->mock(BasketsCommonApi::class);
    }

    // endregion

    // region service logistic
    protected function mockLogisticCargoOrdersApi(): MockInterface|CargoOrdersApi
    {
        return $this->mock(CargoOrdersApi::class);
    }

    protected function mockLogisticKpiApi(): MockInterface|KpiApi
    {
        return $this->mock(KpiApi::class);
    }

    public function mockLogisticDeliveryServicesApi(): MockInterface|DeliveryServicesApi
    {
        return $this->mock(DeliveryServicesApi::class);
    }

    public function mockLogisticGeosApi(): MockInterface|GeosApi
    {
        return $this->mock(GeosApi::class);
    }

    public function mockLogisticDeliveryPricesApi(): MockInterface|DeliveryPricesApi
    {
        return $this->mock(DeliveryPricesApi::class);
    }

    // endregion

    // region service pim
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    protected function mockPimPropertiesApi(): MockInterface|PropertiesApi
    {
        return $this->mock(PropertiesApi::class);
    }

    protected function mockPimEnumsApi(): MockInterface|PimEnumsApi
    {
        return $this->mock(PimEnumsApi::class);
    }

    protected function mockPimCategoriesApi(): MockInterface|CategoriesApi
    {
        return $this->mock(CategoriesApi::class);
    }

    protected function mockPimBrandsApi(): MockInterface|BrandsApi
    {
        return $this->mock(BrandsApi::class);
    }

    protected function mockPimProductFieldsApi(): MockInterface|ProductFieldsApi
    {
        return $this->mock(ProductFieldsApi::class);
    }

    // endregion

    // region service offers
    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }

    protected function mockOffersStocksApi(): MockInterface|StocksApi
    {
        return $this->mock(StocksApi::class);
    }

    // endregion

    // region service units
    protected function mockBuSellersApi(): MockInterface|SellersApi
    {
        return $this->mock(SellersApi::class);
    }

    protected function mockBuStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class);
    }

    // endregion
}
