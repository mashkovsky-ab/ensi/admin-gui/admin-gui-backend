<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Data\CustomerData;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\Dto\PaginationTypeEnum as PaginationTypeEnumCustomerAuth;
use Ensi\CustomerAuthClient\Dto\RequestBodyPagination as RequestBodyPaginationCustomerAuth;
use Ensi\CustomerAuthClient\Dto\SearchUsersRequest;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\PaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\CustomersClient\Dto\SearchCustomersResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class CustomersQuery
 * @package App\Http\ApiV1\Modules\Customers\Queries
 */
class CustomersQuery extends CrmQuery
{
    use QueryBuilderFilterEnumTrait;

    protected bool $withoutUsers = false;

    /**
     * CustomersQuery constructor.
     * @param Request $httpRequest
     * @param CustomersApi $customersApi
     * @param UsersApi $usersApi
     */
    public function __construct(
        protected Request $httpRequest,
        protected CustomersApi $customersApi,
        protected UsersApi $usersApi
    ) {
        parent::__construct($httpRequest, SearchCustomersRequest::class);
    }

    /**
     * @param $request
     * @return SearchCustomersResponse
     * @throws ApiException
     */
    protected function search($request): SearchCustomersResponse
    {
        return $this->customersApi->searchCustomers($request);
    }

    /**
     * @param $response
     * @return array
     * @throws \Ensi\CustomerAuthClient\ApiException
     */
    protected function convertGetToItems($response): array
    {
        /** @var Collection|Customer[] $orders */
        $customers = collect($response->getData());
        $users = $this->loadUsers($customers->pluck('user_id')->unique()->filter()->all());
        $customersData = [];
        foreach ($customers as $customer) {
            $customerData = new CustomerData($customer);

            $user = $users->get($customer->getUserId());
            if ($user) {
                $customerData->setUser($user);
            }
            $customersData[] = $customerData;
        }

        return $customersData;
    }

    /**
     * @param  array  $userIds
     * @return Collection
     * @throws \Ensi\CustomerAuthClient\ApiException
     */
    protected function loadUsers(array $userIds): Collection
    {
        if (!$userIds || $this->withoutUsers) {
            return collect();
        }

        $request = new SearchUsersRequest();
        $request->setFilter((object)[
            'id' => $userIds,
        ]);
        $request->setPagination(
            (new RequestBodyPaginationCustomerAuth())
                ->setLimit(count($userIds))
                ->setType(PaginationTypeEnumCustomerAuth::CURSOR)
        );
        $users = $this->usersApi->searchUsers($request)->getData();

        return collect($users)->keyBy('id');
    }

    /**
     * @param int $id
     * @return CustomerData
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws ApiException
     */
    public function searchById($id): CustomerData
    {
        $customer = new CustomerData($this->customersApi->getCustomer($id, $this->getInclude())->getData());
        $users = $this->loadUsers([$customer->customer->getUserId()]);
        if ($user = $users->get($customer->customer->getUserId())) {
            $customer->setUser($user);
        }

        return $customer;
    }

    /**
     * @param SearchCustomersRequest $request
     * @param null|array $id
     * @param null|string $query
     * @throws \Ensi\CustomerAuthClient\ApiException
     */
    protected function prepareEnumRequest($request, ?array $id, ?string $query)
    {
        $this->withoutUsers = true;
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['email_like'] = $query;
        }

        $request->setFilter((object) $filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
