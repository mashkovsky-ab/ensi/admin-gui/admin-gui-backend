<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Users\CreateCustomerUserAction;
use App\Domain\Customers\Actions\Users\DeleteCustomerUserAction;
use App\Domain\Customers\Actions\Users\RefreshPasswordTokenAction;
use App\Domain\Customers\Actions\Users\ReplaceCustomerUserAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersUserQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Users\CreateCustomerUserRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Users\PatchCustomerUserRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerUserResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\CustomerAuthClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class CustomersUserController
 * @package App\Http\ApiV1\Modules\Customers\Controllers
 */
class CustomersUserController
{
    /**
     * @param  CustomersUserQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(CustomersUserQuery $query): AnonymousResourceCollection
    {
        return CustomerUserResource::collectPage($query->get());
    }

    /**
     * @param  CustomersUserQuery  $query
     * @return CustomerUserResource
     */
    public function searchOne(CustomersUserQuery $query): CustomerUserResource
    {
        return new CustomerUserResource($query->first());
    }

    /**
     * @param  CreateCustomerUserRequest  $request
     * @param  CreateCustomerUserAction  $action
     * @return CustomerUserResource
     * @throws ApiException
     */
    public function create(
        CreateCustomerUserRequest $request,
        CreateCustomerUserAction $action
    ): CustomerUserResource {
        return new CustomerUserResource($action->execute($request->validated()));
    }

    /**
     * @param  int  $customerUserId
     * @param  CustomersUserQuery  $query
     * @return CustomerUserResource
     */
    public function get(int $customerUserId, CustomersUserQuery $query): CustomerUserResource
    {
        return new CustomerUserResource($query->find($customerUserId));
    }

    /**
     * @param  int  $customerUserId
     * @param  PatchCustomerUserRequest  $request
     * @param  ReplaceCustomerUserAction  $action
     * @return CustomerUserResource
     * @throws ApiException
     */
    public function patch(
        int $customerUserId,
        PatchCustomerUserRequest $request,
        ReplaceCustomerUserAction $action
    ): CustomerUserResource {
        return new CustomerUserResource($action->execute($customerUserId, $request->validated()));
    }

    /**
     * @param  int  $customerUserId
     * @param  DeleteCustomerUserAction  $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function delete(int $customerUserId, DeleteCustomerUserAction $action): EmptyResource
    {
        $action->execute($customerUserId);

        return new EmptyResource();
    }

    public function refreshPasswordToken(int $customerUserId, RefreshPasswordTokenAction $action)
    {
        $action->execute($customerUserId);

        return new EmptyResource();
    }
}
