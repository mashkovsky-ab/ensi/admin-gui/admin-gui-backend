<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Customers\CreateCustomerAction;
use App\Domain\Customers\Actions\Customers\DeleteAvatarAction;
use App\Domain\Customers\Actions\Customers\DeleteCustomerAction;
use App\Domain\Customers\Actions\Customers\ReplaceCustomerAction;
use App\Domain\Customers\Actions\Customers\UploadAvatarAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Customers\CreateOrReplaceCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Customers\UploadAvatarRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerEnumValueResource;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class CustomersController
 * @package App\Http\ApiV1\Modules\Customers\Controllers
 */
class CustomersController
{
    /**
     * @param CustomersQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(CustomersQuery $query): AnonymousResourceCollection
    {
        return CustomersResource::collectPage($query->get());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, CustomersQuery $query): AnonymousResourceCollection
    {
        return CustomerEnumValueResource::collection($query->searchEnums());
    }

    /**
     * @param CreateOrReplaceCustomerRequest $request
     * @param CreateCustomerAction $action
     * @return CustomersResource
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function create(CreateOrReplaceCustomerRequest $request, CreateCustomerAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($request->validated()));
    }

    /**
     * @param int $customerId
     * @param CustomersQuery $query
     * @return CustomersResource
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function get(int $customerId, CustomersQuery $query): CustomersResource
    {
        return new CustomersResource($query->searchById($customerId));
    }

    /**
     * @param int $customerId
     * @param CreateOrReplaceCustomerRequest $request
     * @param ReplaceCustomerAction $action
     * @return CustomersResource
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function replace(int $customerId, CreateOrReplaceCustomerRequest $request, ReplaceCustomerAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId, $request->validated()));
    }

    /**
     * @param int $customerId
     * @param DeleteCustomerAction $action
     * @return EmptyResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function delete(int $customerId, DeleteCustomerAction $action): EmptyResource
    {
        $action->execute($customerId);

        return new EmptyResource();
    }

    /**
     * @param int $customerId
     * @param UploadAvatarRequest $request
     * @param UploadAvatarAction $action
     * @return CustomersResource
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function uploadAvatar(int $customerId, UploadAvatarRequest $request, UploadAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId, $request->file('file')));
    }

    /**
     * @param int $customerId
     * @param DeleteAvatarAction $action
     * @return CustomersResource
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function deleteAvatar(int $customerId, DeleteAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId));
    }
}
