<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Statuses\CreateCustomerStatusAction;
use App\Domain\Customers\Actions\Statuses\DeleteCustomerStatusAction;
use App\Domain\Customers\Actions\Statuses\ReplaceCustomerStatusAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersStatusQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Statuses\CreateOrReplaceCustomerStatusRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersStatusesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomerStatusesController
{
    /**
     * @param CustomersStatusQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(CustomersStatusQuery $query): AnonymousResourceCollection
    {
        return CustomersStatusesResource::collectPage($query->get());
    }

    /**
     * @param CreateOrReplaceCustomerStatusRequest $request
     * @param CreateCustomerStatusAction $action
     * @return CustomersStatusesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function create(
        CreateOrReplaceCustomerStatusRequest $request,
        CreateCustomerStatusAction           $action
    ): CustomersStatusesResource {
        return new CustomersStatusesResource($action->execute($request->validated()));
    }

    /**
     * @param int $statusId
     * @param CustomersStatusQuery $query
     * @return CustomersStatusesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function get(int $statusId, CustomersStatusQuery $query): CustomersStatusesResource
    {
        return new CustomersStatusesResource($query->searchById($statusId));
    }

    /**
     * @param int $statusId
     * @param CreateOrReplaceCustomerStatusRequest $request
     * @param ReplaceCustomerStatusAction $action
     * @return CustomersStatusesResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function patch(
        int                                  $statusId,
        CreateOrReplaceCustomerStatusRequest $request,
        ReplaceCustomerStatusAction          $action
    ): CustomersStatusesResource {
        return new CustomersStatusesResource($action->execute($statusId, $request->validated()));
    }

    /**
     * @param int $statusId
     * @param DeleteCustomerStatusAction $action
     * @return EmptyResource
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function delete(int $statusId, DeleteCustomerStatusAction $action): EmptyResource
    {
        $action->execute($statusId);

        return new EmptyResource();
    }
}
