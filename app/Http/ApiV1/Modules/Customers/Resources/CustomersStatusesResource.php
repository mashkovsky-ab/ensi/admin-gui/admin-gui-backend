<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomersClient\Dto\CustomerStatuses;
use Illuminate\Http\Request;

/**
 * Class CustomersResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin CustomerStatuses
 */
class CustomersStatusesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
