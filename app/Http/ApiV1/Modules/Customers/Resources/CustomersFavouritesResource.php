<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CrmClient\Dto\CustomerFavorite;
use Illuminate\Http\Request;

/**
 * Class CustomerFavouritesResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin CustomerFavorite
 */
class CustomersFavouritesResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->getId(),
            "product_id" => $this->getProductId(),
            "customer_id" => $this->getCustomerId(),
        ];
    }
}
