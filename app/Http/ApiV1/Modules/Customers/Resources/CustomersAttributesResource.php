<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomersClient\Dto\CustomerAttributes;
use Illuminate\Http\Request;

/**
 * Class AttributesResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin CustomerAttributes
 */
class CustomersAttributesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
