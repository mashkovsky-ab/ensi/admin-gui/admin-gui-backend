<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CrmClient\Dto\BonusOperation;
use Illuminate\Http\Request;

/**
 * Class CustomersInfoResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin BonusOperation
 */
class BonusOperationResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'customer_id' => $this->getCustomerId(),
            'order_number' => $this->getOrderNumber(),
            'bonus_amount' => $this->getBonusAmount(),
            'comment' => $this->getComment(),
            'activation_date' => $this->getActivationDate(),
            'expiration_date' => $this->getExpirationDate(),
        ];
    }
}
