<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CrmClient\Dto\CustomerInfo;
use Illuminate\Http\Request;

/**
 * Class CustomersInfoResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin CustomerInfo
 */
class CustomersInfoResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'customer_id' => $this->getCustomerId(),
            'kpi_sku_count' => $this->getKpiSkuCount(),
            'kpi_sku_price_sum' => $this->getKpiSkuPriceSum(),
            'kpi_order_count' => $this->getKpiOrderCount(),
            'kpi_shipment_count' => $this->getKpiShipmentCount(),
            'kpi_delivered_count' => $this->getKpiDeliveredCount(),
            'kpi_delivered_sum' => $this->getKpiDeliveredSum(),
            'kpi_refunded_count' => $this->getKpiRefundedCount(),
            'kpi_refunded_sum' => $this->getKpiRefundedSum(),
            'kpi_canceled_count' => $this->getKpiCanceledCount(),
            'kpi_canceled_sum' => $this->getKpiCanceledSum(),
        ];
    }
}
