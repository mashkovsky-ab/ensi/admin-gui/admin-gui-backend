<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Data\CustomerData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class CustomersResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin CustomerData
 */
class CustomersResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->customer->getId(),
            'user_id' => $this->customer->getUserId(),
            'manager_id' => $this->customer->getManagerId(),
            'yandex_metric_id' => $this->customer->getYandexMetricId(),
            'google_analytics_id' => $this->customer->getGoogleAnalyticsId(),
            'status_id' => $this->customer->getStatusId(),
            'active' => $this->customer->getActive(),
            'email' => $this->customer->getEmail(),
            'phone' => $this->customer->getPhone(),
            'first_name' => $this->customer->getFirstName(),
            'last_name' => $this->customer->getLastName(),
            'middle_name' => $this->customer->getMiddleName(),
            'gender' => $this->customer->getGender(),
            'create_by_admin' => $this->customer->getCreateByAdmin(),
            "avatar" => $this->fileUrl($this->customer->getAvatar()),
            'city' => $this->customer->getCity(),
            'birthday' => $this->dateToIso($this->customer->getBirthday()),
            'last_visit_date' => $this->dateTimeToIso($this->customer->getLastVisitDate()),
            'comment_status' => $this->customer->getCommentStatus(),
            'timezone' => $this->customer->getTimezone(),

            'addresses' => CustomersAddressesResource::collection($this->whenNotNull($this->customer->getAddresses())),
            'status' => new CustomersStatusesResource($this->whenNotNull($this->customer->getStatus())),
            'attributes' => CustomersAttributesResource::collection($this->whenNotNull($this->customer->getAttributes())),
            'user' => new CustomerUserResource($this->whenNotNull($this->user)),
        ];
    }
}
