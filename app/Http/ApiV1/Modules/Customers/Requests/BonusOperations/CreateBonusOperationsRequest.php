<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\BonusOperations;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateBonusOperationsRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'order_number' => ['nullable', 'integer'],
            'bonus_amount' => ['required', 'numeric'],
            'comment' => ['nullable', 'string'],
            'activation_date' => ['nullable', 'date'],
            'expiration_date' => ['nullable', 'date'],
        ];
    }
}
