<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Attributes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceCustomerAttributeRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
        ];
    }
}
