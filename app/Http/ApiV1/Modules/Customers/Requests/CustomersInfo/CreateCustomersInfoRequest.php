<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\CustomersInfo;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateCustomersInfoRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'kpi_sku_count' => ['nullable', 'integer'],
            'kpi_sku_price_sum' => ['nullable', 'integer'],
            'kpi_order_count' => ['nullable', 'integer'],
            'kpi_shipment_count' => ['nullable', 'integer'],
            'kpi_delivered_count' => ['nullable', 'integer'],
            'kpi_delivered_sum' => ['nullable', 'integer'],
            'kpi_refunded_count' => ['nullable', 'integer'],
            'kpi_refunded_sum' => ['nullable', 'integer'],
            'kpi_canceled_count' => ['nullable', 'integer'],
            'kpi_canceled_sum' => ['nullable', 'integer'],
        ];
    }
}
