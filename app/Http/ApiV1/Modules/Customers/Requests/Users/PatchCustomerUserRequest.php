<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Users;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class PatchCustomerUserRequest
 * @package App\Http\ApiV1\Modules\Customers\Requests\Users
 */
class PatchCustomerUserRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'login' => ['nullable'],
            'active' => ['nullable', 'boolean'],
            'password' => ['nullable'],
        ];
    }
}
