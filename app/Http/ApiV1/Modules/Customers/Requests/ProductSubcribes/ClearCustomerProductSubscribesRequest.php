<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\ProductSubcribes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ClearCustomerProductSubscribesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
        ];
    }
}
