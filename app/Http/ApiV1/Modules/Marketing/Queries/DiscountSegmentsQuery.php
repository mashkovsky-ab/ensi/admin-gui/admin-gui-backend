<?php


namespace App\Http\ApiV1\Modules\Marketing\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\MarketingClient\Api\DiscountSegmentsApi;
use Ensi\MarketingClient\Dto\RequestBodyPagination;
use Ensi\MarketingClient\Dto\SearchDiscountsRequest as SearchDiscountsRequestMarketing;
use Illuminate\Http\Request;

class DiscountSegmentsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected DiscountSegmentsApi $discountSegmentsApi
    ) {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchDiscountsRequestMarketing::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function search($request)
    {
        return $this->discountSegmentsApi->searchDiscountSegments($request);
    }

    protected function searchById($id)
    {
        return $this->discountSegmentsApi->getDiscountSegment($id);
    }
}
