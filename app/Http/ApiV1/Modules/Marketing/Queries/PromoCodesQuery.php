<?php


namespace App\Http\ApiV1\Modules\Marketing\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\RequestBodyPagination;
use Ensi\MarketingClient\Dto\SearchPromoCodesRequest as SearchPromoCodesRequestMarketing;
use Illuminate\Http\Request;

class PromoCodesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected PromoCodesApi $discountsApi
    ) {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchPromoCodesRequestMarketing::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function search($request)
    {
        return $this->discountsApi->searchPromoCodes($request);
    }

    protected function searchById($id)
    {
        return $this->discountsApi->getPromoCode($id);
    }
}
