<?php


namespace App\Http\ApiV1\Modules\Marketing\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\MarketingClient\Api\DiscountBrandsApi;
use Ensi\MarketingClient\Dto\RequestBodyPagination;
use Ensi\MarketingClient\Dto\SearchDiscountsRequest as SearchDiscountsRequestMarketing;
use Illuminate\Http\Request;

class DiscountBrandsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected DiscountBrandsApi $discountBrandsApi
    ) {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchDiscountsRequestMarketing::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function search($request)
    {
        return $this->discountBrandsApi->searchDiscountBrands($request);
    }

    protected function searchById($id)
    {
        return $this->discountBrandsApi->getDiscountBrand($id);
    }
}
