<?php


namespace App\Http\ApiV1\Modules\Marketing\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\MarketingClient\Api\DiscountCategoriesApi;
use Ensi\MarketingClient\Dto\RequestBodyPagination;
use Ensi\MarketingClient\Dto\SearchDiscountsRequest as SearchDiscountsRequestMarketing;
use Illuminate\Http\Request;

class DiscountCategoriesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected DiscountCategoriesApi $discountCategoriesApi
    ) {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchDiscountsRequestMarketing::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function search($request)
    {
        return $this->discountCategoriesApi->searchDiscountCategories($request);
    }

    protected function searchById($id)
    {
        return $this->discountCategoriesApi->getDiscountCategory($id);
    }
}
