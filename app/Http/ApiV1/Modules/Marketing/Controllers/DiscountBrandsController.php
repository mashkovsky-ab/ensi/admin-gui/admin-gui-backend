<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\Discounts\CreateDiscountBrandAction;
use App\Domain\Marketing\Actions\Discounts\DeleteDiscountBrandAction;
use App\Domain\Marketing\Actions\Discounts\PatchDiscountBrandAction;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountBrandsQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateDiscountBrandRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountBrandRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountBrandsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DiscountBrandsController
{
    public function findById(int $discountBrandId, DiscountBrandsQuery $query)
    {
        return new DiscountBrandsResource($query->find($discountBrandId));
    }

    public function search(DiscountBrandsQuery $query)
    {
        return DiscountBrandsResource::collectPage($query->get());
    }

    public function create(
        CreateDiscountBrandRequest $request,
        CreateDiscountBrandAction $action
    ): DiscountBrandsResource {
        return new DiscountBrandsResource($action->execute($request->validated()));
    }

    public function patch(
        int $discountBrandId,
        PatchDiscountBrandRequest $request,
        PatchDiscountBrandAction $action
    ): DiscountBrandsResource {
        return new DiscountBrandsResource($action->execute($discountBrandId, $request->validated()));
    }

    public function delete(int $discountBrandId, DeleteDiscountBrandAction $action): EmptyResource
    {
        $action->execute($discountBrandId);

        return new EmptyResource();
    }
}
