<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\Discounts\CreateDiscountCategoryAction;
use App\Domain\Marketing\Actions\Discounts\DeleteDiscountCategoryAction;
use App\Domain\Marketing\Actions\Discounts\PatchDiscountCategoryAction;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountCategoriesQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateDiscountCategoryRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountCategoryRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountCategoriesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DiscountCategoriesController
{
    public function findById(int $discountCategoryId, DiscountCategoriesQuery $query)
    {
        return new DiscountCategoriesResource($query->find($discountCategoryId));
    }

    public function search(DiscountCategoriesQuery $query)
    {
        return DiscountCategoriesResource::collectPage($query->get());
    }

    public function create(CreateDiscountCategoryRequest $request, CreateDiscountCategoryAction $action): DiscountCategoriesResource
    {
        return new DiscountCategoriesResource($action->execute($request->validated()));
    }

    public function patch(
        int $discountCategoryId,
        PatchDiscountCategoryRequest $request,
        PatchDiscountCategoryAction $action
    ): DiscountCategoriesResource {
        return new DiscountCategoriesResource($action->execute($discountCategoryId, $request->validated()));
    }

    public function delete(int $discountCategoryId, DeleteDiscountCategoryAction $action): EmptyResource
    {
        $action->execute($discountCategoryId);

        return new EmptyResource();
    }
}
