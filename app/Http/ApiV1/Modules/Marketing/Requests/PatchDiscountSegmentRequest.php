<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDiscountSegmentRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'segment_id' => ['integer'],
        ];
    }
}
