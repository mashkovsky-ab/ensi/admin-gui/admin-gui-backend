<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateDiscountCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'discount_id' => ['required', 'integer'],
            'category_id' => ['required', 'integer'],
        ];
    }
}
