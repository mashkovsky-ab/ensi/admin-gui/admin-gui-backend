<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\MarketingDiscountConditionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\MarketingDiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\MarketingDiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\MarketingDiscountValueTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateDiscountRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['required', 'integer', Rule::in(MarketingDiscountTypeEnum::cases())],
            'name' => ['required', 'string'],
            'value_type' => ['required', 'integer', Rule::in(MarketingDiscountValueTypeEnum::cases())],
            'value' => ['required', 'integer'],
            'status' => ['required', 'integer', Rule::in(MarketingDiscountStatusEnum::cases())],
            'start_date' => ['nullable', 'string'],
            'end_date' => ['nullable', 'string'],
            'promo_code_only' => ['required', 'boolean'],

            /** OffersResource **/
            'offers' => [
                'required_if:type,' . MarketingDiscountTypeEnum::DISCOUNT_TYPE_OFFER,
                'array',
            ],
            'offers.*.offer_id' => ['required_with:offers', 'integer'],
            'offers.*.except' => ['required_with:offers', 'boolean'],

            /** BrandsResource **/
            'brands' => [
                'required_if:type,' . MarketingDiscountTypeEnum::DISCOUNT_TYPE_BRAND,
                'array',
            ],
            'brands.*.brand_id' => ['required_with:brands', 'integer'],
            'brands.*.except' => ['required_with:brands', 'boolean'],

            /** CategoriesResource **/
            'categories' => [
                'required_if:type,' . MarketingDiscountTypeEnum::DISCOUNT_TYPE_CATEGORY,
                'array',
            ],
            'categories.*.category_id' => ['required_with:categories', 'integer'],

            /** SegmentsResource **/
            'segments' => ['nullable', 'array'],
            'segments.*.segment_id' => ['required_with:segments', 'integer'],

            /** ConditionsResource **/
            'conditions' => ['nullable', 'array'],
            'conditions.*.type' => [
                'required_with:conditions',
                'integer',
                Rule::in(MarketingDiscountConditionTypeEnum::cases()),
            ],
            'conditions.*.condition' => ['required_with:conditions', 'array'],
        ];
    }
}
