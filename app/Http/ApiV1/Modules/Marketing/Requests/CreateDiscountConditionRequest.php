<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\MarketingDiscountConditionTypeEnum as TypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\MarketingDiscountConditionTypePropEnum as KeyEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateDiscountConditionRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'discount_id' => ['required', 'integer'],
            'type' => ['required', 'integer', Rule::in(TypeEnum::cases())],
            'condition' => ['nullable', 'array', 'required_unless:type,' . TypeEnum::MIN_PRICE_ORDER],
            'condition.*' => Rule::in(KeyEnum::cases()),

            /** Condition Min Price */
            'condition.' . KeyEnum::FIELD_MIN_PRICE => [
                'integer',
                'gt:0',
                'required_if:type,' . TypeEnum::MIN_PRICE_ORDER,
                'exclude_unless:type,' . TypeEnum::MIN_PRICE_ORDER,
            ],

            /** Condition Brands */
            'condition.' . KeyEnum::FIELD_BRANDS => [
                'array',
                'required_if:type,' . TypeEnum::MIN_PRICE_BRAND,
                'exclude_unless:type,' . TypeEnum::MIN_PRICE_BRAND,
            ],
            'condition.' . KeyEnum::FIELD_BRANDS . '.*' => ['integer'],

            /** Condition Categories */
            'condition.' . KeyEnum::FIELD_CATEGORIES => [
                'array',
                'required_if:type,' . TypeEnum::MIN_PRICE_CATEGORY,
                'exclude_unless:type,' . TypeEnum::MIN_PRICE_CATEGORY,
            ],
            'condition.' . KeyEnum::FIELD_CATEGORIES . '.*' => ['integer'],

            /** Condition Count */
            'condition.' . KeyEnum::FIELD_COUNT => [
                'integer',
                'gt:0',
                'required_if:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
                'exclude_unless:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
            ],
            'condition.' . KeyEnum::FIELD_OFFER => [
                'integer',
                'required_if:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
                'exclude_unless:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
            ],

            /** Condition Delivery Methods */
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS => [
                'array',
                'required_if:type,' . TypeEnum::DELIVERY_METHOD,
                'exclude_unless:type,' . TypeEnum::DELIVERY_METHOD,
            ],
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS . '.*' => ['integer'],

            /** Condition Payment Methods */
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS => [
                'array',
                'required_if:type,' . TypeEnum::PAY_METHOD,
                'exclude_unless:type,' . TypeEnum::PAY_METHOD,
            ],
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS . '.*' => ['integer'],
        ];
    }
}
