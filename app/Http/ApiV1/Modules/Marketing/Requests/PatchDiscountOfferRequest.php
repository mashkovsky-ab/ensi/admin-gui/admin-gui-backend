<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDiscountOfferRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'offer_id' => ['integer'],
            'except' => ['boolean'],
        ];
    }
}
