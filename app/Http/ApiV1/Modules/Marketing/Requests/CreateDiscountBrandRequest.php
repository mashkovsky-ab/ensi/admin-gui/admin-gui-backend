<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateDiscountBrandRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'discount_id' => ['required', 'integer'],
            'brand_id' => ['required', 'integer'],
            'except' => ['required', 'boolean'],
        ];
    }
}
