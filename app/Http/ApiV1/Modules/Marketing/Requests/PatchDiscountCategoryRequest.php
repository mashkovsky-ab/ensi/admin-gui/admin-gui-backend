<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDiscountCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'category_id' => ['integer'],
        ];
    }
}
