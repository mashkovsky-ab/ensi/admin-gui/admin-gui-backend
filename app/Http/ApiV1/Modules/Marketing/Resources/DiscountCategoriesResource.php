<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountCategory;

/**
 * Class DiscountCategoriesResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountCategory
 */
class DiscountCategoriesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'discount_id' => $this->getDiscountId(),
            'category_id' => $this->getCategoryId(),
        ];
    }
}
