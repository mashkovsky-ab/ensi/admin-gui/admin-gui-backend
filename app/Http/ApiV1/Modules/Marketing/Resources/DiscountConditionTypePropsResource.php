<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountConditionTypeProp;

/**
 * Class DiscountConditionTypesResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountConditionTypeProp
 */
class DiscountConditionTypePropsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'type' => $this->getType(),
        ];
    }
}
