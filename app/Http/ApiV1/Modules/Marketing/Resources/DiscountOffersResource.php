<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountOffer;

/**
 * Class DiscountOffersResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountOffer
 */
class DiscountOffersResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'discount_id' => $this->getDiscountId(),
            'offer_id' => $this->getOfferId(),
            'except' => $this->getExcept(),
        ];
    }
}
