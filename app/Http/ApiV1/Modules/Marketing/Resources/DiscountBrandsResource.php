<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountBrand;

/**
 * Class DiscountBrandsResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountBrand
 */
class DiscountBrandsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'discount_id' => $this->getDiscountId(),
            'brand_id' => $this->getBrandId(),
            'except' => $this->getExcept(),
        ];
    }
}
