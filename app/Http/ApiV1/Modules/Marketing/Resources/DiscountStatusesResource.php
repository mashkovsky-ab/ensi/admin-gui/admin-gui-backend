<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountStatus;

/**
 * Class DeliveryStatusesResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountStatus
 */
class DiscountStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
