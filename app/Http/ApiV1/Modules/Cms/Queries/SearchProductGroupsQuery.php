<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\ProductGroupsApi;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchOneProductGroupsRequest;
use Ensi\CmsClient\Dto\SearchProductGroupsRequest;
use Illuminate\Http\Request;

class SearchProductGroupsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFirstTrait;

    public function __construct(
        Request $httpRequest,
        protected ProductGroupsApi $productGroupsApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchProductGroupsRequest::class;
    }

    protected function requestFirstClass(): string
    {
        return SearchOneProductGroupsRequest::class;
    }

    protected function search($request)
    {
        return $this->productGroupsApi->searchProductGroups($request);
    }

    protected function searchOne($request)
    {
        return $this->productGroupsApi->searchOneProductGroups($request);
    }
}
