<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\SearchPropertiesRequest;
use Illuminate\Http\Request;

class SearchProductGroupFiltersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        Request $httpRequest,
        protected PropertiesApi $api,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchPropertiesRequest::class;
    }

    protected function search($request)
    {
        return $this->api->searchProperties($request);
    }
}
