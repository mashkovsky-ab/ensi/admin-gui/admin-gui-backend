<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Contents\Actions\DeleteBannerFileAction;
use App\Domain\Contents\Actions\UploadBannerFileAction;
use App\Http\ApiV1\Modules\Cms\Requests\DeleteBannerFileRequest;
use App\Http\ApiV1\Modules\Cms\Requests\UploadBannerFileRequest;
use App\Http\ApiV1\Modules\Cms\Resources\BannerFilesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class BannerFilesController
{
    public function upload($id, UploadBannerFileRequest $request, UploadBannerFileAction $action)
    {
        $file = $request->file('file');
        $type = $request->get('type');

        return new BannerFilesResource($action->execute($id, $type, $file));
    }

    public function delete($id, DeleteBannerFileRequest $request, DeleteBannerFileAction $action)
    {
        $type = $request->get('type');

        $action->execute($id, $type);

        return new EmptyResource();
    }
}
