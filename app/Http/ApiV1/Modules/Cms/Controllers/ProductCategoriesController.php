<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Contents\Actions\CreateProductCategoriesAction;
use App\Domain\Contents\Actions\DeleteProductCategoriesAction;
use App\Domain\Contents\Actions\MassDeleteProductCategoriesAction;
use App\Domain\Contents\Actions\ReplaceProductCategoriesAction;
use App\Http\ApiV1\Modules\Cms\Queries\ProductCategoriesQuery;
use App\Http\ApiV1\Modules\Cms\Queries\ProductCategoriesTreeQuery;
use App\Http\ApiV1\Modules\Cms\Requests\CreateOrReplaceProductCategoriesRequest;
use App\Http\ApiV1\Modules\Cms\Requests\MassDeleteProductCategoriesRequest;
use App\Http\ApiV1\Modules\Cms\Resources\ProductCategoriesResource;
use App\Http\ApiV1\Modules\Cms\Resources\ProductCategoriesTreeResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductCategoriesController
{
    public function create(CreateOrReplaceProductCategoriesRequest $request, CreateProductCategoriesAction $action)
    {
        return new ProductCategoriesResource($action->execute($request->validated()));
    }

    public function replace(int $id, CreateOrReplaceProductCategoriesRequest $request, ReplaceProductCategoriesAction $action)
    {
        return new ProductCategoriesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductCategoriesAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteProductCategoriesRequest $request, MassDeleteProductCategoriesAction $action)
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function search(ProductCategoriesQuery $query)
    {
        return ProductCategoriesResource::collectPage($query->get());
    }

    public function get(int $id, ProductCategoriesQuery $query)
    {
        return new ProductCategoriesResource($query->find($id));
    }

    public function tree(ProductCategoriesTreeQuery $query): AnonymousResourceCollection
    {
        return ProductCategoriesTreeResource::collection($query->get());
    }
}
