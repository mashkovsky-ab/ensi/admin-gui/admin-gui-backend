<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Contents\Actions\CreateProductPimCategoriesAction;
use App\Domain\Contents\Actions\DeleteProductPimCategoriesAction;
use App\Domain\Contents\Actions\ReplaceProductPimCategoriesAction;
use App\Http\ApiV1\Modules\Cms\Queries\ProductPimCategoriesQuery;
use App\Http\ApiV1\Modules\Cms\Requests\CreateOrReplaceProductPimCategoriesRequest;
use App\Http\ApiV1\Modules\Cms\Resources\ProductPimCategoriesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ProductPimCategoriesController
{
    public function create(CreateOrReplaceProductPimCategoriesRequest $request, CreateProductPimCategoriesAction $action)
    {
        return new ProductPimCategoriesResource($action->execute($request->validated()));
    }

    public function replace(int $id, CreateOrReplaceProductPimCategoriesRequest $request, ReplaceProductPimCategoriesAction $action)
    {
        return new ProductPimCategoriesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductPimCategoriesAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(ProductPimCategoriesQuery $query)
    {
        return ProductPimCategoriesResource::collectPage($query->get());
    }

    public function get(int $id, ProductPimCategoriesQuery $query)
    {
        return new ProductPimCategoriesResource($query->find($id));
    }
}
