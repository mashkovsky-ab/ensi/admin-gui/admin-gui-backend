<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Contents\Actions\ReplaceMenuTreesAction;
use App\Http\ApiV1\Modules\Cms\Requests\ReplaceMenuTreesRequest;
use App\Http\ApiV1\Modules\Cms\Resources\MenuTreesResource;

class MenuTreesController
{
    public function update(int $id, ReplaceMenuTreesRequest $request, ReplaceMenuTreesAction $action)
    {
        return MenuTreesResource::collection($action->execute($id, $request->validated()));
    }
}
