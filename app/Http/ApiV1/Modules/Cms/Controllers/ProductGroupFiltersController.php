<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\SearchProductGroupFiltersByCategoryQuery;
use App\Http\ApiV1\Modules\Cms\Queries\SearchProductGroupFiltersQuery;
use App\Http\ApiV1\Modules\Cms\Resources\ProductGroupFiltersResource;
use Illuminate\Http\Request;

class ProductGroupFiltersController
{
    public function __construct(
        protected SearchProductGroupFiltersQuery $searchFiltersQuery,
        protected SearchProductGroupFiltersByCategoryQuery $searchFiltersByCategoryQuery
    ) {
    }

    public function search(Request $request)
    {
        if (!is_null($request->get('category'))) {
            // Список для конкретной категории
            return ProductGroupFiltersResource::collectPage($this->searchFiltersByCategoryQuery->get());
        }

        // Общий список
        return ProductGroupFiltersResource::collectPage($this->searchFiltersQuery->get());
    }
}
