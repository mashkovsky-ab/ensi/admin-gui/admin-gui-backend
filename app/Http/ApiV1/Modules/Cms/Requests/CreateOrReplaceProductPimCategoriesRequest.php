<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceProductPimCategoriesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'code' => ['required', 'string'],
            'product_category_id' => ['required', 'integer'],
            'filters' => ['array'],
            'filters.*.code' => ['string', 'required_with:filters'],
            'filters.*.value' => ['string', 'required_with:filters'],
        ];
    }
}
