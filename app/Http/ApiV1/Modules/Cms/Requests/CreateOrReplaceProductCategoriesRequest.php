<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceProductCategoriesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'url' => ['required', 'string'],
            'active' => ['required', 'bool'],
            'order' => ['nullable', 'integer'],
            'parent_id' => ['nullable', 'integer'],

            'pim_categories' => ['array'],
            'pim_categories.*.code' => ['required_with:pim_categories', 'string'],
            'pim_categories.*.filters' => ['array'],
            'pim_categories.*.filters.*.code' => ['string', 'required_with:pim_categories.*.filters'],
            'pim_categories.*.filters.*.value' => ['string', 'required_with:pim_categories.*.filters'],
        ];
    }
}
