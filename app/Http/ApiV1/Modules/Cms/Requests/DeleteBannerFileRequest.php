<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CmsClient\Dto\BannerImageTypeEnum;
use Illuminate\Validation\Rule;

class DeleteBannerFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['required', Rule::in(BannerImageTypeEnum::getAllowableEnumValues())],
        ];
    }
}
