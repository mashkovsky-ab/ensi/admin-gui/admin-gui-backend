<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceBannersRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'active' => ['boolean', 'required'],
            'url' => ['string', 'nullable'],
            'type_id' => ['integer', 'required'],
            'button_id' => ['integer', 'nullable'],
            'button' => ['nullable'],
            'button.url' => ['string', 'required_with:button'],
            'button.text' => ['string', 'required_with:button'],
            'button.location' => ['string', 'required_with:button'],
            'button.type' => ['string', 'required_with:button'],
        ];
    }
}
