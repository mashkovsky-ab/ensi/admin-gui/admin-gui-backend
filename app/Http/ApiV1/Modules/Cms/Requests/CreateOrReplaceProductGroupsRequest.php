<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceProductGroupsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'code' => ['string', 'required'],
            'active' => ['boolean', 'required'],
            'is_shown' => ['boolean', 'required'],
            'type_id' => ['integer', 'required'],
            'banner_id' => ['integer', 'nullable'],
            'category_code' => ['string', 'nullable'],
            'products' => ['array'],
            'products.*.product_id' => ['integer', 'min:0', 'required_with:products'],
            'products.*.sort' => ['integer', 'min:0', 'required_with:products'],
            'filters' => ['array'],
            'filters.*.code' => ['string', 'required_with:filters'],
            'filters.*.value' => ['string', 'required_with:filters'],
        ];
    }
}
