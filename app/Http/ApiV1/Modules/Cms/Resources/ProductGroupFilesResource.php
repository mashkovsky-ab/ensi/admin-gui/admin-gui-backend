<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\ProductGroupFile;
use Illuminate\Http\Request;

/** @mixin ProductGroupFile */
class ProductGroupFilesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'url' => $this->getUrl(),
        ];
    }
}
