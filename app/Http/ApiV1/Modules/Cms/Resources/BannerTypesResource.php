<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\BannerType;
use Illuminate\Http\Request;

/** @mixin BannerType */
class BannerTypesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'active' => $this->getActive(),
        ];
    }
}
