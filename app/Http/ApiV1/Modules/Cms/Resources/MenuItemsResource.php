<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\MenuItem;
use Illuminate\Http\Request;

/** @mixin MenuItem */
class MenuItemsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'url' => $this->getUrl(),
            'name' => $this->getName(),
            'menu_id' => $this->getMenuId(),
            '_lft' => $this->getLft(),
            '_rgt' => $this->getRgt(),
            'parent_id' => $this->getParentId(),
            'active' => $this->getActive(),
        ];
    }
}
