<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\Banner;
use Illuminate\Http\Request;

/** @mixin Banner */
class BannersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'active' => $this->getActive(),
            'url' => $this->getUrl(),
            'desktop_image' => $this->fileUrl($this->getDesktopImage()),
            'tablet_image' => $this->fileUrl($this->getTabletImage()),
            'mobile_image' => $this->fileUrl($this->getMobileImage()),
            'type_id' => $this->getTypeId(),
            'button_id' => $this->getButtonId(),
            'type' => BannerTypesResource::make($this->whenNotNull($this->getType())),
            'button' => BannerButtonsResource::make($this->whenNotNull($this->getButton())),
        ];
    }
}
