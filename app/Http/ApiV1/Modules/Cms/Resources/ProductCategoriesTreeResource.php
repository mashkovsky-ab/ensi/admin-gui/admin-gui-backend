<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\ProductCategoriesTreeItem;

/**
 * @mixin ProductCategoriesTreeItem
 */
class ProductCategoriesTreeResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'url' => $this->getUrl(),
            'active' => $this->getActive(),
            'order' => $this->getOrder(),
            'children' => $this->getChildren(),
        ];
    }
}
