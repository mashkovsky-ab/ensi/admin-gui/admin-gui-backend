<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use Ensi\CommunicationManagerClient\Api\StatusesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\SearchStatusesRequest;
use Ensi\CommunicationManagerClient\Dto\SearchStatusesResponse;
use Illuminate\Http\Request;

class StatusesQuery extends CommunicationQuery
{
    public function __construct(Request $request, private StatusesApi $api)
    {
        parent::__construct($request, SearchStatusesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchStatusesResponse
    {
        return $this->api->searchStatuses($request);
    }
}
