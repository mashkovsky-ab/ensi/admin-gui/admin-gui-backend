<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CommunicationManagerClient\Api\ChannelsApi;
use Ensi\CommunicationManagerClient\Dto\RequestBodyPagination;
use Ensi\CommunicationManagerClient\Dto\SearchChannelsRequest;
use Illuminate\Http\Request;

class SearchChannelsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        Request $httpRequest,
        protected ChannelsApi $channelsApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchChannelsRequest::class;
    }

    protected function search($request)
    {
        return $this->channelsApi->searchChannels($request);
    }
}
