<?php

namespace App\Http\ApiV1\Modules\Communication\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\InternalMessenger\Dto\UploadAttachmentResponseData;

/** @mixin UploadAttachmentResponseData */
class FilesResource extends BaseJsonResource
{
    public function toArray($request)
    {
        // @todo должно быть переработано в рамках 79636, сейчас все поля оставляю просто для обратной совместимости с фронтом
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'path' => $this->getFile()->getPath(),
            'disk' => 'protected',
            'url' => $this->getFile()->getUrl(),
        ];
    }
}
