<?php


namespace App\Http\ApiV1\Modules\Communication\Resources;

use App\Domain\Communication\Data\ChatData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class ChatsResource
 * @package App\Http\ApiV1\Modules\Communication\Resources
 * @mixin ChatData
 */
class ChatsResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->chat->getId(),
            "user_id" => $this->chat->getUserId(),
            "user_type" => $this->chat->getUserType(),
            "theme" => $this->chat->getTheme(),
            "type_id" => $this->chat->getTypeId(),
            "messages" => MessagesResource::collection($this->whenNotNull($this->getMessages())),
        ];
    }
}
