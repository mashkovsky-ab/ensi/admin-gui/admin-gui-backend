<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateStatusAction;
use App\Domain\Communication\Actions\DeleteStatusAction;
use App\Domain\Communication\Actions\PatchStatusAction;
use App\Http\ApiV1\Modules\Communication\Queries\StatusesQuery;
use App\Http\ApiV1\Modules\Communication\Requests\CreateStatusRequest;
use App\Http\ApiV1\Modules\Communication\Requests\PatchStatusRequest;
use App\Http\ApiV1\Modules\Communication\Resources\StatusesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class StatusesController
{
    public function search(StatusesQuery $query)
    {
        return StatusesResource::collectPage($query->get());
    }

    public function create(CreateStatusRequest $request, CreateStatusAction $action)
    {
        return new StatusesResource($action->execute($request->validated()));
    }

    public function patch(int $statusId, PatchStatusRequest $request, PatchStatusAction $action)
    {
        return new StatusesResource($action->execute($statusId, $request->validated()));
    }

    public function delete(int $statusId, DeleteStatusAction $action)
    {
        $action->execute($statusId);

        return new EmptyResource();
    }
}
