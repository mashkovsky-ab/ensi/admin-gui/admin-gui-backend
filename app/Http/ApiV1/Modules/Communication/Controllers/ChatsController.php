<?php


namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateChatAction;
use App\Domain\Communication\Actions\PatchChatAction;
use App\Http\ApiV1\Modules\Communication\Queries\ChatsQuery;
use App\Http\ApiV1\Modules\Communication\Requests\CreateChatRequest;
use App\Http\ApiV1\Modules\Communication\Requests\PatchChatRequest;
use App\Http\ApiV1\Modules\Communication\Resources\ChatsResource;

class ChatsController
{
    public function search(ChatsQuery $query)
    {
        return ChatsResource::collectPage($query->get());
    }

    public function create(CreateChatAction $action, CreateChatRequest $request)
    {
        return new ChatsResource($action->execute($request->validated()));
    }

    public function patch(int $chatId, PatchChatAction $action, PatchChatRequest $request)
    {
        return new ChatsResource($action->execute($chatId, $request->validated()));
    }
}
