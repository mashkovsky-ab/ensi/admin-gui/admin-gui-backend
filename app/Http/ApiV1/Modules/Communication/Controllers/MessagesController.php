<?php


namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateMessageAction;
use App\Http\ApiV1\Modules\Communication\Queries\MessagesQuery;
use App\Http\ApiV1\Modules\Communication\Requests\CreateMessageRequest;
use App\Http\ApiV1\Modules\Communication\Resources\MessagesResource;

class MessagesController
{
    public function search(MessagesQuery $query)
    {
        return MessagesResource::collectPage($query->get());
    }

    public function create(CreateMessageAction $action, CreateMessageRequest $request)
    {
        return new MessagesResource($action->execute($request->validated()));
    }
}
