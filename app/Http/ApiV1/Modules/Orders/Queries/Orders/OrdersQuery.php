<?php


namespace App\Http\ApiV1\Modules\Orders\Queries\Orders;

use App\Domain\Common\Actions\AsyncLoadAction;
use App\Domain\Customers\Data\CustomerData;
use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderAdminUsersLoader;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderCustomersLoader;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderCustomerUsersLoader;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderOffersLoader;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderProductsLoader;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderSellersLoader;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderStoresLoader;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\RequestBodyPagination;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class OrdersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    protected bool $isShipmentsOrderItems = false;

    public function __construct(
        protected Request $httpRequest,
        protected OrdersApi $ordersApi,
        protected AsyncLoadAction $asyncLoadAction,
        protected OrderCustomersLoader $customersLoader,
        protected OrderCustomerUsersLoader $customerUsersLoader,
        protected OrderAdminUsersLoader $adminUsersLoader,
        protected OrderOffersLoader $offersLoader,
        protected OrderProductsLoader $productsLoader,
        protected OrderSellersLoader $sellersLoader,
        protected OrderStoresLoader $storesLoader,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchOrdersRequest::class;
    }

    protected function search($request)
    {
        return $this->ordersApi->searchOrders($request);
    }

    protected function searchById($id)
    {
        return $this->ordersApi->getOrder($id, $this->getInclude());
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertFindToItem($response)
    {
        return current($this->convertArray([$response->getData()]));
    }

    protected function convertArray(array $orders): array
    {
        /** @var Collection|Order[] $orders */
        $orders = collect($orders);

        $this->customersLoader->customerIds = $this->prepareFilterValues($orders->pluck('customer_id'));
        $this->adminUsersLoader->userIds = $this->prepareFilterValues($orders->pluck('responsible_id'));
        $this->offersLoader->offerIds = $this->prepareFilterValues($this->getOrderItems($orders)->pluck('offer_id'));
        $shipments = $this->getOrderShipments($orders);
        $this->sellersLoader->sellerIds = $this->prepareFilterValues($shipments->pluck('seller_id'));
        $this->storesLoader->storeIds = $this->prepareFilterValues($shipments->pluck('store_id'));

        $this->asyncLoadAction->execute([
            $this->customersLoader,
            $this->adminUsersLoader,
            $this->offersLoader,
            $this->sellersLoader,
            $this->storesLoader,
        ]);

        $this->productsLoader->productIds = array_values(array_unique(Arr::pluck($this->offersLoader->offersData, 'offer.product_id')));
        $this->customerUsersLoader->userIds = $this->prepareFilterValues($this->customersLoader->customers->pluck('user_id'));

        $this->asyncLoadAction->execute([
            $this->productsLoader,
            $this->customerUsersLoader,
        ]);

        $ordersData = [];
        foreach ($orders as $order) {
            $orderData = new OrderData($order);

            /** @var Customer $customer */
            $customer = $this->customersLoader->customers->get($order->getCustomerId());
            if ($customer) {
                $orderData->customer = new CustomerData($customer);
                $orderData->customer->user = $this->customerUsersLoader->users->get($customer->getUserId());
            }
            $orderData->offersData = $this->offersLoader->offersData;
            $orderData->products = $this->productsLoader->products;
            $orderData->sellersData = $this->sellersLoader->sellersData;
            $orderData->stores = $this->storesLoader->stores;

            $orderData->responsible = $order->getResponsibleId() ? $this->adminUsersLoader->users->get($order->getResponsibleId()) : null;

            $ordersData[] = $orderData;
        }

        return $ordersData;
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "customer":
                    $this->customersLoader->load = true;

                    break;
                case "customer.user":
                    $this->customerUsersLoader->load = true;
                    $this->customersLoader->load = true;

                    break;
                case "orderItems.product":
                    $this->productsLoader->load = true;
                    $this->offersLoader->load = true;

                    break;
                case "orderItems.product.images":
                    $this->productsLoader->loadProductImage();
                    $this->offersLoader->load = true;

                    break;
                case "orderItems.product.category":
                    $this->productsLoader->loadProductCategory();
                    $this->offersLoader->load = true;

                    break;
                case "orderItems.product.brand":
                    $this->productsLoader->loadProductBrand();
                    $this->offersLoader->load = true;

                    break;
                case "orderItems.stock":
                    $this->offersLoader->loadStock();

                    break;
                case "deliveries.shipments.orderItems":
                    $this->include[] = 'deliveries.shipments.orderItems';
                    $this->isShipmentsOrderItems = true;

                    break;
                case "responsible":
                    $this->adminUsersLoader->load = true;

                    break;
                case "deliveries.shipments.seller":
                    $this->sellersLoader->load = true;
                    $includes[] = "deliveries.shipments";

                    break;
                case "deliveries.shipments.store":
                    $this->storesLoader->load = true;
                    $includes[] = "deliveries.shipments";

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }

    protected function getOrderItems(Collection $orders): Collection
    {
        if ($this->isShipmentsOrderItems) {
            return $orders
                ->pluck('deliveries')->collapse()
                ->pluck('shipments')->collapse()
                ->pluck('order_items')->collapse();
        }

        return collect();
    }

    protected function getOrderShipments(Collection $orders): Collection
    {
        return $orders
            ->pluck('deliveries')->collapse()
            ->pluck('shipments')->collapse();
    }

    protected function prepareFilterValues(Collection $values): array
    {
        return $values->unique()->values()->filter()->all();
    }
}
