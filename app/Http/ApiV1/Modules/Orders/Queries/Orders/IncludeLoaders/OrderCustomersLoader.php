<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\PaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination;
use Ensi\CustomersClient\Dto\SearchCustomersFilter;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\CustomersClient\Dto\SearchCustomersResponse;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Collection;

class OrderCustomersLoader implements AsyncLoader
{
    /** @var Collection|Customer[] */
    public Collection $customers;
    public array $customerIds = [];
    public bool $load = false;

    public function __construct(protected CustomersApi $customersApi)
    {
        $this->customers = collect();
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->customerIds) {
            return null;
        }

        $request = new SearchCustomersRequest();
        $request->setFilter(new SearchCustomersFilter(['id' => $this->customerIds,]));
        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->customerIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->customersApi->searchCustomersAsync($request);
    }

    /**
     * @param SearchCustomersResponse $response
     */
    public function processResponse($response)
    {
        $this->customers = collect($response->getData())->keyBy('id');
    }
}
