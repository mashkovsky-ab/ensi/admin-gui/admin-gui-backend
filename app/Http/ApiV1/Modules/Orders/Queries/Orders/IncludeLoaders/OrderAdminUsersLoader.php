<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\PaginationTypeEnum;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Ensi\AdminAuthClient\Dto\SearchUsersResponse;
use Ensi\AdminAuthClient\Dto\User;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Collection;

class OrderAdminUsersLoader implements AsyncLoader
{
    /** @var Collection|User[] */
    public Collection $users;
    public array $userIds = [];
    public bool $load = false;

    public function __construct(protected UsersApi $usersApi)
    {
        $this->users = collect();
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->userIds) {
            return null;
        }

        $request = new SearchUsersRequest();
        $request->setFilter((object)[
            'id' => $this->userIds,
        ]);
        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->userIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->usersApi->searchUsersAsync($request);
    }

    /**
     * @param SearchUsersResponse $response
     */
    public function processResponse($response)
    {
        $this->users = collect($response->getData())->keyBy('id');
    }
}
