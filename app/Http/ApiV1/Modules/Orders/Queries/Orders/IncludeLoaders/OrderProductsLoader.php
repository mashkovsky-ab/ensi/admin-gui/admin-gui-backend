<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrderProductsLoader implements AsyncLoader
{
    /** @var Product[] */
    public array $products = [];
    public array $productIds = [];
    public bool $load = false;
    public bool $loadProductImage = false;
    public bool $loadProductCategory = false;
    public bool $loadProductBrand = false;

    public function __construct(protected ProductsApi $productsApi)
    {
    }

    public function loadProductBrand()
    {
        $this->loadProductBrand = true;
        $this->load = true;
    }

    public function loadProductImage()
    {
        $this->loadProductImage = true;
        $this->load = true;
    }

    public function loadProductCategory()
    {
        $this->loadProductCategory = true;
        $this->load = true;
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->productIds) {
            return null;
        }

        $request = new SearchProductsRequest();
        $request->setFilter((object)['id' => $this->productIds]);

        $includes = [];
        if ($this->loadProductCategory) {
            $includes[] = 'category';
        }
        if ($this->loadProductBrand) {
            $includes[] = 'brand';
        }
        if ($this->loadProductImage) {
            $includes[] = 'images';
        }
        if ($includes) {
            $request->setInclude($includes);
        }
        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->productIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->productsApi->searchProductsAsync($request);
    }

    /**
     * @param SearchProductsResponse $response
     */
    public function processResponse($response)
    {
        $this->products = $response->getData();
    }
}
