<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders;

use App\Domain\Catalog\Data\Offers\OfferData;
use App\Domain\Common\Data\AsyncLoader;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\PaginationTypeEnum;
use Ensi\OffersClient\Dto\RequestBodyPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrderOffersLoader implements AsyncLoader
{
    /** @var OfferData[] */
    public array $offersData = [];
    public array $offerIds = [];
    public bool $load = false;
    public bool $loadStock = false;

    public function __construct(protected OffersApi $offersApi)
    {
    }

    public function loadStock()
    {
        $this->loadStock = true;
        $this->load = true;
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->offerIds) {
            return null;
        }

        $request = (new SearchOffersRequest())->setFilter((object)[
            'id' => $this->offerIds,
        ]);
        $includes = [];
        if ($this->loadStock) {
            $includes[] = 'stocks';
        }
        $request->setInclude($includes);
        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->offerIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->offersApi->searchOffersAsync($request);
    }

    /**
     * @param SearchOffersResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $offer) {
            $this->offersData[] = new OfferData($offer);
        }
    }
}
