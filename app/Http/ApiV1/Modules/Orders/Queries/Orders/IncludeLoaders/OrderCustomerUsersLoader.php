<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\Dto\PaginationTypeEnum;
use Ensi\CustomerAuthClient\Dto\RequestBodyPagination;
use Ensi\CustomerAuthClient\Dto\SearchUsersRequest;
use Ensi\CustomerAuthClient\Dto\SearchUsersResponse;
use Ensi\CustomerAuthClient\Dto\User;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Collection;

class OrderCustomerUsersLoader implements AsyncLoader
{
    /** @var Collection|User[] */
    public Collection $users;
    public array $userIds = [];
    public bool $load = false;

    public function __construct(protected UsersApi $usersApi)
    {
        $this->users = collect();
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->userIds) {
            return null;
        }

        $request = new SearchUsersRequest();
        $request->setFilter((object)[
            'id' => $this->userIds,
        ]);
        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->userIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->usersApi->searchUsersAsync($request);
    }

    /**
     * @param SearchUsersResponse $response
     */
    public function processResponse($response)
    {
        $this->users = collect($response->getData())->keyBy('id');
    }
}
