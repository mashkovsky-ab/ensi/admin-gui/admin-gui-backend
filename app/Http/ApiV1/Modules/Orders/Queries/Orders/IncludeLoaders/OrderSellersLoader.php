<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use App\Domain\Units\Data\SellerData;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Dto\PaginationTypeEnum;
use Ensi\BuClient\Dto\RequestBodyPagination;
use Ensi\BuClient\Dto\SearchSellersRequest;
use Ensi\BuClient\Dto\SearchSellersResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrderSellersLoader implements AsyncLoader
{
    /** @var SellerData[] */
    public array $sellersData = [];
    public array $sellerIds = [];
    public bool $load = false;

    public function __construct(protected SellersApi $sellersApi)
    {
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->sellerIds) {
            return null;
        }

        $request = new SearchSellersRequest();
        $request->setFilter((object)['id' => $this->sellerIds,]);
        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->sellerIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->sellersApi->searchSellersAsync($request);
    }

    /**
     * @param SearchSellersResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $seller) {
            $this->sellersData[] = new SellerData($seller);
        }
    }
}
