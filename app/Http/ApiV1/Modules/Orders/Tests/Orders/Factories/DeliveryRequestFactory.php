<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories;

use App\Domain\Orders\Tests\Factories\Orders\Data\TimeslotFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class DeliveryRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'date' => $this->faker->optional()->date(),
            'timeslot' => $this->faker->boolean() ? TimeslotFactory::new()->makeArray() : null,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makePathDelivery(array $extra = []): array
    {
        return $this->only([
            'date',
            'timeslot',
        ])->makeArray($extra);
    }
}
