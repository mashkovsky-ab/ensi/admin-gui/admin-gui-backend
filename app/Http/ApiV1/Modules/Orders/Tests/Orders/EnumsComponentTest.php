<?php

use App\Domain\Orders\Tests\Factories\Orders\Enums\DeliveryStatusFactory;
use App\Domain\Orders\Tests\Factories\Orders\Enums\OrderStatusFactory;
use App\Domain\Orders\Tests\Factories\Orders\Enums\PaymentMethodFactory;
use App\Domain\Orders\Tests\Factories\Orders\Enums\PaymentStatusFactory;
use App\Domain\Orders\Tests\Factories\Orders\Enums\ShipmentStatusFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderSourceFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.enums');

test('GET /api/v1/orders/order-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockOmsEnumsApi()->allows([
        'getOrderStatuses' => OrderStatusFactory::new()->makeResponseSearch([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/orders/order-statuses")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});

test('GET /api/v1/orders/order-sources 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockOmsEnumsApi()->allows([
        'getOrderSources' => OrderSourceFactory::new()->makeResponseSearch([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/orders/order-sources")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});

test('GET /api/v1/orders/payment-methods 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockOmsEnumsApi()->allows([
        'getPaymentMethods' => PaymentMethodFactory::new()->makeResponseSearch([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/orders/payment-methods")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});

test('GET /api/v1/orders/payment-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockOmsEnumsApi()->allows([
        'getPaymentStatuses' => PaymentStatusFactory::new()->makeResponseSearch([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/orders/payment-statuses")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});

test('GET /api/v1/orders/delivery-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockOmsEnumsApi()->allows([
        'getDeliveryStatuses' => DeliveryStatusFactory::new()->makeResponseSearch([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/orders/delivery-statuses")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});

test('GET /api/v1/orders/shipment-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockOmsEnumsApi()->allows([
        'getShipmentStatuses' => ShipmentStatusFactory::new()->makeResponseSearch([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/orders/shipment-statuses")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});
