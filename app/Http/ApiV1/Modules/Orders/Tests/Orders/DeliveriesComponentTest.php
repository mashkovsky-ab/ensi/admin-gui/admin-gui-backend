<?php

use App\Domain\Orders\Tests\Factories\Orders\DeliveryFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories\DeliveryRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.deliveries');

test('PATCH /api/v1/orders/deliveries/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $pathDeliveryData = DeliveryRequestFactory::new()->makePathDelivery();

    $this->mockOmsDeliveriesApi()->allows([
        'patchDelivery' => DeliveryFactory::new()->makeResponseOne(),
    ]);

    patchJson("/api/v1/orders/deliveries/1", $pathDeliveryData)->assertStatus(200);
});
