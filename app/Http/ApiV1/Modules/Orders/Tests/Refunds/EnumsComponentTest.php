<?php

use App\Domain\Orders\Tests\Factories\Refunds\Enums\RefundStatusFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.enums');

test('GET /api/v1/orders/refund-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockOmsEnumsApi()->allows([
        'getRefundStatuses' => RefundStatusFactory::new()->makeResponseSearch([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/orders/refund-statuses")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});
