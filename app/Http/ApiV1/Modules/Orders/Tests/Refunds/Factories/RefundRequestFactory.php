<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Refunds\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\RefundStatusEnum;

class RefundRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_id' => $this->faker->randomNumber(),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'responsible_id' => $this->faker->optional()->randomNumber(),
            'source' => $this->faker->randomElement(OrderSourceEnum::getAllowableEnumValues()),
            'status' => $this->faker->randomElement(RefundStatusEnum::getAllowableEnumValues()),
            'is_partial' => $this->faker->boolean(),
            'user_comment' => $this->faker->text(50),
            'rejection_comment' => $this->faker->optional()->text(50),
            'order_items' => [
                ['id' => $this->faker->randomNumber(), 'qty' => $this->faker->numberBetween(1, 10)],
            ],
            'refund_reason_ids' => [$this->faker->randomNumber()],
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
