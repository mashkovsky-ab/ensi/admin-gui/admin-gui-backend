<?php

use App\Domain\Orders\Tests\Factories\OmsCommon\SettingFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.settings');

test('GET /api/v1/orders/oms-settings 200', function () {
    $settingId = 1;
    $settingValue = 'test';

    $this->mockOmsCommonApi()->allows([
        'searchSettings' => SettingFactory::new()->makeResponseSearch(['id' => $settingId, 'code' => $settingValue]),
    ]);

    getJson("/api/v1/orders/oms-settings")
        ->assertStatus(200);
});

test('PATCH /api/v1/orders/oms-settings 200', function () {
    $settingId = 1;
    $settingValue = 'test';

    $this->mockOmsCommonApi()->allows([
        'patchSettings' => SettingFactory::new()->makeResponseSearch(['id' => $settingId, 'value' => $settingValue]),
    ]);

    patchJson('/api/v1/orders/oms-settings', ['settings' => [['id' => $settingId, 'value' => $settingValue]]])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $settingId)
        ->assertJsonPath('data.0.value', $settingValue);
});
