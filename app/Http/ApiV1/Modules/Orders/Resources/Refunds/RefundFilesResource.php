<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Refunds;

use App\Domain\Orders\ProtectedFiles\RefundFile;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\RefundFile as OmsRefundFile;

/**
 * Class RefundFilesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 *
 * @mixin OmsRefundFile
 */
class RefundFilesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),

            'refund_id' => $this->getRefundId(),
            'original_name' => $this->getOriginalName(),
            'file' => RefundFile::createFromModel($this->resource),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
