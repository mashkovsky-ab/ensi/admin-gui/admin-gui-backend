<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\OmsCommon;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\Setting;

/**
 * Class SettingResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin Setting
 */
class SettingResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),

            'code' => $this->getCode(),
            'name' => $this->getName(),
            'value' => $this->getValue(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
