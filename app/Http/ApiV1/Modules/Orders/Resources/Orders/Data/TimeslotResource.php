<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Orders\Data;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\Timeslot;

/**
 * Class TimeslotResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin Timeslot
 */
class TimeslotResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'from' => $this->getFrom(),
            'to' => $this->getTo(),
        ];
    }
}
