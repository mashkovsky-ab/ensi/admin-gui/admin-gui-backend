<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Orders;

use App\Domain\Orders\ProtectedFiles\OrderFile;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\OrderFile as OmsOrderFile;

/**
 * Class OrderFilesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 *
 * @mixin OmsOrderFile
 */
class OrderFilesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),

            'order_id' => $this->getOrderId(),
            'original_name' => $this->getOriginalName(),
            'file' => OrderFile::createFromModel($this->resource),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
