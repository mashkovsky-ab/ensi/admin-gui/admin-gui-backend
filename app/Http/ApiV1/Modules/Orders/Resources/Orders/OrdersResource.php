<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Data\DeliveryAddressResource;
use App\Http\ApiV1\Modules\Units\Resources\AdminUsersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class OrdersResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin OrderData
 */
class OrdersResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            "id" => $this->order->getId(),
            "number" => $this->order->getNumber(),
            "source" => $this->order->getSource(),
            "customer_id" => $this->order->getCustomerId(),
            "responsible_id" => $this->order->getResponsibleId(),
            'customer_email' => $this->order->getCustomerEmail(),
            "cost" => $this->order->getCost(),
            "price" => $this->order->getPrice(),
            "spent_bonus" => $this->order->getSpentBonus(),
            "added_bonus" => $this->order->getAddedBonus(),
            "promo_code" => $this->order->getPromoCode(),
            "delivery_method" => $this->order->getDeliveryMethod(),
            "delivery_service" => $this->order->getDeliveryService(),
            "delivery_tariff_id" => $this->order->getDeliveryTariffId(),
            "delivery_point_id" => $this->order->getDeliveryPointId(),
            "delivery_address" => DeliveryAddressResource::make($this->order->getDeliveryAddress()),
            "delivery_address_string" => $this->order->getDeliveryAddress()?->getAddressString(),
            "delivery_price" => $this->order->getDeliveryPrice(),
            "delivery_cost" => $this->order->getDeliveryCost(),
            "receiver_name" => $this->order->getReceiverName(),
            "receiver_phone" => $this->order->getReceiverPhone(),
            "receiver_email" => $this->order->getReceiverEmail(),
            "status" => $this->order->getStatus(),
            "status_at" => $this->dateTimeToIso($this->order->getStatusAt()),
            "payment_status" => $this->order->getPaymentStatus(),
            "payment_status_at" => $this->dateTimeToIso($this->order->getPaymentStatusAt()),
            "payed_at" => $this->dateTimeToIso($this->order->getPayedAt()),
            "payment_expires_at" => $this->dateTimeToIso($this->order->getPaymentExpiresAt()),
            "payment_method" => $this->order->getPaymentMethod(),
            "payment_system" => $this->order->getPaymentSystem(),
//            "payment_link" => $this->order->getPaymentLink(),
            "payment_external_id" => $this->order->getPaymentExternalId(),
            "is_expired" => $this->order->getIsExpired(),
            "is_expired_at" => $this->dateTimeToIso($this->order->getIsExpiredAt()),
            "is_return" => $this->order->getIsReturn(),
            "is_return_at" => $this->dateTimeToIso($this->order->getIsPartialReturnAt()),
            "is_partial_return" => $this->order->getIsPartialReturn(),
            "is_partial_return_at" => $this->dateTimeToIso($this->order->getIsPartialReturnAt()),
            "is_problem" => $this->order->getIsProblem(),
            "is_problem_at" => $this->dateTimeToIso($this->order->getIsProblemAt()),
            "problem_comment" => $this->order->getProblemComment(),
            "created_at" => $this->dateTimeToIso($this->order->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->order->getUpdatedAt()),
            "delivery_comment" => $this->order->getDeliveryComment(),
            "client_comment" => $this->order->getClientComment(),
            "deliveries" => DeliveriesResource::collection($this->whenNotNull($this->getDeliveries())),
            "files" => OrderFilesResource::collection($this->whenNotNull($this->order->getFiles())),
            "customer" => new CustomersResource($this->whenNotNull($this->customer)),
            "responsible" => new AdminUsersResource($this->whenNotNull($this->responsible)),
        ];
    }
}
