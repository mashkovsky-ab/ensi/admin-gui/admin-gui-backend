<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers\Refunds;

use App\Domain\Orders\Actions\Refunds\AttachRefundFileAction;
use App\Domain\Orders\Actions\Refunds\CreateRefundAction;
use App\Domain\Orders\Actions\Refunds\DeleteRefundFilesAction;
use App\Domain\Orders\Actions\Refunds\PatchRefundAction;
use App\Http\ApiV1\Modules\Orders\Queries\Refunds\RefundsQuery;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\AttachRefundFileRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\CreateRefundRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\DeleteRefundFilesRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\PatchRefundRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Refunds\RefundFilesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Refunds\RefundsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class RefundsController
{
    public function create(CreateRefundRequest $request, CreateRefundAction $action): RefundsResource
    {
        return new RefundsResource($action->execute($request->validated()));
    }

    public function search(RefundsQuery $query)
    {
        return RefundsResource::collectPage($query->get());
    }

    public function get(int $id, RefundsQuery $query)
    {
        return new RefundsResource($query->find($id));
    }

    public function patch(int $id, PatchRefundRequest $request, PatchRefundAction $action): RefundsResource
    {
        return new RefundsResource($action->execute($id, $request->validated()));
    }

    public function attachFile(int $id, AttachRefundFileAction $action, AttachRefundFileRequest $request)
    {
        return new RefundFilesResource($action->execute($id, $request->getFile()));
    }

    public function deleteFiles(int $id, DeleteRefundFilesAction $action, DeleteRefundFilesRequest $request)
    {
        $action->execute($id, $request->getFileIds());

        return new EmptyResource();
    }
}
