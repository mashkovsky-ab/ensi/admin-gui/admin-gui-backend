<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Refunds;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\OmsClient\Dto\RefundStatusEnum;
use Illuminate\Validation\Rule;

class PatchRefundRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'responsible_id' => ['nullable', 'integer'],
            'status' => [Rule::in(RefundStatusEnum::getAllowableEnumValues())],
            'rejection_comment' => ['nullable', 'string'],
        ];
    }
}
