<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\OmsClient\Dto\PaymentSystemEnum;
use Illuminate\Validation\Rule;

class ChangeOrderPaymentSystemRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'payment_system' => ['required', 'integer', Rule::in(PaymentSystemEnum::getAllowableEnumValues())],
        ];
    }

    public function getPaymentSystem(): string
    {
        return $this->get('payment_system');
    }
}
