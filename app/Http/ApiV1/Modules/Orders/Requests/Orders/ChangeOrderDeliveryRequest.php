<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ChangeOrderDeliveryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service' => ['integer'],
            'delivery_method' => ['integer'],
            'delivery_tariff_id' => ['integer'],
            'delivery_point_id' => ['nullable', 'integer'],
            'delivery_address' => ['nullable', 'array'],
            'delivery_price' => ['integer'],
            'delivery_cost' => ['integer'],
            'delivery_comment' => ['nullable', 'string'],
        ];
    }
}
