<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDeliveryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'date' => ['nullable', 'date'],
            'timeslot' => ['nullable', 'array'],
        ];
    }
}
