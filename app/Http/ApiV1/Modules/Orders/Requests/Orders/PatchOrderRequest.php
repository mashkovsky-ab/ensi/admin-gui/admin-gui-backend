<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrdersOrderStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchOrderRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'responsible_id' => ['nullable', 'integer'],
            'status' => [Rule::in(OrdersOrderStatusEnum::cases())],
            'client_comment' => ['nullable', 'string'],
            'receiver_name' => ['string'],
            'receiver_phone' => ['string'],
            'receiver_email' => ['string'],
            'is_problem' => ['boolean'],
            'problem_comment' => ['string'],
        ];
    }
}
