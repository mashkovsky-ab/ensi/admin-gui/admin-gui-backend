<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryPrices;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\LogisticClient\Dto\Tariff;

/**
 *  Class TariffEnumValueResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin Tariff
 */
class TariffEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): string
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
