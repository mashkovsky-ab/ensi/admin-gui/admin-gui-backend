<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\CargoOrders;

use App\Domain\Logistic\Data\CargoData;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServicesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\ShipmentsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CargoData
 */
class CargoResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->cargo->getId(),
            'seller_id' => $this->cargo->getSellerId(),
            'store_id' => $this->cargo->getStoreId(),
            'delivery_service_id' => $this->cargo->getDeliveryServiceId(),
            'status' => $this->cargo->getStatus(),
            'status_at' => $this->dateTimeToIso($this->cargo->getStatusAt()),
            'is_problem' => $this->cargo->getIsProblem(),
            'is_problem_at' => $this->dateTimeToIso($this->cargo->getIsProblemAt()),
            'is_canceled' => $this->cargo->getIsCanceled(),
            'is_canceled_at' => $this->dateTimeToIso($this->cargo->getCreatedAt()),
            'width' => $this->cargo->getWidth(),
            'height' => $this->cargo->getHeight(),
            'length' => $this->cargo->getLength(),
            'weight' => $this->cargo->getWeight(),
            'shipping_problem_comment' => $this->cargo->getShippingProblemComment(),
            'created_at' => $this->dateTimeToIso($this->cargo->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->cargo->getUpdatedAt()),
            'shipments' => ShipmentsResource::collection($this->whenNotNull($this->getShipments())),
            'delivery_service' => DeliveryServicesResource::make($this->whenNotNull($this->cargo->getDeliveryService())),
        ];
    }
}
