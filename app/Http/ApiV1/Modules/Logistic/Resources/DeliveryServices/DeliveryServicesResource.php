<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\DeliveryService;

/**
 * Class DeliveryServicesResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin DeliveryService
 */
class DeliveryServicesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'name' => $this->getName(),
            'registered_at' => $this->dateTimeToIso($this->getRegisteredAt()),
            'legal_info_company_name' => $this->getLegalInfoCompanyName(),
            'legal_info_company_address' => $this->getLegalInfoCompanyAddress(),
            'legal_info_inn' => $this->getLegalInfoInn(),
            'legal_info_payment_account' => $this->getLegalInfoPaymentAccount(),
            'legal_info_bik' => $this->getLegalInfoBik(),
            'legal_info_bank' => $this->getLegalInfoBank(),
            'legal_info_bank_correspondent_account' => $this->getLegalInfoBankCorrespondentAccount(),
            'general_manager_name' => $this->getGeneralManagerName(),
            'contract_number' => $this->getContractNumber(),
            'contract_date' => $this->dateToIso($this->getContractDate()),
            'vat_rate' => $this->getVatRate(),
            'taxation_type' => $this->getTaxationType(),
            'status' => $this->getStatus(),
            'comment' => $this->getComment(),
            'apiship_key' => $this->getApishipKey(),
            'priority' => $this->getPriority(),
            'max_shipments_per_day' => $this->getMaxShipmentsPerDay(),
            'max_cargo_export_time' => $this->getMaxCargoExportTime(),
            'do_consolidation' => $this->getDoConsolidation(),
            'do_deconsolidation' => $this->getDoDeconsolidation(),
            'do_zero_mile' => $this->getDoZeroMile(),
            'do_express_delivery' => $this->getDoExpressDelivery(),
            'do_transportation_oversized_cargo' => $this->getDoTransportationOversizedCargo(),
            'do_return' => $this->getDoReturn(),
            'do_dangerous_products_delivery' => $this->getDoDangerousProductsDelivery(),
            'add_partial_reject_service' => $this->getAddPartialRejectService(),
            'add_insurance_service' => $this->getAddInsuranceService(),
            'add_fitting_service' => $this->getAddFittingService(),
            'add_return_service' => $this->getAddReturnService(),
            'add_open_service' => $this->getAddOpenService(),
            'pct' => $this->getPct(),
            'payment_methods' => $this->whenNotNull($this->getPaymentMethods()),
        ];
    }
}
