<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\DeliveryServiceStatus;

/**
 * Class DeliveryServiceStatusesResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin DeliveryServiceStatus
 */
class DeliveryServiceStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
