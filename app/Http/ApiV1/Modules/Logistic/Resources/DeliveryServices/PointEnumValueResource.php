<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\LogisticClient\Dto\Point;

/**
 *  Class PointEnumValueResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin Point
 */
class PointEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): string
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
