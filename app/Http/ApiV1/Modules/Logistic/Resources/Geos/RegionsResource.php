<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\Geos;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

/**
 * Class RegionsResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\Geos
 *
 * @mixin Region
 */
class RegionsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'federal_district_id' => $this->getFederalDistrictId(),
            'name' => $this->getName(),
            'guid' => $this->getGuid(),
            'created_at' => $this->getCreatedAt() ? new Carbon($this->getCreatedAt()) : null,
            'updated_at' => $this->getUpdatedAt() ? new Carbon($this->getUpdatedAt()) : null,
        ];
    }
}
