<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\Geos;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

/**
 * Class FederalDistrictsResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\Geos
 *
 * @mixin FederalDistrict
 */
class FederalDistrictsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'created_at' => $this->getCreatedAt() ? new Carbon($this->getCreatedAt()) : null,
            'updated_at' => $this->getUpdatedAt() ? new Carbon($this->getUpdatedAt()) : null,
            'regions' => RegionsResource::collection($this->whenNotNull($this->getRegions())),
        ];
    }
}
