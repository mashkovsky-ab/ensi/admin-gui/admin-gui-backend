<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\DeliveryKpiPpt;

/**
 * Class DeliveryKpiPptResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis
 *
 * @mixin DeliveryKpiPpt
 */
class DeliveryKpiPptResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'seller_id' => $this->getSellerId(),
            'ppt' => $this->getPpt(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
