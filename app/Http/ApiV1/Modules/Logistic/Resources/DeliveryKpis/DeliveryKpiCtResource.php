<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\DeliveryKpiCt;

/**
 * Class DeliveryKpiCtResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis
 *
 * @mixin DeliveryKpiCt
 */
class DeliveryKpiCtResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'seller_id' => $this->getSellerId(),
            'ct' => $this->getCt(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
