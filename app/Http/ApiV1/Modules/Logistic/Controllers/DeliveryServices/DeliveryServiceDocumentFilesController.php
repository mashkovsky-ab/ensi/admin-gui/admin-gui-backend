<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceDocument\DeleteDeliveryServiceDocumentFileAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\SaveDeliveryServiceDocumentFileAction;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\UploadDeliveryServiceDocumentFileRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceDocumentsResource;

class DeliveryServiceDocumentFilesController
{
    public function upload(int $id, UploadDeliveryServiceDocumentFileRequest $request, SaveDeliveryServiceDocumentFileAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($id, $request->getFile()));
    }

    public function delete(int $id, DeleteDeliveryServiceDocumentFileAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($id));
    }
}
