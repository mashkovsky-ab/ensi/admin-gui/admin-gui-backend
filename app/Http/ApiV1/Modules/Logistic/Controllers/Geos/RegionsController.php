<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\Geos;

use App\Domain\Logistic\Actions\Geos\CreateRegionAction;
use App\Domain\Logistic\Actions\Geos\DeleteRegionAction;
use App\Domain\Logistic\Actions\Geos\PatchRegionAction;
use App\Domain\Logistic\Actions\Geos\ReplaceRegionAction;
use App\Http\ApiV1\Modules\Logistic\Queries\Geos\RegionsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\CreateOrReplaceRegionRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\PatchRegionRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\RegionsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class RegionsController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\Geos
 */
class RegionsController
{
    public function create(CreateOrReplaceRegionRequest $request, CreateRegionAction $action): RegionsResource
    {
        return new RegionsResource($action->execute($request->validated()));
    }

    public function replace(int $id, CreateOrReplaceRegionRequest $request, ReplaceRegionAction $action): RegionsResource
    {
        return new RegionsResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchRegionRequest $request, PatchRegionAction $action): RegionsResource
    {
        return new RegionsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteRegionAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, RegionsQuery $query): RegionsResource
    {
        return new RegionsResource($query->find($id));
    }

    public function search(RegionsQuery $query): AnonymousResourceCollection
    {
        return RegionsResource::collectPage($query->get());
    }

    public function searchOne(RegionsQuery $query): RegionsResource
    {
        return new RegionsResource($query->first());
    }
}
