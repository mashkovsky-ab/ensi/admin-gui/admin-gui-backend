<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis;

use App\Domain\Logistic\Actions\DeliveryKpis\PatchDeliveryKpiAction;
use App\Domain\Logistic\Actions\DeliveryKpis\ReplaceDeliveryKpiAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis\DeliveryKpiQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\PatchDeliveryKpiRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\ReplaceDeliveryKpiRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis\DeliveryKpiResource;
use Ensi\LogisticClient\ApiException;

/**
 * Class DeliveryKpiController
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Controllers
 */
class DeliveryKpiController
{
    /**
     * @param DeliveryKpiQuery $query
     * @return DeliveryKpiResource
     * @throws ApiException
     */
    public function get(DeliveryKpiQuery $query): DeliveryKpiResource
    {
        return new DeliveryKpiResource($query->get());
    }

    /**
     * @param PatchDeliveryKpiRequest $request
     * @param PatchDeliveryKpiAction $action
     * @return DeliveryKpiResource
     * @throws ApiException
     */
    public function patch(PatchDeliveryKpiRequest $request, PatchDeliveryKpiAction $action): DeliveryKpiResource
    {
        return new DeliveryKpiResource($action->execute($request->validated()));
    }

    /**
     * @param ReplaceDeliveryKpiRequest $request
     * @param ReplaceDeliveryKpiAction $action
     * @return DeliveryKpiResource
     * @throws ApiException
     */
    public function replace(ReplaceDeliveryKpiRequest $request, ReplaceDeliveryKpiAction $action): DeliveryKpiResource
    {
        return new DeliveryKpiResource($action->execute($request->validated()));
    }
}
