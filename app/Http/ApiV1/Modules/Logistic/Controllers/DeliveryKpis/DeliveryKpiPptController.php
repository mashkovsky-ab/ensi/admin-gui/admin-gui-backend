<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis;

use App\Domain\Logistic\Actions\DeliveryKpis\CreateDeliveryKpiPptAction;
use App\Domain\Logistic\Actions\DeliveryKpis\DeleteDeliveryKpiPptAction;
use App\Domain\Logistic\Actions\DeliveryKpis\ReplaceDeliveryKpiPptAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis\DeliveryKpiPptQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\CreateOrReplaceDeliveryKpiPptRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis\DeliveryKpiPptResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\LogisticClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryKpiPptController
 * @package App\Http\ApiV1\Modules\DeliveryKpis\Controllers
 */
class DeliveryKpiPptController
{
    /**
     * @param DeliveryKpiPptQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(DeliveryKpiPptQuery $query): AnonymousResourceCollection
    {
        return DeliveryKpiPptResource::collectPage($query->get());
    }

    /**
     * @param DeliveryKpiPptQuery $query
     * @return DeliveryKpiPptResource
     */
    public function searchOne(DeliveryKpiPptQuery $query): DeliveryKpiPptResource
    {
        return new DeliveryKpiPptResource($query->first());
    }

    /**
     * @param int $sellerId
     * @param DeliveryKpiPptQuery $query
     * @return DeliveryKpiPptResource
     */
    public function get(int $sellerId, DeliveryKpiPptQuery $query): DeliveryKpiPptResource
    {
        return new DeliveryKpiPptResource($query->find($sellerId));
    }

    /**
     * @param int $sellerId
     * @param CreateOrReplaceDeliveryKpiPptRequest $request
     * @param CreateDeliveryKpiPptAction $action
     * @return DeliveryKpiPptResource
     * @throws ApiException
     */
    public function create(int $sellerId, CreateOrReplaceDeliveryKpiPptRequest $request, CreateDeliveryKpiPptAction $action): DeliveryKpiPptResource
    {
        return new DeliveryKpiPptResource($action->execute($sellerId, $request->validated()));
    }

    /**
     * @param int $sellerId
     * @param CreateOrReplaceDeliveryKpiPptRequest $request
     * @param ReplaceDeliveryKpiPptAction $action
     * @return DeliveryKpiPptResource
     * @throws ApiException
     */
    public function replace(int $sellerId, CreateOrReplaceDeliveryKpiPptRequest $request, ReplaceDeliveryKpiPptAction $action): DeliveryKpiPptResource
    {
        return new DeliveryKpiPptResource($action->execute($sellerId, $request->validated()));
    }

    /**
     * @param int $sellerId
     * @param DeleteDeliveryKpiPptAction $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function delete(int $sellerId, DeleteDeliveryKpiPptAction $action): EmptyResource
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
