<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices;

use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices\TariffQuery;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryPrices\TariffEnumValueResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TariffsController
{
    public function searchEnumValues(CommonFilterEnumValuesRequest $request, TariffQuery $query): AnonymousResourceCollection
    {
        return TariffEnumValueResource::collection($query->searchEnums());
    }
}
