<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices;

use App\Domain\Logistic\Actions\DeliveryPrices\CreateDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\DeleteDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\PatchDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\ReplaceDeliveryPriceAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices\DeliveryPricesQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices\CreateOrReplaceDeliveryPriceRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices\PatchDeliveryPriceRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryPrices\DeliveryPricesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryPricesController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices
 */
class DeliveryPricesController
{
    public function create(CreateOrReplaceDeliveryPriceRequest $request, CreateDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute($request->validated()));
    }

    public function replace(int $id, CreateOrReplaceDeliveryPriceRequest $request, ReplaceDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchDeliveryPriceRequest $request, PatchDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryPriceAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryPricesQuery $query): DeliveryPricesResource
    {
        return new DeliveryPricesResource($query->find($id));
    }

    public function search(DeliveryPricesQuery $query): AnonymousResourceCollection
    {
        return DeliveryPricesResource::collectPage($query->get());
    }

    public function searchOne(DeliveryPricesQuery $query): DeliveryPricesResource
    {
        return new DeliveryPricesResource($query->first());
    }
}
