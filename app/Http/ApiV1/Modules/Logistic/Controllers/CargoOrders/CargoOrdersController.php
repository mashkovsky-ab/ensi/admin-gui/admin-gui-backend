<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\CargoOrders;

use App\Domain\Logistic\Actions\CargoOrders\CancelCargoOrdersAction;
use App\Http\ApiV1\Modules\Logistic\Queries\CargoOrders\CargoOrderQuery;
use App\Http\ApiV1\Modules\Logistic\Resources\CargoOrders\CargoOrdersResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CargoOrdersController
{
    public function cancel(int $cargoOrderId, CancelCargoOrdersAction $action): CargoOrdersResource
    {
        return CargoOrdersResource::make($action->execute($cargoOrderId));
    }

    public function search(CargoOrderQuery $query): AnonymousResourceCollection
    {
        return CargoOrdersResource::collectPage($query->get());
    }
}
