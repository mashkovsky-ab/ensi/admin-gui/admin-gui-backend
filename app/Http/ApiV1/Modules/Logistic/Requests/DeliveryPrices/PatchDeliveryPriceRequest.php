<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PatchDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices
 */
class PatchDeliveryPriceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'federal_district_id' => ['nullable', 'integer'],
            'region_id' => ['nullable', 'integer'],
            'region_guid' => ['nullable', 'string'],
            'delivery_service' => ['nullable', Rule::in(LogisticDeliveryServiceEnum::cases())],
            'delivery_method' => ['nullable', Rule::in(LogisticDeliveryMethodEnum::cases())],
            'price' => ['nullable', 'integer'],
      ];
    }
}
