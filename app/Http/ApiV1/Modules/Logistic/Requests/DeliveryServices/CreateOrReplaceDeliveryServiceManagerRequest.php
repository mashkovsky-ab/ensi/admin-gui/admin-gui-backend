<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceDeliveryServiceManagerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['required', Rule::in(LogisticDeliveryServiceEnum::cases())],
            'name' => ['required', 'string'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/'],
            'email' => ['required', 'email'],
      ];
    }
}
