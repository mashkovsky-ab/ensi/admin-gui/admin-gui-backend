<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrdersPaymentMethodEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class DeletePaymentMethodFromDeliveryServiceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'payment_method' => ['required', Rule::in(OrdersPaymentMethodEnum::cases())],
        ];
    }

    public function getPaymentMethod(): int
    {
        return $this->get('payment_method');
    }
}
