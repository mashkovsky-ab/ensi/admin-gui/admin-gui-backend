<?php


namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class SearchDeliveryServiceRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices
 */
class SearchDeliveryServiceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'filter' => ['nullable', 'array'],
            "filter.id" => ['nullable', 'integer'],
            "filter.name" => ['nullable', 'string'],
            "filter.legal_info_company_name" => ['nullable', 'string'],
            "filter.legal_info_inn" => ['nullable', 'string'],
            "filter.legal_info_payment_account" => ['nullable', 'string'],
            "filter.legal_info_bik" => ['nullable', 'string'],
            "filter.legal_info_bank" => ['nullable', 'string'],
            "filter.legal_info_bank_correspondent_account" => ['nullable', 'string'],
            "filter.general_manager_name" => ['nullable', 'string'],
            "filter.contract_number" => ['nullable', 'string'],
            "filter.status" => [Rule::in(LogisticDeliveryServiceEnum::cases())],
            "filter.comment" => ['nullable', 'string'],
            "filter.do_consolidation" => ['nullable', 'boolean'],
            "filter.do_deconsolidation" => ['nullable', 'boolean'],
            "filter.do_zero_mile" => ['nullable', 'boolean'],
            "filter.do_express_delivery" => ['nullable', 'boolean'],
            "filter.do_return" => ['nullable', 'boolean'],
            "filter.do_dangerous_products_delivery" => ['nullable', 'boolean'],
            "filter.add_partial_reject_service" => ['nullable', 'boolean'],
            "filter.add_insurance_service" => ['nullable', 'boolean'],
            "filter.add_fitting_service" => ['nullable', 'boolean'],
            "filter.add_return_service" => ['nullable', 'boolean'],
            "filter.add_open_service" => ['nullable', 'boolean'],

            'sort' => ['nullable','array'],
            'sort.*' => ['nullable','string'],

            'pagination' => ['nullable','array'],
            'pagination.limit' => ['nullable','integer'],
            'pagination.offset' => ['nullable','integer'],
        ];
    }
}
