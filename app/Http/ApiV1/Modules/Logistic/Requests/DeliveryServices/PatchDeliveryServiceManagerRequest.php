<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchDeliveryServiceManagerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service' => ['nullable', Rule::in(LogisticDeliveryServiceEnum::cases())],
            'name' => ['nullable', 'string'],
            'phone' => ['nullable', 'regex:/^\+7\d{10}$/'],
            'email' => ['nullable', 'string'],
      ];
    }
}
