<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\CargoOrders;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchCargoOrdersRequest;
use Illuminate\Http\Request;

class CargoOrderQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(protected Request $httpRequest, protected CargoOrdersApi $cargoOrdersApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchCargoOrdersRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function search($request)
    {
        return $this->cargoOrdersApi->searchCargoOrders($request);
    }
}
