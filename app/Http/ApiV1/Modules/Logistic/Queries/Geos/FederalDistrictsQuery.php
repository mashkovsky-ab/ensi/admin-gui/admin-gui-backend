<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\Geos;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Ensi\LogisticClient\Dto\FederalDistrictResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest;
use Ensi\LogisticClient\Dto\SearchFederalDistrictsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class FederalDistrictsQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\Geos
 */
class FederalDistrictsQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    /**
     * FederalDistrictsQuery constructor.
     * @param  Request  $httpRequest
     * @param  GeosApi  $geosApi
     */
    public function __construct(protected Request $httpRequest, protected GeosApi $geosApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestFirstClass(): string
    {
        return SearchFederalDistrictsRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchFederalDistrictsRequest::class;
    }

    /**
     * @param $id
     * @return FederalDistrictResponse
     * @throws ApiException
     */
    protected function searchById($id): FederalDistrictResponse
    {
        return $this->geosApi->getFederalDistrict($id, $this->httpRequest->get('include'));
    }

    /**
     * @param  SearchFederalDistrictsRequest  $requestFields
     * @return Collection|FederalDistrict[]
     * @throws SearchFederalDistrictsResponse
     * @throws ApiException
     */
    protected function search($request): SearchFederalDistrictsResponse
    {
        return $this->geosApi->searchFederalDistricts($request);
    }

    /**
     * @param  SearchFederalDistrictsRequest  $requestFields
     * @return FederalDistrictResponse
     * @throws ApiException
     */
    protected function searchOne($request): FederalDistrictResponse
    {
        return $this->geosApi->searchOneFederalDistrict($request);
    }
}
