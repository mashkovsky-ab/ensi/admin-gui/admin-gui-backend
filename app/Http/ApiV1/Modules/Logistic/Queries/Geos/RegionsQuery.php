<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\Geos;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\RegionResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchRegionsRequest;
use Ensi\LogisticClient\Dto\SearchRegionsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class RegionsQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\Geos
 */
class RegionsQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    /**
     * RegionsQuery constructor.
     * @param  Request  $httpRequest
     * @param  GeosApi  $geosApi
     */
    public function __construct(protected Request $httpRequest, protected GeosApi $geosApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestFirstClass(): string
    {
        return SearchRegionsRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchRegionsRequest::class;
    }

    /**
     * @param $id
     * @return RegionResponse
     * @throws ApiException
     */
    protected function searchById($id): RegionResponse
    {
        return $this->geosApi->getRegion($id, $this->httpRequest->get('include'));
    }

    /**
     * @param  SearchRegionsRequest  $requestFields
     * @return Collection|Region[]
     * @throws SearchRegionsResponse
     * @throws ApiException
     */
    protected function search($request): SearchRegionsResponse
    {
        return $this->geosApi->searchRegions($request);
    }

    /**
     * @param  SearchRegionsRequest  $requestFields
     * @return RegionResponse
     * @throws ApiException
     */
    protected function searchOne($request): RegionResponse
    {
        return $this->geosApi->searchOneRegion($request);
    }
}
