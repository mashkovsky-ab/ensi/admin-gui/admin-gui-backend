<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersResponse;
use Illuminate\Http\Request;

class DeliveryServiceManagersQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    public function __construct(protected Request $httpRequest, protected DeliveryServicesApi $deliveryPricesApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestFirstClass(): string
    {
        return SearchDeliveryServiceManagersRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchDeliveryServiceManagersRequest::class;
    }

    protected function searchById($id): DeliveryServiceManagerResponse
    {
        return $this->deliveryPricesApi->getDeliveryServiceManager($id, $this->httpRequest->get('include'));
    }

    protected function search($request): SearchDeliveryServiceManagersResponse
    {
        return $this->deliveryPricesApi->searchDeliveryServiceManagers($request);
    }

    protected function searchOne($request): DeliveryServiceManagerResponse
    {
        return $this->deliveryPricesApi->searchOneDeliveryServiceManager($request);
    }
}
