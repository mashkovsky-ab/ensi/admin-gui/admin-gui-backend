<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchPointsRequest;
use Ensi\LogisticClient\Dto\SearchPointsResponse;
use Illuminate\Http\Request;

class PointsQuery extends QueryBuilder
{
    use QueryBuilderFilterEnumTrait;

    /**
     * PointsQuery constructor.
     * @param Request $httpRequest
     * @param GeosApi $geosApi
     */
    public function __construct(protected Request $httpRequest, protected GeosApi $geosApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchPointsRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @param $request
     * @return SearchPointsResponse
     * @throws ApiException
     */
    public function search($request): SearchPointsResponse
    {
        return $this->geosApi->searchPoints($request);
    }

    /**
     * @param SearchPointsRequest $request
     * @param null|array $id
     * @param null|string $query
     */
    protected function prepareEnumRequest($request, ?array $id, ?string $query)
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name'] = $query;
        }
        $request->setFilter((object)$filter);
    }
}
