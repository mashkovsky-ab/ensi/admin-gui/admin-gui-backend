<?php


namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpi;

/**
 * Class DeliveryKpiQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis
 */
class DeliveryKpiQuery
{
    /**
     * DeliveryKpiQuery constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @return DeliveryKpi
     * @throws ApiException
     */
    public function get(): DeliveryKpi
    {
        return $this->kpiApi->getDeliveryKpi()->getData();
    }
}
