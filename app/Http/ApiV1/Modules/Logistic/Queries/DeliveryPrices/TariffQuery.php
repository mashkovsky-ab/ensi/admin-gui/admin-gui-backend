<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchTariffsRequest;
use Ensi\LogisticClient\Dto\SearchTariffsResponse;
use Illuminate\Http\Request;

class TariffQuery extends QueryBuilder
{
    use QueryBuilderFilterEnumTrait;

    /**
     * PointsQuery constructor.
     * @param Request $httpRequest
     * @param DeliveryPricesApi $deliveryPricesApi
     */
    public function __construct(protected Request $httpRequest, protected DeliveryPricesApi $deliveryPricesApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchTariffsRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @param $request
     * @return SearchTariffsResponse
     * @throws ApiException
     */
    public function search($request): SearchTariffsResponse
    {
        return $this->deliveryPricesApi->searchTariffs($request);
    }

    /**
     * @param SearchTariffsRequest $request
     * @param null|array $id
     * @param null|string $query
     */
    protected function prepareEnumRequest($request, ?array $id, ?string $query)
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name'] = $query;
        }
        $request->setFilter((object)$filter);
    }
}
