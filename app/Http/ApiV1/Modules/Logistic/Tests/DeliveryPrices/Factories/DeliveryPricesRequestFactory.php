<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\DeliveryPrices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class DeliveryPricesRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'federal_district_id' => $this->faker->randomNumber(),
            'region_id' => $this->faker->randomNumber(),
            'region_guid' => $this->faker->uuid(),
            'delivery_service' => $this->faker->randomElement(LogisticDeliveryServiceEnum::cases()),
            'delivery_method' => $this->faker->randomElement(LogisticDeliveryMethodEnum::cases()),
            'price' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
