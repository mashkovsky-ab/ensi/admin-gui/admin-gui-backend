<?php

use App\Domain\Logistic\Tests\DeliveryPrices\Factories\TariffFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test('POST /api/v1/logistic/tariff-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockLogisticDeliveryPricesApi()->allows([
        'searchTariffs' => TariffFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/logistic/tariff-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);
