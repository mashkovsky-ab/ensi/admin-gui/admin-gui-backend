<?php

use App\Domain\Logistic\Tests\DeliveryPrices\Factories\DeliveryPricesFactory;
use App\Domain\Logistic\Tests\Geo\Factories\FederalDistrictsFactory;
use App\Domain\Logistic\Tests\Geo\Factories\RegionsFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryPrices\Factories\DeliveryPricesRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-prices/{deliveryPriceId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;

    $this->mockLogisticDeliveryPricesApi()->allows([
        'getDeliveryPrice' => DeliveryPricesFactory::new()->makeResponseOne(['id' => $deliveryPriceId]),
    ]);

    getJson("/api/v1/logistic/delivery-prices/{$deliveryPriceId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryPriceId);
});

test("POST /api/v1/logistic/delivery-prices/{deliveryPriceId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;
    $deliveryPricesData = DeliveryPricesRequestFactory::new()->make();

    $this->mockLogisticDeliveryPricesApi()->allows([
        'createDeliveryPrice' => DeliveryPricesFactory::new()->makeResponseOne(['id' => $deliveryPriceId]),
    ]);

    postJson("/api/v1/logistic/delivery-prices", $deliveryPricesData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryPriceId);
});

test("PUT /api/v1/logistic/delivery-prices/{deliveryPriceId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;
    $deliveryPricesData = DeliveryPricesRequestFactory::new()->make();

    $this->mockLogisticDeliveryPricesApi()->allows([
        'replaceDeliveryPrice' => DeliveryPricesFactory::new()->makeResponseOne(['id' => $deliveryPriceId]),
    ]);

    putJson("/api/v1/logistic/delivery-prices/{$deliveryPriceId}", $deliveryPricesData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryPriceId);
});

test("PATCH /api/v1/logistic/delivery-prices/{deliveryPriceId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;
    $deliveryPricesData = DeliveryPricesRequestFactory::new()->make();

    $this->mockLogisticDeliveryPricesApi()->allows([
        'patchDeliveryPrice' => DeliveryPricesFactory::new()->makeResponseOne(['id' => $deliveryPriceId]),
    ]);

    patchJson("/api/v1/logistic/delivery-prices/{$deliveryPriceId}", $deliveryPricesData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryPriceId);
});

test("DELETE /api/v1/logistic/delivery-prices/{deliveryPriceId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;

    $this->mockLogisticDeliveryPricesApi()->shouldReceive('deleteDeliveryPrice');

    deleteJson("/api/v1/logistic/delivery-prices/{$deliveryPriceId}")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/delivery-prices:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;

    $this->mockLogisticDeliveryPricesApi()->allows([
        'searchDeliveryPrices' => DeliveryPricesFactory::new()->makeResponseSearch(['id' => $deliveryPriceId]),
    ]);

    postJson("/api/v1/logistic/delivery-prices:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryPriceId);
});

test("POST /api/v1/logistic/delivery-prices:search all includes 200", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;
    $regionId = 2;
    $federalDistrictId = 3;

    $includes = ["region", "federal_district"];

    $region = RegionsFactory::new()->make(['id' => $regionId]);
    $federalDistrict = FederalDistrictsFactory::new()->make(['id' => $federalDistrictId]);

    $deliveryPricesSearchResponse =
        DeliveryPricesFactory::new()
            ->withRegion($region)->withFederalDistrict($federalDistrict)
            ->makeResponseSearch(['id' => $deliveryPriceId]);

    $this->mockLogisticDeliveryPricesApi()
        ->shouldReceive('searchDeliveryPrices')
        ->withArgs(function (SearchDeliveryPricesRequest $arg) use ($includes) {
            assertEquals($includes, $arg->getInclude());

            return true;
        })
        ->andReturn($deliveryPricesSearchResponse);

    postJson("/api/v1/logistic/delivery-prices:search", [
        "include" => $includes,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryPriceId)
        ->assertJsonPath('data.0.region.id', $regionId)
        ->assertJsonPath('data.0.federal_district.id', $federalDistrictId);
});

test("POST /api/v1/logistic/delivery-prices:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPriceId = 1;

    $this->mockLogisticDeliveryPricesApi()->allows([
        'searchOneDeliveryPrice' => DeliveryPricesFactory::new()->makeResponseOne(['id' => $deliveryPriceId]),
    ]);

    postJson("/api/v1/logistic/delivery-prices:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryPriceId);
});
