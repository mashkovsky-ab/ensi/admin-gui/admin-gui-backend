<?php

use App\Domain\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-kpi success", function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockLogisticKpiApi()->allows([
        'getDeliveryKpi' => DeliveryKpiFactory::new()->makeResponseOne(),
    ]);

    getJson("/api/v1/logistic/delivery-kpi")->assertStatus(200);
});

test("PATCH /api/v1/logistic/delivery-kpi success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryKpiData = DeliveryKpiRequestFactory::new()->make();

    $this->mockLogisticKpiApi()->allows([
        'patchDeliveryKpi' => DeliveryKpiFactory::new()->makeResponseOne(),
    ]);

    patchJson("/api/v1/logistic/delivery-kpi", $deliveryKpiData)->assertStatus(200);
});

test("PUT /api/v1/logistic/delivery-kpi success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryKpiData = DeliveryKpiRequestFactory::new()->make();

    $this->mockLogisticKpiApi()->allows([
        'replaceDeliveryKpi' => DeliveryKpiFactory::new()->makeResponseOne(),
    ]);

    putJson("/api/v1/logistic/delivery-kpi", $deliveryKpiData)->assertStatus(200);
});
