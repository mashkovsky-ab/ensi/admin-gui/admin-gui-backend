<?php

use App\Domain\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiPptFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiPptRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-kpi-ppt/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->allows([
        'getDeliveryKpiPpt' => DeliveryKpiPptFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    getJson("/api/v1/logistic/delivery-kpi-ppt/{$sellerId}")
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});

test("POST /api/v1/logistic/delivery-kpi-ppt/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $deliveryKpiPpiData = DeliveryKpiPptRequestFactory::new()->make();

    $this->mockLogisticKpiApi()->allows([
        'createDeliveryKpiPpt' => DeliveryKpiPptFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    postJson("/api/v1/logistic/delivery-kpi-ppt/{$sellerId}", $deliveryKpiPpiData)
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});

test("PUT /api/v1/logistic/delivery-kpi-ppt/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $deliveryKpiPpiData = DeliveryKpiPptRequestFactory::new()->make();

    $this->mockLogisticKpiApi()->allows([
        'replaceDeliveryKpiPpt' => DeliveryKpiPptFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    putJson("/api/v1/logistic/delivery-kpi-ppt/{$sellerId}", $deliveryKpiPpiData)
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});

test("DELETE /api/v1/logistic/delivery-kpi-ppt/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->shouldReceive('deleteDeliveryKpiPpt');

    deleteJson("/api/v1/logistic/delivery-kpi-ppt/{$sellerId}")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/delivery-kpi-ppt:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->allows([
        'searchDeliveryKpiPpts' => DeliveryKpiPptFactory::new()->makeResponseSearch(['seller_id' => $sellerId]),
    ]);

    postJson("/api/v1/logistic/delivery-kpi-ppt:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.seller_id', $sellerId);
});

test("POST /api/v1/logistic/delivery-kpi-ppt:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->allows([
        'searchOneDeliveryKpiPpt' => DeliveryKpiPptFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    postJson("/api/v1/logistic/delivery-kpi-ppt:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});
