<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\DeliveryKpis\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class DeliveryKpiRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'rtg' => $this->faker->randomNumber(),
            'ct' => $this->faker->randomNumber(),
            'ppt' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
