<?php

use App\Domain\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiCtFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiCtRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-kpi-ct/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->allows([
        'getDeliveryKpiCt' => DeliveryKpiCtFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    getJson("/api/v1/logistic/delivery-kpi-ct/{$sellerId}")
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});

test("POST /api/v1/logistic/delivery-kpi-ct/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $deliveryKpiCtData = DeliveryKpiCtRequestFactory::new()->make();

    $this->mockLogisticKpiApi()->allows([
        'createDeliveryKpiCt' => DeliveryKpiCtFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    postJson("/api/v1/logistic/delivery-kpi-ct/{$sellerId}", $deliveryKpiCtData)
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});

test("PUT /api/v1/logistic/delivery-kpi-ct/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $deliveryKpiCtData = DeliveryKpiCtRequestFactory::new()->make();

    $this->mockLogisticKpiApi()->allows([
        'replaceDeliveryKpiCt' => DeliveryKpiCtFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    putJson("/api/v1/logistic/delivery-kpi-ct/{$sellerId}", $deliveryKpiCtData)
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});

test("DELETE /api/v1/logistic/delivery-kpi-ct/{sellerId} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->shouldReceive('deleteDeliveryKpiCt');

    deleteJson("/api/v1/logistic/delivery-kpi-ct/{$sellerId}")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/delivery-kpi-ct:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->allows([
        'searchDeliveryKpiCts' => DeliveryKpiCtFactory::new()->makeResponseSearch(['seller_id' => $sellerId]),
    ]);

    postJson("/api/v1/logistic/delivery-kpi-ct:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.seller_id', $sellerId);
});

test("POST /api/v1/logistic/delivery-kpi-ct:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;

    $this->mockLogisticKpiApi()->allows([
        'searchOneDeliveryKpiCt' => DeliveryKpiCtFactory::new()->makeResponseOne(['seller_id' => $sellerId]),
    ]);

    postJson("/api/v1/logistic/delivery-kpi-ct:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.seller_id', $sellerId);
});
