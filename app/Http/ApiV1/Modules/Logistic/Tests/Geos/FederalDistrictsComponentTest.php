<?php

use App\Domain\Logistic\Tests\Geo\Factories\FederalDistrictsFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\Geos\Factories\FederalDistrictsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/federal-districts/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;

    $this->mockLogisticGeosApi()->allows([
        'getFederalDistrict' => FederalDistrictsFactory::new()->makeResponseOne(['id' => $federalDistrictId]),
    ]);

    getJson("/api/v1/logistic/federal-districts/{$federalDistrictId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $federalDistrictId);
});

test("POST /api/v1/logistic/federal-districts/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;
    $federalDistrictsData = FederalDistrictsRequestFactory::new()->make();

    $this->mockLogisticGeosApi()->allows([
        'createFederalDistrict' => FederalDistrictsFactory::new()->makeResponseOne(['id' => $federalDistrictId]),
    ]);

    postJson("/api/v1/logistic/federal-districts", $federalDistrictsData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $federalDistrictId);
});

test("PUT /api/v1/logistic/federal-districts/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;
    $federalDistrictsData = FederalDistrictsRequestFactory::new()->make();

    $this->mockLogisticGeosApi()->allows([
        'replaceFederalDistrict' => FederalDistrictsFactory::new()->makeResponseOne(['id' => $federalDistrictId]),
    ]);

    putJson("/api/v1/logistic/federal-districts/{$federalDistrictId}", $federalDistrictsData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $federalDistrictId);
});

test("PATCH /api/v1/logistic/federal-districts/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;
    $federalDistrictsData = FederalDistrictsRequestFactory::new()->make();

    $this->mockLogisticGeosApi()->allows([
        'patchFederalDistrict' => FederalDistrictsFactory::new()->makeResponseOne(['id' => $federalDistrictId]),
    ]);

    patchJson("/api/v1/logistic/federal-districts/{$federalDistrictId}", $federalDistrictsData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $federalDistrictId);
});

test("DELETE /api/v1/logistic/federal-districts/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;

    $this->mockLogisticGeosApi()->shouldReceive('deleteFederalDistrict');

    deleteJson("/api/v1/logistic/federal-districts/{$federalDistrictId}")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/federal-districts:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;

    $this->mockLogisticGeosApi()->allows([
        'searchFederalDistricts' => FederalDistrictsFactory::new()->makeResponseSearch(['id' => $federalDistrictId]),
    ]);

    postJson("/api/v1/logistic/federal-districts:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $federalDistrictId);
});

test("POST /api/v1/logistic/federal-districts:search all includes 200", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;
    $includes = ["regions"];

    $federalDistrictsSearchResponse = FederalDistrictsFactory::new()->withRegion()->makeResponseSearch(['id' => $federalDistrictId]);

    $this->mockLogisticGeosApi()
        ->shouldReceive('searchFederalDistricts')
        ->withArgs(function (SearchFederalDistrictsRequest $arg) use ($includes) {
            assertEquals($includes, $arg->getInclude());

            return true;
        })
        ->andReturn($federalDistrictsSearchResponse);

    postJson("/api/v1/logistic/federal-districts:search", [
        "include" => $includes,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $federalDistrictId)
        ->assertJsonCount(1, 'data.0.regions');
});

test("POST /api/v1/logistic/federal-districts:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $federalDistrictId = 1;

    $this->mockLogisticGeosApi()->allows([
        'searchOneFederalDistrict' => FederalDistrictsFactory::new()->makeResponseOne(['id' => $federalDistrictId]),
    ]);

    postJson("/api/v1/logistic/federal-districts:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $federalDistrictId);
});
