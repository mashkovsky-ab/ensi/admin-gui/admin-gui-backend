<?php


use App\Domain\Logistic\Tests\Geo\Factories\RegionsFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\Geos\Factories\RegionsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/regions/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $regionId = 1;

    $this->mockLogisticGeosApi()->allows([
        'getRegion' => RegionsFactory::new()->makeResponseOne(['id' => $regionId]),
    ]);

    getJson("/api/v1/logistic/regions/{$regionId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $regionId);
});

test("POST /api/v1/logistic/regions/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $regionId = 1;
    $regionsData = RegionsRequestFactory::new()->make();

    $this->mockLogisticGeosApi()->allows([
        'createRegion' => RegionsFactory::new()->makeResponseOne(['id' => $regionId]),
    ]);

    postJson("/api/v1/logistic/regions", $regionsData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $regionId);
});

test("PUT /api/v1/logistic/regions/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $regionId = 1;
    $regionsData = RegionsRequestFactory::new()->make();

    $this->mockLogisticGeosApi()->allows([
        'replaceRegion' => RegionsFactory::new()->makeResponseOne(['id' => $regionId]),
    ]);

    putJson("/api/v1/logistic/regions/{$regionId}", $regionsData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $regionId);
});

test("PATCH /api/v1/logistic/regions/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $regionId = 1;
    $regionsData = RegionsRequestFactory::new()->make();

    $this->mockLogisticGeosApi()->allows([
        'patchRegion' => RegionsFactory::new()->makeResponseOne(['id' => $regionId]),
    ]);

    patchJson("/api/v1/logistic/regions/{$regionId}", $regionsData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $regionId);
});

test("DELETE /api/v1/logistic/regions/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $regionId = 1;

    $this->mockLogisticGeosApi()->shouldReceive('deleteRegion');

    deleteJson("/api/v1/logistic/regions/{$regionId}")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/regions:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $regionId = 1;

    $this->mockLogisticGeosApi()->allows([
        'searchRegions' => RegionsFactory::new()->makeResponseSearch(['id' => $regionId]),
    ]);

    postJson("/api/v1/logistic/regions:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $regionId);
});

test("POST /api/v1/logistic/regions:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $regionId = 1;

    $this->mockLogisticGeosApi()->allows([
        'searchOneRegion' => RegionsFactory::new()->makeResponseOne(['id' => $regionId]),
    ]);

    postJson("/api/v1/logistic/regions:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $regionId);
});
