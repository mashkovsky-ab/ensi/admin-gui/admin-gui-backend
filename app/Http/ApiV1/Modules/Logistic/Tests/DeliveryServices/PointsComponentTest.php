<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\PointFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\SearchPointsRequest;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test('POST /api/v1/logistic/point-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockLogisticGeosApi()->allows([
        'searchPoints' => PointFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/logistic/point-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);

test("POST /api/v1/logistic/points:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $pointId = 1;

    $this->mockLogisticGeosApi()->allows([
        'searchPoints' => PointFactory::new()->makeResponseSearch(['id' => $pointId]),
    ]);

    postJson("/api/v1/logistic/points:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $pointId);
});

test("POST /api/v1/logistic/points:search all includes 200", function () {
    /** @var ApiV1ComponentTestCase $this */
    $pointId = 1;
    $includes = ["metro_stations"];

    $pointsSearchResponse = PointFactory::new()->withMetroStation()->makeResponseSearch(['id' => $pointId]);

    $this->mockLogisticGeosApi()
        ->shouldReceive('searchPoints')
        ->withArgs(function (SearchPointsRequest $arg) use ($includes) {
            assertEquals($includes, $arg->getInclude());

            return true;
        })
        ->andReturn($pointsSearchResponse);

    postJson("/api/v1/logistic/points:search", [
        "include" => $includes,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $pointId)
        ->assertJsonCount(1, 'data.0.metro_stations');
});
