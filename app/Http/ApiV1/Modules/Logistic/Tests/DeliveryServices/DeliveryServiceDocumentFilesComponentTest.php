<?php

use App\Domain\Logistic\ProtectedFiles\DeliveryServiceDocumentFile;
use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceDocumentFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\LogisticClient\Dto\File;
use Illuminate\Http\UploadedFile;
use function Pest\Laravel\post;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("POST /api/v1/logistic/delivery-service-documents/{id}:upload-file success", function (string $ext) {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;
    $fileName = "file.{$ext}";

    $this->mockLogisticDeliveryServicesApi()->allows([
        'uploadDeliveryServiceDocumentFile' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    $requestBody = ['file' => UploadedFile::fake()->create($fileName, 100)];
    post("/api/v1/logistic/delivery-service-documents/{$deliveryServiceDocumentId}:upload-file", $requestBody, ['Content-Type' => "multipart/form-data"])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceDocumentId);
})->with(['jpg', 'pdf']);

test('POST /common/files/download-protected order-file success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;
    $deliveryServiceDocument = DeliveryServiceDocumentFactory::new()->make([
        'id' => $deliveryServiceDocumentId,
        'file' => new File(EnsiFileFactory::new()->makeReal()),
    ]);

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchOneDeliveryServiceDocument' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    postJson(
        "/api/v1/common/files/download-protected",
        DeliveryServiceDocumentFile::createFromModel($deliveryServiceDocument, $deliveryServiceDocument->getFile())->jsonSerialize()
    )
        ->assertStatus(200);
});

test("POST /api/v1/logistic/delivery-service-documents/{id}:delete-file success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'deleteDeliveryServiceDocumentFile' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    postJson("/api/v1/logistic/delivery-service-documents/{$deliveryServiceDocumentId}:delete-file")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceDocumentId);
});
