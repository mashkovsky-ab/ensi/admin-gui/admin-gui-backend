<?php

use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories\AddPaymentMethodsToDeliveryServiceRequestFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories\DeletePaymentMethodFromDeliveryServiceRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("POST /api/v1/logistic/delivery-services/{id}:add-payment-methods success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceId = 1;
    $addPaymentMethodsToDeliveryServiceData = AddPaymentMethodsToDeliveryServiceRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->shouldReceive('addPaymentMethodsToDeliveryService');

    postJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}:add-payment-methods", $addPaymentMethodsToDeliveryServiceData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/delivery-services/{id}:delete-payment-method success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceId = 1;
    $deletePaymentMethodFromDeliveryServiceData = DeletePaymentMethodFromDeliveryServiceRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->shouldReceive('deletePaymentMethodFromDeliveryService');

    postJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}:delete-payment-method", $deletePaymentMethodFromDeliveryServiceData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
