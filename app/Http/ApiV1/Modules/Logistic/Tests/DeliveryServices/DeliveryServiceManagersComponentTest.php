<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceFactory;
use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceManagerFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceManagerRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersRequest;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-service-managers/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryServiceManager' => DeliveryServiceManagerFactory::new()->makeResponseOne(['id' => $deliveryServiceManagerId]),
    ]);

    getJson("/api/v1/logistic/delivery-service-managers/{$deliveryServiceManagerId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceManagerId);
});

test("POST /api/v1/logistic/delivery-service-managers/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;
    $deliveryServiceManagersData = DeliveryServiceManagerRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'createDeliveryServiceManager' => DeliveryServiceManagerFactory::new()->makeResponseOne(['id' => $deliveryServiceManagerId]),
    ]);

    postJson("/api/v1/logistic/delivery-service-managers", $deliveryServiceManagersData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceManagerId);
});

test("PUT /api/v1/logistic/delivery-service-managers/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;
    $deliveryServiceManagersData = DeliveryServiceManagerRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'replaceDeliveryServiceManager' => DeliveryServiceManagerFactory::new()->makeResponseOne(['id' => $deliveryServiceManagerId]),
    ]);

    putJson("/api/v1/logistic/delivery-service-managers/{$deliveryServiceManagerId}", $deliveryServiceManagersData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceManagerId);
});

test("PATCH /api/v1/logistic/delivery-service-managers/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;
    $deliveryServiceManagersData = DeliveryServiceManagerRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'patchDeliveryServiceManager' => DeliveryServiceManagerFactory::new()->makeResponseOne(['id' => $deliveryServiceManagerId]),
    ]);

    patchJson("/api/v1/logistic/delivery-service-managers/{$deliveryServiceManagerId}", $deliveryServiceManagersData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceManagerId);
});

test("DELETE /api/v1/logistic/delivery-service-managers/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;

    $this->mockLogisticDeliveryServicesApi()->shouldReceive('deleteDeliveryServiceManager');

    deleteJson("/api/v1/logistic/delivery-service-managers/{$deliveryServiceManagerId}")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/delivery-service-managers:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServiceManagers' => DeliveryServiceManagerFactory::new()->makeResponseSearch(['id' => $deliveryServiceManagerId]),
    ]);

    postJson("/api/v1/logistic/delivery-service-managers:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryServiceManagerId);
});

test("POST /api/v1/logistic/delivery-service-managers:search all includes 200", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;
    $deliveryServiceId = 2;
    $includes = ["delivery_service"];

    $deliveryService = DeliveryServiceFactory::new()->make(['id' => $deliveryServiceId]);
    $deliveryServiceManagerSearchResponse =
        DeliveryServiceManagerFactory::new()
            ->withDeliveryService($deliveryService)
            ->makeResponseSearch(['id' => $deliveryServiceManagerId]);

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServiceManagers' => DeliveryServiceManagerFactory::new()->makeResponseSearch(['id' => $deliveryServiceManagerId]),
    ]);

    $this->mockLogisticDeliveryServicesApi()
        ->shouldReceive('searchDeliveryServiceManagers')
        ->withArgs(function (SearchDeliveryServiceManagersRequest $arg) use ($includes) {
            assertEquals($includes, $arg->getInclude());

            return true;
        })
        ->andReturn($deliveryServiceManagerSearchResponse);

    postJson("/api/v1/logistic/delivery-service-managers:search", [
        "include" => $includes,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryServiceManagerId)
        ->assertJsonPath('data.0.delivery_service.id', $deliveryServiceId);
});

test("POST /api/v1/logistic/delivery-service-managers:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceManagerId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchOneDeliveryServiceManager' => DeliveryServiceManagerFactory::new()->makeResponseOne(['id' => $deliveryServiceManagerId]),
    ]);

    postJson("/api/v1/logistic/delivery-service-managers:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceManagerId);
});
