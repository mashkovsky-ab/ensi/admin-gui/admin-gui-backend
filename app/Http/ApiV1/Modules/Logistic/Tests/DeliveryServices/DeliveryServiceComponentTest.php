<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-services/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryService' => DeliveryServiceFactory::new()->makeResponseOne(['id' => $deliveryServiceId]),
    ]);

    getJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceId);
});

test("PATCH /api/v1/logistic/delivery-services/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceId = 1;
    $deliveryServicesData = DeliveryServiceRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'patchDeliveryService' => DeliveryServiceFactory::new()->makeResponseOne(['id' => $deliveryServiceId]),
    ]);

    patchJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}", $deliveryServicesData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceId);
});

test("POST /api/v1/logistic/delivery-services:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServices' => DeliveryServiceFactory::new()->makeResponseSearch(['id' => $deliveryServiceId]),
    ]);

    postJson("/api/v1/logistic/delivery-services:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryServiceId);
});

test("POST /api/v1/logistic/delivery-services:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchOneDeliveryService' => DeliveryServiceFactory::new()->makeResponseOne(['id' => $deliveryServiceId]),
    ]);

    postJson("/api/v1/logistic/delivery-services:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceId);
});
