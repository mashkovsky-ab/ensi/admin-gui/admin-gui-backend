<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\Enums\DeliveryMethodFactory;
use App\Domain\Logistic\Tests\DeliveryServices\Factories\Enums\DeliveryServicesStatusFactory;
use App\Domain\Logistic\Tests\DeliveryServices\Factories\Enums\ShipmentMethodFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test('GET /api/v1/logistic/delivery-service-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryServiceStatuses' => DeliveryServicesStatusFactory::new()->makeResponseGet([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/logistic/delivery-service-statuses")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});

test('GET /api/v1/logistic/delivery-methods 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryMethods' => DeliveryMethodFactory::new()->makeResponseGet([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/logistic/delivery-methods")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});

test('GET /api/v1/logistic/shipment-methods 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $name = 'name';

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getShipmentMethods' => ShipmentMethodFactory::new()->makeResponseGet([
            'id' => $id,
            'name' => $name,
        ]),
    ]);

    getJson("/api/v1/logistic/shipment-methods")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});
