<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrdersPaymentMethodEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class AddPaymentMethodsToDeliveryServiceRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'payment_methods' => $this->faker->randomElements(OrdersPaymentMethodEnum::cases()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
