<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class DeliveryServiceDocumentRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'delivery_service_id' => $this->faker->randomElement(LogisticDeliveryServiceEnum::cases()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
