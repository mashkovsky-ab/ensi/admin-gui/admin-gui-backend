<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceDocumentFactory;
use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceDocumentRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-service-documents/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryServiceDocument' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    getJson("/api/v1/logistic/delivery-service-documents/{$deliveryServiceDocumentId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceDocumentId);
});

test("POST /api/v1/logistic/delivery-service-documents/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;
    $deliveryServiceDocumentData = DeliveryServiceDocumentRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'createDeliveryServiceDocument' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    postJson("/api/v1/logistic/delivery-service-documents", $deliveryServiceDocumentData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceDocumentId);
});

test("PUT /api/v1/logistic/delivery-service-documents/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;
    $deliveryServiceDocumentData = DeliveryServiceDocumentRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'replaceDeliveryServiceDocument' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    putJson("/api/v1/logistic/delivery-service-documents/{$deliveryServiceDocumentId}", $deliveryServiceDocumentData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceDocumentId);
});

test("PATCH /api/v1/logistic/delivery-service-documents/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;
    $deliveryServiceDocumentData = DeliveryServiceDocumentRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'patchDeliveryServiceDocument' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    patchJson("/api/v1/logistic/delivery-service-documents/{$deliveryServiceDocumentId}", $deliveryServiceDocumentData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceDocumentId);
});

test("DELETE /api/v1/logistic/delivery-service-documents/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;

    $this->mockLogisticDeliveryServicesApi()->shouldReceive('deleteDeliveryServiceDocument');

    deleteJson("/api/v1/logistic/delivery-service-documents/{$deliveryServiceDocumentId}")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test("POST /api/v1/logistic/delivery-service-documents:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServiceDocuments' => DeliveryServiceDocumentFactory::new()->makeResponseSearch(['id' => $deliveryServiceDocumentId]),
    ]);

    postJson("/api/v1/logistic/delivery-service-documents:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryServiceDocumentId);
});

test("POST /api/v1/logistic/delivery-service-documents:search all includes 200", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;
    $deliveryServiceId = 2;
    $includes = ["delivery_service"];

    $deliveryService = DeliveryServiceFactory::new()->make(['id' => $deliveryServiceId]);
    $deliveryServiceDocumentsSearchRequest =
        DeliveryServiceDocumentFactory::new()
            ->withDeliveryService($deliveryService)
            ->makeResponseSearch(['id' => $deliveryServiceDocumentId]);

    $this->mockLogisticDeliveryServicesApi()
        ->shouldReceive('searchDeliveryServiceDocuments')
        ->withArgs(function (SearchDeliveryServiceDocumentsRequest $arg) use ($includes) {
            assertEquals($includes, $arg->getInclude());

            return true;
        })
        ->andReturn($deliveryServiceDocumentsSearchRequest);

    postJson("/api/v1/logistic/delivery-service-documents:search", [
        "include" => $includes,
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryServiceDocumentId)
        ->assertJsonPath('data.0.delivery_service.id', $deliveryServiceId);
});

test("POST /api/v1/logistic/delivery-service-documents:search-one success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceDocumentId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchOneDeliveryServiceDocument' => DeliveryServiceDocumentFactory::new()->makeResponseOne(['id' => $deliveryServiceDocumentId]),
    ]);

    postJson("/api/v1/logistic/delivery-service-documents:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceDocumentId);
});
