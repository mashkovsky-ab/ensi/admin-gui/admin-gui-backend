<?php

use App\Domain\Catalog\Tests\Factories\Categories\PropertyFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\PropertyRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\EmptyDataResponse;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use Tests\Helpers\Catalog\EnumInfoFactory;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('GET /api/v1/catalog/properties/{id} success', function () {
    $this->mockPimPropertiesApi()->allows([
        'getProperty' => PropertyFactory::new()->withId(10)->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/properties/10')
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'is_multiple', 'created_at', 'is_moderated']]);
});

test('GET /api/v1/catalog/properties/{id}?include=directory success', function () {
    $this->mockPimPropertiesApi()->allows([
        'getProperty' => PropertyFactory::new()->withId(11)->withDirectory()->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/properties/11?include=directory')
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'directory' => [['id', 'name', 'code', 'value']]]]);
});

test('POST /api/v1/catalog/properties:search success', function () {
    $this->mockPimPropertiesApi()->allows([
        'searchProperties' => PropertyFactory::new()->withId(10)->makeResponseSearch(),
    ]);

    postJson('/api/v1/catalog/properties:search')
        ->assertOk()
        ->assertJsonPath('data.0.id', 10)
        ->assertJsonStructure(['data' => [['id', 'name', 'has_directory', 'is_moderated']]]);
});

test('POST /api/v1/catalog/properties success', function () {
    $request = PropertyRequestFactory::new()->make();
    $this->mockPimPropertiesApi()->allows([
        'createProperty' => PropertyFactory::new()->withId(12)->makeResponseOne(),
    ]);

    postJson('/api/v1/catalog/properties', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 12);
});

test('PUT /api/v1/catalog/properties/{id} success', function () {
    $request = PropertyRequestFactory::new()->make();
    $this->mockPimPropertiesApi()->allows([
        'replaceProperty' => PropertyFactory::new()->withId(15)->makeResponseOne(),
    ]);

    putJson('/api/v1/catalog/properties/15', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 15);
});

test('PATCH /api/v1/catalog/properties/{id} success', function () {
    $request = PropertyRequestFactory::new()->make();
    $this->mockPimPropertiesApi()->allows([
        'patchProperty' => PropertyFactory::new()->withId(15)->makeResponseOne(),
    ]);

    patchJson('/api/v1/catalog/properties/15', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 15);
});

test('DELETE /api/v1/catalog/properties/{id} success', function () {
    $this->mockPimPropertiesApi()->allows([
        'deleteProperty' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog/properties/19')
        ->assertOk();
});

test('GET /api/v1/catalog/properties:meta success', function () {
    $this->mockPimEnumsApi()->allows([
        'getPropertyTypesAsync' => EnumInfoFactory::propertyTypes()->makeAsyncResponse(),
    ]);

    getJson('/api/v1/catalog/properties:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
