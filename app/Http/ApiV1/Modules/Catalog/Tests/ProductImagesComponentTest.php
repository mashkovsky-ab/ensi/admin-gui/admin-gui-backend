<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductImageFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductImageRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('PUT /api/v1/catalog/products/{id}/images success', function () {
    $request = ProductImageRequestFactory::new()->makeRequest(3);

    $this->mockPimProductsApi()->allows([
        'replaceProductImages' => ProductImageFactory::new()->makeResponse(3),
    ]);

    putJson('/api/v1/catalog/products/114/images', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['name', 'sort']]]);
});

test('PATCH /api/v1/catalog/products/{id}/images success', function () {
    $request = ProductImageRequestFactory::new()->makeRequest(3);

    $this->mockPimProductsApi()->allows([
        'patchProductImages' => ProductImageFactory::new()->makeResponse(3),
    ]);

    patchJson('/api/v1/catalog/products/165/images', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['name', 'sort']]]);
});
