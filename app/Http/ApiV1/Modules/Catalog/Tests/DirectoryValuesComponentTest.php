<?php

use App\Domain\Catalog\Tests\Factories\Categories\DirectoryValueFactory;
use App\Domain\Catalog\Tests\Factories\PreloadFileFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\DirectoryValueRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\EmptyDataResponse;
use Illuminate\Http\UploadedFile;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/properties/{id}:add-directory success', function () {
    $request = DirectoryValueRequestFactory::new()->make();
    $this->mockPimPropertiesApi()->allows([
        'createDirectoryValue' => DirectoryValueFactory::new()->withId(22)->makeResponseOne(),
    ]);

    postJson('/api/v1/catalog/properties/5:add-directory', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 22);
});

test('PUT /api/v1/catalog/properties/directory/{id} success', function () {
    $request = DirectoryValueRequestFactory::new()->make();
    $this->mockPimPropertiesApi()->allows([
        'replaceDirectoryValue' => DirectoryValueFactory::new()->withId(15)->makeResponseOne(),
    ]);

    putJson('/api/v1/catalog/properties/directory/15', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 15);
});

test('DELETE /api/v1/catalog/properties/directory/{id} success', function () {
    $this->mockPimPropertiesApi()->allows([
        'deleteDirectoryValue' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog/properties/directory/30')
        ->assertOk();
});

test('GET /api/v1/catalog/properties/directory/{id} success', function () {
    $this->mockPimPropertiesApi()->allows([
        'getDirectoryValue' => DirectoryValueFactory::new()->withId(45)->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/properties/directory/45')
        ->assertOk()
        ->assertJsonPath('data.id', 45)
        ->assertJsonStructure(['data' => ['code', 'name', 'id', 'value']]);
});

test('GET /api/v1/catalog/properties/directory/{id} with file success', function () {
    $this->mockPimPropertiesApi()->allows([
        'getDirectoryValue' => DirectoryValueFactory::new()->withFile('https://ya.ru/images/1')->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/properties/directory/45')
        ->assertOk()
        ->assertJsonPath('data.url', 'https://ya.ru/images/1');
});

test('POST /api/v1/catalog/properties/directory:search success', function () {
    $this->mockPimPropertiesApi()->allows([
        'searchDirectoryValues' => DirectoryValueFactory::new()->withId(51)->makeResponseSearch(),
    ]);

    postJson('/api/v1/catalog/properties/directory:search', [])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', 51)
        ->assertJsonStructure(['data' => [['id', 'name', 'value', 'property_id']]]);
});

test('POST /api/v1/catalog/properties/directory:preload-image success', function () {
    $this->mockPimPropertiesApi()->allows([
        'preloadDirectoryValueImage' => PreloadFileFactory::new()->make(),
    ]);

    $file = UploadedFile::fake()->create('foo.png', kilobytes: 20);

    postFile('/api/v1/catalog/properties/directory:preload-image', $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']]);
});
