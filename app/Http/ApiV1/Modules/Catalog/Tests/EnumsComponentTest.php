<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\PimClient\Dto\IntegerEnumInfo;
use Ensi\PimClient\Dto\IntegerEnumInfoResponse;
use Ensi\PimClient\Dto\ProductTypeEnum;
use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('GET /api/v1/catalog/products/product-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = ['id' => ProductTypeEnum::PIECE, 'name' => 'Штучный'];
    $this->mockPimEnumsApi()->allows([
        'getProductTypes' => new IntegerEnumInfoResponse(['data' => [new IntegerEnumInfo($item)]]),
    ]);

    getJson('/api/v1/catalog/products/product-types')
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment($item);
});
