<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductAttributeValueFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductAttributeValueRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('PUT /api/v1/catalog/products/{id}/attributes success', function () {
    $request = ProductAttributeValueRequestFactory::new()->makeRequest(2);

    $this->mockPimProductsApi()->allows([
        'replaceProductAttributes' => ProductAttributeValueFactory::new()->makeResponse(2),
    ]);

    putJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'type']]])
        ->assertOk();
});

test('PATCH /api/v1/catalog/products/{id}/attributes success', function () {
    $request = ProductAttributeValueRequestFactory::new()->makeRequest(2);

    $this->mockPimProductsApi()->allows([
        'patchProductAttributes' => ProductAttributeValueFactory::new()->makeResponse(2),
    ]);

    patchJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'type']]])
        ->assertOk();
});

test('PATCH /api/v1/catalog/products/{id}/attributes preload file', function () {
    $this->mockPimProductsApi()->allows([
        'patchProductAttributes' => ProductAttributeValueFactory::new()->makeResponse(),
    ]);

    $request = ['attributes' => [
        [
            'property_id' => 105,
            'preload_file_id' => 12540,
        ],
    ]];

    patchJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertOk();
});
