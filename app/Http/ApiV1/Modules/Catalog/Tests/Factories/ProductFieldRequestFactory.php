<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\MetricsCategoryEnum;

class ProductFieldRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(3),
            'is_required' => $this->faker->boolean,
            'is_moderated' => $this->faker->boolean,
            'metrics_category' => $this->faker->randomElement(MetricsCategoryEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
