<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\ProductStatusEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Illuminate\Support\Collection;

class ProductRequestFactory extends BaseApiFactory
{
    public ?Collection $attributes = null;
    public ?Collection $images = null;

    protected function definition(): array
    {
        return [
            'external_id' => $this->faker->uuid(),
            'category_id' => $this->faker->randomNumber(),
            'brand_id' => $this->faker->randomNumber(),
            'status_id' => $this->faker->randomElement(ProductStatusEnum::getAllowableEnumValues()),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->text(50),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'allow_publish' => $this->faker->boolean,
            'vendor_code' => $this->faker->numerify('######'),
            'barcode' => $this->faker->ean13(),

            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'is_adult' => $this->faker->boolean(),
            'base_price' => $this->faker->optional()->numberBetween(0, 100_000),

            'attributes' => $this->notNull($this->attributes?->all()),
            'images' => $this->notNull($this->images?->all()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withAttributes(int $count = 1, array $extra = []): self
    {
        $attributes = ProductAttributeValueRequestFactory::new()->makeSeveral($count, $extra);

        return $this->immutableSet('attributes', $attributes);
    }

    public function withImages(int $count = 1, array $extra = []): self
    {
        $images = ProductImageRequestFactory::new()->makeSeveral($count, $extra);

        return $this->immutableSet('images', $images);
    }

    public function onlyRequired(): self
    {
        return $this->only(['name', 'vendor_code', 'type']);
    }
}
