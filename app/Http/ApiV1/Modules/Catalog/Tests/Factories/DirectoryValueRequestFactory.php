<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Faker\Generator;
use Tests\Helpers\Catalog\PropertyValueGenerator;

class DirectoryValueRequestFactory extends BaseApiFactory
{
    public PropertyValueGenerator $valueGenerator;

    public function __construct(Generator $faker)
    {
        parent::__construct($faker);

        $this->valueGenerator = PropertyValueGenerator::new($this->faker);
    }

    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(3),
            'value' => $this->valueGenerator->value(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
