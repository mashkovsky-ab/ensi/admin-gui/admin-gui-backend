<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Domain\Catalog\Tests\Factories\Products\ProductAttributeValueFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class ProductAttributeValueRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $result = [
            'property_id' => $this->faker->randomNumber(),
            'name' => $this->notNull($this->faker->optional->sentence),
        ];

        if ($this->faker->boolean) {
            $result['directory_value_id'] = $this->faker->numberBetween(1, 999);
        } else {
            $type = $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues());
            $result['value'] = ProductAttributeValueFactory::generateValue($type, $this->faker);
        }

        return $result;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeRequest(int $count = 1, array $extra = []): array
    {
        return ['attributes' => $this->makeSeveral($count, $extra)->all()];
    }
}
