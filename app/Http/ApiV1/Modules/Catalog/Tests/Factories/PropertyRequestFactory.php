<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PropertyRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'display_name' => $this->faker->word,
            'code' => $this->faker->slug(2),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'is_multiple' => $this->faker->boolean,
            'is_filterable' => $this->faker->boolean,
            'is_public' => $this->faker->boolean,
            'is_active' => true,
            'is_required' => $this->faker->boolean,
            'hint_value' => $this->faker->sentence,
            'hint_value_name' => $this->faker->sentence,
            'has_directory' => $this->faker->boolean,
            'is_moderated' => $this->faker->boolean,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
