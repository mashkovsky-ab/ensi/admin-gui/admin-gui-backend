<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class CategoryRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(2),
            'code' => $this->faker->slug(2),
            'is_inherits_properties' => $this->faker->boolean,
            'is_active' => $this->faker->boolean,
            'parent_id' =>$this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
