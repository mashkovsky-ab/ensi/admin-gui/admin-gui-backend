<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\File;

class ProductImageRequestFactory extends BaseApiFactory
{
    public ?string $url = null;
    public ?File $file = null;

    protected function definition(): array
    {
        return array_merge(
            [
                'sort' => $this->faker->numberBetween(1, 500),
                'name' => $this->faker->optional->sentence(3),
            ],
            $this->makeCaseData()
        );
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeRequest(int $count = 1, array $extra = []): array
    {
        return ['images' => $this->makeSeveral($count, $extra)->all()];
    }

    private function makeCaseData(): array
    {
        $case = $this->faker->randomElement([1, 2, 3]);

        return match ($case) {
            1 => ['url' => $this->faker->url],
            2 => ['preload_file_id' => $this->faker->randomNumber(6)],
            default => ['id' => $this->faker->randomNumber(5)],
        };
    }
}
