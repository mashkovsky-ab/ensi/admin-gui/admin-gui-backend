<?php

use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\BrandRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\EmptyDataResponse;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/brands success', function () {
    $this->mockPimBrandsApi()->allows([
        'createBrand' => BrandFactory::new()->withId(10)->makeResponseOne(),
    ]);

    $request = BrandRequestFactory::new()->make();

    postJson('/api/v1/catalog/brands', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'image_url']])
        ->assertJsonPath('data.id', 10);
});

test('PUT /api/v1/catalog/brands/{id} success', function () {
    $this->mockPimBrandsApi()->allows([
        'replaceBrand' => BrandFactory::new()->withId(15)->makeResponseOne(),
    ]);

    $request = BrandRequestFactory::new()->make();

    putJson('/api/v1/catalog/brands/15', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'description']])
        ->assertJsonPath('data.id', 15);
});

test('DELETE /api/v1/catalog/brands/{id} success', function () {
    $this->mockPimBrandsApi()->allows(['deleteBrand' => new EmptyDataResponse()]);

    deleteJson('/api/v1/catalog/brands/21')
        ->assertOk();
});

test('GET /api/v1/catalog/brands/{id} internal logo success', function () {
    $expectedUrl = 'https://files.ensi.ru/public/catalog/1234.jpg';

    $this->mockPimBrandsApi()->allows([
        'getBrand' => BrandFactory::new()
            ->withId(25)
            ->withFileUrl($expectedUrl)
            ->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/brands/25')
        ->assertOk()
        ->assertJsonPath('data.image_url', $expectedUrl)
        ->assertJsonPath('data.logo_url', null);
});

test('GET /api/v1/catalog/brands/{id} external logo success', function () {
    $expectedUrl = 'https://ya.ru/images/1234.jpg';

    $this->mockPimBrandsApi()->allows([
        'getBrand' => BrandFactory::new()
            ->withId(27)
            ->withExternalLogo($expectedUrl)
            ->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/brands/27')
        ->assertOk()
        ->assertJsonPath('data.image_url', $expectedUrl)
        ->assertJsonPath('data.logo_url', $expectedUrl);
});

test('POST /api/v1/catalog/brands:search success', function () {
    $this->mockPimBrandsApi()->allows([
        'searchBrands' => BrandFactory::new()->withId(33)->makeResponseSearch([], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/brands:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'name', 'image_url']]])
        ->assertJsonPath('data.0.id', 33);
});

test('GET /api/v1/catalog/brands:meta success', function () {
    getJson('/api/v1/catalog/brands:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
