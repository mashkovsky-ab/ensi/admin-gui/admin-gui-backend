<?php

use App\Domain\Catalog\Tests\Factories\Categories\CategoriesTreeItemFactory;
use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\CategoryRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\PimClient\Dto\EmptyDataResponse;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/categories success', function () {
    $this->mockPimCategoriesApi()->allows([
        'createCategory' => CategoryFactory::new()->withId(10)->makeResponseOne(),
    ]);
    $request = CategoryRequestFactory::new()->make(['parent_id' => null]);

    postJson('/api/v1/catalog/categories', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 10);
});

test('PUT /api/v1/catalog/categories/{id} success', function () {
    $this->mockPimCategoriesApi()->allows([
        'replaceCategory' => CategoryFactory::new()->withId(12)->makeResponseOne(),
    ]);

    $request = CategoryRequestFactory::new()->make();

    putJson('/api/v1/catalog/categories/12', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 12);
});

test('DELETE /api/v1/catalog/categories/{id} success', function () {
    $this->mockPimCategoriesApi()->allows([
        'deleteCategory' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog/categories/50', [])
        ->assertOk();
});

test('GET /api/v1/catalog/categories/{id}?include=properties success', function () {
    $this->mockPimCategoriesApi()->allows([
        'getCategory' => CategoryFactory::new()->withId(30)->withProperties(2)->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/categories/30?include=properties')
        ->assertOk()
        ->assertJsonPath('data.id', 30)
        ->assertJsonCount(2, 'data.properties')
        ->assertJsonStructure(['data' => ['properties' => [['property_id', 'name', 'type', 'is_system', 'is_moderated']]]]);
});

test('POST /api/v1/catalog/categories:search success', function () {
    $this->mockPimCategoriesApi()->allows([
        'searchCategories' => CategoryFactory::new()
            ->withId(10)
            ->withProperties(2)
            ->makeResponseSearch([], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
        'include' => ['properties'],
    ];

    postJson('/api/v1/catalog/categories:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonCount(2, 'data.0.properties');
});

test('POST /api/v1/catalog/categories/{id}:bind-properties success', function () {
    $this->mockPimCategoriesApi()->allows([
        'bindCategoryProperties' => CategoryFactory::new()->withId(22)->withProperties(1)->makeResponseOne(),
    ]);

    $request = [
        'replace' => false,
        'properties' => [
            ['id' => 100, 'is_required' => false],
        ],
    ];

    postJson('/api/v1/catalog/categories/22:bind-properties', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 22);
});

test('POST /api/v1/catalog/categories:tree success', function () {
    $this->mockPimCategoriesApi()->allows([
        'getCategoriesTree' => CategoriesTreeItemFactory::new()
            ->withChildren(2)
            ->depth(2)
            ->makeResponse(2),
    ]);

    $request = [
        'filter' => ['is_active' => true],
    ];

    postJson('/api/v1/catalog/categories:tree', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonCount(2, 'data.0.children');
});

test('GET /api/v1/catalog/categories:meta success', function () {
    getJson('/api/v1/catalog/categories:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
