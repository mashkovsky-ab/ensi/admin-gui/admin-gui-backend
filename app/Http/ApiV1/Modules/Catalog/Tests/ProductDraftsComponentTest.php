<?php

use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Domain\Catalog\Tests\Factories\PreloadFileFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductDraftFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\PimClient\Dto\EmptyDataResponse;
use Illuminate\Http\UploadedFile;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/products success', function () {
    $request = ProductRequestFactory::new()->make();
    $this->mockPimProductsApi()->allows([
        'createProduct' => ProductDraftFactory::new()->withId(10)->makeResponseOne(),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([]),
        'createOffer' => OfferFactory::new()->withId(25)->makeResponseOne(),
    ]);

    postJson('/api/v1/catalog/products', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand_id', 'category_id']])
        ->assertJsonPath('data.id', 10);
});

test('POST /api/v1/catalog/products only required', function () {
    $request = ProductRequestFactory::new()->onlyRequired()->make();

    $this->mockPimProductsApi()->allows([
        'createProduct' => ProductDraftFactory::new()->withId(10)->makeResponseOne(),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([]),
        'createOffer' => OfferFactory::new()->withId(25)->makeResponseOne(),
    ]);

    postJson('/api/v1/catalog/products', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'vendor_code', 'category_id']])
        ->assertJsonPath('data.id', 10);
});

test('POST /api/v1/catalog/products with images', function () {
    $request = ProductRequestFactory::new()->withImages(2)->make();

    $this->mockPimProductsApi()->allows([
        'createProduct' => ProductDraftFactory::new()->withId(10)->withImages()->makeResponseOne(),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([]),
        'createOffer' => OfferFactory::new()->withId(25)->makeResponseOne(),
    ]);

    postJson('/api/v1/catalog/products', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['images' => [['id', 'sort', 'name']]]]);
});

test('PATCH /api/v1/catalog/products/{id} success', function () {
    $request = ProductRequestFactory::new()->make();

    $this->mockPimProductsApi()->allows([
        'patchProduct' => ProductDraftFactory::new()->withId(25)->makeResponseOne(),
    ]);

    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withId(30)->makeResponseSearch(),
        'patchOffer' => OfferFactory::new()->withId(30)->makeResponseOne(),
    ]);

    patchJson('/api/v1/catalog/products/25', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand_id', 'category_id', 'base_price']])
        ->assertJsonPath('data.id', 25);
});

test('PATCH /api/v1/catalog/products/{id} updates offer price', function () {
    $request = ProductRequestFactory::new()->make();

    $this->mockPimProductsApi()->allows([
        'patchProduct' => ProductDraftFactory::new()->withId(25)->makeResponseOne(),
    ]);

    $patchResponse = OfferFactory::new()->withId(30)->makeResponseOne();
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withId(30)->makeResponseSearch(),
        'patchOffer' => $patchResponse,
    ]);

    patchJson('/api/v1/catalog/products/25', $request)
        ->assertOk()
        ->assertJsonPath('data.base_price', $patchResponse->getData()->getBasePrice());
});

test('PATCH /api/v1/catalog/products/{id} with attributes and images', function () {
    $request = ProductRequestFactory::new()
        ->withAttributes(2)
        ->withImages(3)
        ->make();

    $this->mockPimProductsApi()->allows([
        'patchProduct' => ProductDraftFactory::new()->withId(25)->makeResponseOne(),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withId(30)->makeResponseSearch(),
        'patchOffer' => OfferFactory::new()->withId(30)->makeResponseOne(),
    ]);

    patchJson('/api/v1/catalog/products/25', $request)
        ->assertOk();
});

test('PUT /api/v1/catalog/products/{id} success', function () {
    $request = ProductRequestFactory::new()->make();

    $this->mockPimProductsApi()->allows([
        'replaceProduct' => ProductDraftFactory::new()->withId(151)->makeResponseOne(),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withId(30)->makeResponseSearch(),
        'patchOffer' => OfferFactory::new()->withId(30)->makeResponseOne(),
    ]);

    putJson('/api/v1/catalog/products/25', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand_id', 'category_id']])
        ->assertJsonPath('data.id', 151);
});

test('PUT /api/v1/catalog/products/{id} updates offer price', function () {
    $request = ProductRequestFactory::new()->make();

    $this->mockPimProductsApi()->allows([
        'replaceProduct' => ProductDraftFactory::new()->withId(151)->makeResponseOne(),
    ]);

    $patchResponse = OfferFactory::new()->withId(30)->makeResponseOne();
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withId(30)->makeResponseSearch(),
        'patchOffer' => $patchResponse,
    ]);

    putJson('/api/v1/catalog/products/25', $request)
        ->assertOk()
        ->assertJsonPath('data.base_price', $patchResponse->getData()->getBasePrice());
});

test('PUT /api/v1/catalog/products/{id} with attributes and images', function () {
    $request = ProductRequestFactory::new()
        ->withAttributes(3)
        ->withImages(2)
        ->make();

    $this->mockPimProductsApi()->allows([
        'replaceProduct' => ProductDraftFactory::new()->withId(25)->makeResponseOne(),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withId(30)->makeResponseSearch(),
        'patchOffer' => OfferFactory::new()->withId(30)->makeResponseOne(),
    ]);

    putJson('/api/v1/catalog/products/25', $request)
        ->assertOk();
});

test('DELETE /api/v1/catalog/products/{id} success', function () {
    $this->mockPimProductsApi()->allows(['deleteProduct' => new EmptyDataResponse()]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->withId(30)->makeResponseSearch(),
        'patchOffer' => OfferFactory::new()->withId(30)->makeResponseOne(),
    ]);

    deleteJson('/api/v1/catalog/products/47')
        ->assertOk();
});

test('GET /api/v1/catalog/products/drafts/{id} success', function () {
    $this->mockPimProductsApi()->allows([
        'getProductDraft' => ProductDraftFactory::new()->withId(201)->makeResponseOne(),
    ]);

    getJson('/api/v1/catalog/products/drafts/201')
        ->assertOk()
        ->assertJsonPath('data.id', 201);
});

test('GET /api/v1/catalog/products/drafts/{id} include brand and category', function () {
    $response = ProductDraftFactory::new()
        ->withId(205)
        ->withBrand()
        ->withCategory()
        ->makeResponseOne();

    $this->mockPimProductsApi()->allows(['getProductDraft' => $response]);

    getJson('/api/v1/catalog/products/drafts/205?include=brand,category')
        ->assertOk()
        ->assertJsonStructure(['data' => ['brand' => ['id', 'name'], 'category' => ['id', 'name']]]);
});

test('GET /api/v1/catalog/products/drafts/{id} include attributes', function () {
    $response = ProductDraftFactory::new()
        ->withId(205)
        ->withAttributes(2)
        ->makeResponseOne();

    $this->mockPimProductsApi()->allows(['getProductDraft' => $response]);

    getJson('/api/v1/catalog/products/drafts/205?include=brand,category')
        ->assertOk()
        ->assertJsonStructure(['data' => ['attributes' => [['property_id', 'value']]]]);
});

test('POST /api/v1/catalog/products/drafts:search success', function () {
    $this->mockPimProductsApi()->allows([
        'searchProductDrafts' => ProductDraftFactory::new()->withId(38)->makeResponseSearch([], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products/drafts:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'category_id']]])
        ->assertJsonPath('data.0.id', 38);
});

test('POST /api/v1/catalog/products:preload-image success', function () {
    $this->mockPimProductsApi()->allows([
        'preloadProductImage' => PreloadFileFactory::new()->make(),
    ]);

    $file = UploadedFile::fake()->create('foo.png', kilobytes: 20);

    postFile('/api/v1/catalog/products:preload-image', $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']]);
});
