<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Classifiers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Brand;

/**
 * @mixin Brand
 */
class BrandsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'is_active' => $this->getIsActive(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'description' => $this->getDescription(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'logo_url' => $this->getLogoUrl(),
            'image_url' => $this->fileUrl($this->getLogoFile()) ?? $this->getLogoUrl(),
        ];
    }
}
