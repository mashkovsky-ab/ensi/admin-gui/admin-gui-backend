<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Offers;

use App\Http\ApiV1\Modules\Catalog\Resources\Stocks\StocksResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OffersClient\Dto\Offer;

/**
 * @mixin Offer
 */
class OffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'product_id' => $this->getProductId(),
            'seller_id' => $this->getSellerId(),
            'external_id' => $this->getExternalId(),
            'sale_status' => $this->getSaleStatus(),
            'storage_address' => $this->getStorageAddress(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            'base_price' => $this->getBasePrice(),
            'stocks' => StocksResource::collection($this->whenLoaded('stocks')),
        ];
    }
}
