<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\OpenApiGenerated\Enums\CatalogFieldSettingsMaskEnum;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductField;

/**
 * @mixin ProductField
 */
class ProductFieldsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'edit_mask' => $this->expandEditMask(),

            'name' => $this->getName(),
            'is_required' => $this->getIsRequired(),
            'is_moderated' => $this->getIsModerated(),
            'metric_category' => $this->getMetricsCategory(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }

    private function expandEditMask(): array
    {
        $mask = $this->getEditMask();

        return array_filter(
            CatalogFieldSettingsMaskEnum::cases(),
            fn (int $bit) => ($mask & $bit) !== 0
        );
    }
}
