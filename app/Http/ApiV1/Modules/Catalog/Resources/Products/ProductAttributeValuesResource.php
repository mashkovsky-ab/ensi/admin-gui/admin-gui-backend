<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductAttributeValue;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Webmozart\Assert\Assert;

/**
 * @mixin ProductAttributeValue
 */
class ProductAttributeValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'property_id' => $this->getPropertyId(),
            'type' => $this->getType(),
            'value' => $this->convertValue($this->getType(), $this->getValue()),
            'name' => $this->getName(),
            'directory_value_id' => $this->whenNotNull($this->getDirectoryValueId()),
        ];
    }

    private function convertValue(string $type, mixed $value): mixed
    {
        Assert::oneOf($type, PropertyTypeEnum::getAllowableEnumValues());

        if ($value === null) {
            return null;
        }

        return match ($type) {
            PropertyTypeEnum::BOOLEAN => filter_var($value, FILTER_VALIDATE_BOOLEAN),
            PropertyTypeEnum::INTEGER => (int)$value,
            PropertyTypeEnum::DOUBLE => (float)$value,
            default => $value,
        };
    }
}
