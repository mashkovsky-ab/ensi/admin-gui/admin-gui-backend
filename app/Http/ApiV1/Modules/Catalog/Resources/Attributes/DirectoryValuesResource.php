<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Attributes;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\AttributeDirectoryValue;
use Ensi\PimClient\Dto\DirectoryValue;

/**
 * @mixin AttributeDirectoryValue
 * @mixin DirectoryValue
 */
class DirectoryValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'type' => $this->getType(),
            'value' => $this->getValue(),
            'url' => $this->whenNotNull($this->getFile()?->getUrl()),

            'property_id' => $this->when(
                method_exists($this->resource, 'getPropertyId'),
                fn () => $this->getPropertyId()
            ),
        ];
    }
}
