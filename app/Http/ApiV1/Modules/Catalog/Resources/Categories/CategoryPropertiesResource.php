<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Categories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\CategoryBoundProperty;

/**
 * @mixin CategoryBoundProperty
 */
class CategoryPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'property_id' => $this->getPropertyId(),
            'name' => $this->getName(),
            'display_name' => $this->getDisplayName(),
            'code' => $this->getCode(),
            'type' => $this->getType(),

            'is_multiple' => $this->getIsMultiple(),
            'is_filterable' => $this->getIsFilterable(),
            'is_public' => $this->getIsPublic(),
            'is_active' => $this->getIsActive(),
            'is_required' => $this->getIsRequired(),
            'is_inherited' => $this->getIsInherited(),
            'is_common' => $this->getIsCommon(),
            'is_system' => $this->getIsSystem(),
            'is_moderated' => $this->getIsModerated(),

            'hint_value' => $this->getHintValue(),
            'hint_value_name' => $this->getHintValueName(),
            'has_directory' => $this->getHasDirectory(),
        ];
    }
}
