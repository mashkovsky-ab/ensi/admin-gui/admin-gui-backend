<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\PreloadFileData;

/**
 * @mixin PreloadFileData
 */
class PreloadFileResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'preload_file_id' => $this->getPreloadFileId(),
            'url' => $this->getUrl(),
        ];
    }
}
