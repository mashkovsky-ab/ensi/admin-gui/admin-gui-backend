<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\Support\Pagination\Page;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Api\SearchApi;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProductsQuery
{
    private static array $indexFilters = [
        'active',
        'qty_from',
        'qty_to',
        'price_from',
        'price_to',
    ];

    public function __construct(
        private Request $request,
        private ProductsApi $productsApi,
        private SearchApi $searchApi
    ) {
    }

    public function get(): Page
    {
        $indexFilters = Arr::only($this->request->get('filter') ?? [], self::$indexFilters);

        return !$indexFilters ? $this->getFromPim() : $this->getFromIndex();
    }

    public function find($id): mixed
    {
        return $this->createPimQuery()->find($id);
    }

    private function getFromIndex(): Page
    {
        $page = $this->createIndexQuery()->get();
        if (!$page->items) {
            return $page;
        }

        $pimRequest = new Request([
            'filter' => ['id' => Arr::pluck($page->items, 'id')],
            'include' => $this->request->get('include') ?? '',
        ]);
        $page->items = $this->getFromPim($pimRequest)->items;

        return $page;
    }

    private function getFromPim(?Request $request = null): Page
    {
        return $this->createPimQuery($request)->get();
    }

    private function createPimQuery(?Request $request = null): ProductsPimQuery
    {
        return new ProductsPimQuery($request ?? $this->request, $this->productsApi);
    }

    private function createIndexQuery(): ProductsIndexQuery
    {
        return new ProductsIndexQuery($this->request, $this->searchApi);
    }
}
