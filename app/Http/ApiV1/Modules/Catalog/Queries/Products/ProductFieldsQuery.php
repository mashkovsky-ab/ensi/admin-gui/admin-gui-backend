<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ProductFieldsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductFieldResponse;
use Ensi\PimClient\Dto\SearchProductFieldsRequest;
use Ensi\PimClient\Dto\SearchProductFieldsResponse;
use Illuminate\Http\Request;

class ProductFieldsQuery extends PimQuery
{
    public function __construct(Request $request, private ProductFieldsApi $api)
    {
        parent::__construct($request, SearchProductFieldsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): ProductFieldResponse
    {
        return $this->api->getProductField($id);
    }

    protected function search($request): SearchProductFieldsResponse
    {
        return $this->api->searchProductFields($request);
    }
}
