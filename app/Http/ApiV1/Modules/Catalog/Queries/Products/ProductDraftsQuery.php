<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductDraftResponse;
use Ensi\PimClient\Dto\SearchProductDraftsRequest;
use Ensi\PimClient\Dto\SearchProductDraftsResponse;
use Illuminate\Http\Request;

class ProductDraftsQuery extends PimQuery
{
    public function __construct(Request $request, private ProductsApi $api)
    {
        parent::__construct($request, SearchProductDraftsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): ProductDraftResponse
    {
        return $this->api->getProductDraft($id, $this->getInclude());
    }

    protected function search($request): SearchProductDraftsResponse
    {
        return $this->api->searchProductDrafts($request);
    }
}
