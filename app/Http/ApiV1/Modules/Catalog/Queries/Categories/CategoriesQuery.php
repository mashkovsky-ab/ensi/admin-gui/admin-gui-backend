<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Categories;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\CategoryResponse;
use Ensi\PimClient\Dto\SearchCategoriesRequest;
use Ensi\PimClient\Dto\SearchCategoriesResponse;
use Illuminate\Http\Request;

class CategoriesQuery extends PimQuery
{
    public function __construct(Request $request, private CategoriesApi $api)
    {
        parent::__construct($request, SearchCategoriesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): CategoryResponse
    {
        return $this->api->getCategory($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchCategoriesResponse
    {
        return $this->api->searchCategories($request);
    }
}
