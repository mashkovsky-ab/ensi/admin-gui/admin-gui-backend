<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Variants;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\VariantsApi;
use Ensi\PimClient\Dto\SearchVariantGroupsRequest;
use Ensi\PimClient\Dto\SearchVariantGroupsResponse;
use Ensi\PimClient\Dto\VariantGroupResponse;
use Illuminate\Http\Request;

class VariantGroupsQuery extends PimQuery
{
    public function __construct(Request $request, private VariantsApi $api)
    {
        parent::__construct($request, SearchVariantGroupsRequest::class);
    }

    protected function searchById($id): VariantGroupResponse
    {
        return $this->api->getVariantGroup($id, $this->getInclude());
    }

    protected function search($request): SearchVariantGroupsResponse
    {
        return $this->api->searchVariantGroups($request);
    }
}
