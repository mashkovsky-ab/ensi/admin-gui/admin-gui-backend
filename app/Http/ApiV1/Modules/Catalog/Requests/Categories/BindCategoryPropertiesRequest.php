<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Categories;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class BindCategoryPropertiesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'replace' => ['sometimes', 'boolean'],
            'properties' => ['sometimes', 'array'],
            'properties.*.id' => ['required'],
            'properties.*.is_required' => ['required', 'boolean'],
        ];
    }
}
