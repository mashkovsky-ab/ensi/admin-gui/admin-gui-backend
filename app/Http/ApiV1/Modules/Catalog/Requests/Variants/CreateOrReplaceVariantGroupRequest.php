<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Variants;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceVariantGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'product_ids' => ['required', 'array'],
            'product_ids.*' => ['required', 'integer', 'distinct'],
            'attribute_ids' => ['required', 'array'],
            'attribute_ids.*' => ['required', 'integer', 'distinct'],
        ];
    }
}
