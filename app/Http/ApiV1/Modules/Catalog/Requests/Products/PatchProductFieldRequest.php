<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\MetricsCategoryEnum;
use Illuminate\Validation\Rule;

class PatchProductFieldRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'is_moderated' => ['sometimes', 'required', 'boolean'],
            'is_required' => ['sometimes', 'required', 'boolean'],
            'metrics_category' => ['sometimes', 'nullable', Rule::in(MetricsCategoryEnum::getAllowableEnumValues())],
        ];
    }
}
