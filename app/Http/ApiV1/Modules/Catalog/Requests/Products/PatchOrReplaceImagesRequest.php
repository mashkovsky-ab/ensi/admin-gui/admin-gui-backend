<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchOrReplaceImagesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return array_merge(
            ['images' => ['present', 'array']],
            self::itemRules()
        );
    }

    public function images(): array
    {
        return $this->validated()['images'];
    }

    public static function itemRules(): array
    {
        return [
            'images.*.name' => ['sometimes', 'nullable', 'string'],
            'images.*.sort' => ['sometimes', 'integer'],

            'images.*.id' => [
                'required_without_all:images.*.preload_file_id,images.*.url',
                'integer',
            ],

            'images.*.preload_file_id' => [
                'prohibits:images.*.id,images.*.url',
                'integer',
            ],

            'images.*.url' => [
                'prohibits:images.*.id,images.*.preload_file_id',
                'url',
            ],
        ];
    }
}
