<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\ProductStatusEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Illuminate\Validation\Rule;

class PatchProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $fieldRules = [
            'name' => ['sometimes', 'required', 'string'],
            'category_id' => ['sometimes', 'required', 'integer'],
            'allow_publish' => ['sometimes', 'required', 'boolean'],
            'type' => ['sometimes', 'required', Rule::in(ProductTypeEnum::getAllowableEnumValues())],

            'code' => ['sometimes', 'nullable', 'string'],
            'description' => ['sometimes', 'nullable', 'string'],
            'vendor_code' => ['sometimes', 'nullable', 'string'],
            'barcode' => ['sometimes', 'nullable', 'string'],
            'external_id' => ['sometimes', 'nullable', 'string'],

            'brand_id' => ['sometimes', 'nullable', 'integer'],

            'weight' => ['sometimes', 'nullable', 'numeric'],
            'weight_gross' => ['sometimes', 'nullable', 'numeric'],
            'length' => ['sometimes', 'nullable', 'numeric'],
            'width' => ['sometimes', 'nullable', 'numeric'],
            'height' => ['sometimes', 'nullable', 'numeric'],
            'is_adult' => ['sometimes', 'nullable', 'boolean'],
            'base_price' => ['sometimes', 'nullable', 'integer'],

            'status_id' => ['sometimes', 'required', Rule::in(ProductStatusEnum::getAllowableEnumValues())],
            'status_comment' => ['sometimes', 'nullable', 'string'],
        ];

        return array_merge(
            $fieldRules,
            PatchOrReplaceAttributesRequest::itemRules(),
            PatchOrReplaceImagesRequest::itemRules()
        );
    }
}
