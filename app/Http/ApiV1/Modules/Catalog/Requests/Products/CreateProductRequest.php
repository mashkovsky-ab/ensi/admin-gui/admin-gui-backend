<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Illuminate\Validation\Rule;

class CreateProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $fieldRules = [
            'name' => ['required', 'string'],
            'type' => ['required', Rule::in(ProductTypeEnum::getAllowableEnumValues())],
            'category_id' => ['nullable', 'integer'],
            'brand_id' => ['nullable', 'integer'],
            'allow_publish' => ['sometimes', 'boolean'],

            'code' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'vendor_code' => ['nullable', 'string'],
            'barcode' => ['nullable', 'string'],
            'external_id' => ['nullable', 'string'],

            'weight' => ['nullable', 'numeric'],
            'weight_gross' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'is_adult' => ['nullable', 'boolean'],

            'base_price' => ['nullable', 'integer'],
        ];

        return array_merge($fieldRules, PatchOrReplaceImagesRequest::itemRules());
    }
}
