<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Attributes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceDirectoryValueRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'value' => ['required_without:preload_file_id'],
            'name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'preload_file_id' => ['sometimes', 'integer'],
        ];
    }
}
