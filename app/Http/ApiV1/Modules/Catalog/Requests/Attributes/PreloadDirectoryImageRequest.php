<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Attributes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Http\UploadedFile;

class PreloadDirectoryImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }

    public function image(): UploadedFile
    {
        return $this->file('file');
    }
}
