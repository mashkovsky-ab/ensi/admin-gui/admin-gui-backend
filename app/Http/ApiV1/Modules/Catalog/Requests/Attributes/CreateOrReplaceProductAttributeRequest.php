<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Attributes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Illuminate\Validation\Rule;

class CreateOrReplaceProductAttributeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'type' => ['required', Rule::in(PropertyTypeEnum::getAllowableEnumValues())],
            'display_name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'hint_value' => ['nullable', 'string'],
            'hint_value_name' => ['nullable', 'string'],

            'is_multiple' => ['boolean'],
            'is_filterable' => ['boolean'],
            'is_public' => ['boolean'],
            'is_active' => ['boolean'],
            'has_directory' => ['boolean'],
            'is_moderated' => ['boolean'],

            'is_required' => ['sometimes', 'boolean'],
        ];
    }
}
