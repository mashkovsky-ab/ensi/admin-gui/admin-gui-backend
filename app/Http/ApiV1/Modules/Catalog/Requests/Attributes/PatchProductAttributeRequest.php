<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Attributes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Illuminate\Validation\Rule;

class PatchProductAttributeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'type' => ['sometimes', Rule::in(PropertyTypeEnum::getAllowableEnumValues())],
            'display_name' => ['sometimes', 'string'],
            'code' => ['sometimes', 'nullable', 'string'],
            'hint_value' => ['sometimes', 'nullable', 'string'],
            'hint_value_name' => ['sometimes', 'nullable', 'string'],
            'is_multiple' => ['sometimes', 'boolean'],
            'is_filterable' => ['sometimes', 'boolean'],
            'is_public' => ['sometimes', 'boolean'],
            'is_active' => ['sometimes', 'boolean'],
            'has_directory' => ['sometimes', 'boolean'],
            'is_moderated' => ['sometimes', 'boolean'],
        ];
    }
}
