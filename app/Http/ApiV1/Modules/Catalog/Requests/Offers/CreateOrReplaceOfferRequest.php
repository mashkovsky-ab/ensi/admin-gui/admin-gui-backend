<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Offers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Illuminate\Validation\Rule;

class CreateOrReplaceOfferRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => ['required', 'integer'],
            'seller_id' => ['required', 'integer'],
            'external_id' => ['required', 'string'],
            'sale_status' => ['required', 'integer', Rule::in(OfferSaleStatusEnum::getAllowableEnumValues())],
            'storage_address' => ['nullable', 'string'],
            'base_price' => ['nullable', 'integer'],
        ];
    }
}
