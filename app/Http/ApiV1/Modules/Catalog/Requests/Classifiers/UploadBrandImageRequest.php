<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Classifiers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadBrandImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }
}
