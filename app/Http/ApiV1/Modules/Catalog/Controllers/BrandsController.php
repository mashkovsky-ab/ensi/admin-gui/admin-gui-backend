<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Classifiers\Brands\CreateBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\DeleteBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\DeleteBrandImageAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\ReplaceBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\SaveBrandImageAction;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Classifiers\BrandsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\CreateOrReplaceBrandRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\UploadBrandImageRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\BrandsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BrandsController
{
    public function search(BrandsQuery $query): AnonymousResourceCollection
    {
        return BrandsResource::collectPage($query->get());
    }

    public function create(
        CreateOrReplaceBrandRequest $request,
        CreateBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($request->validated()));
    }

    public function get(
        int $id,
        BrandsQuery $query
    ): BrandsResource {
        return new BrandsResource($query->find($id));
    }

    public function replace(
        int $id,
        CreateOrReplaceBrandRequest $request,
        ReplaceBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id, $request->validated()));
    }

    public function delete(
        int $id,
        DeleteBrandAction $action
    ): EmptyResource {
        $action->execute($id);

        return new EmptyResource();
    }

    public function uploadImage(
        int $id,
        UploadBrandImageRequest $request,
        SaveBrandImageAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id, $request->file('file')));
    }

    public function deleteImage(
        int $id,
        DeleteBrandImageAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id));
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Наименование')->sort()->filterDefault(),
            Field::keyword('code', 'Код')->sort(),
            Field::string('description', 'Описание'),

            Field::boolean('is_active', 'Активен')->filter(),
            Field::string('image_url', 'URL логотипа'),

            Field::datetime('created_at', 'Дата создания'),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
