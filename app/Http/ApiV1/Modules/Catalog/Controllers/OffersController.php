<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Offers\CreateOfferAction;
use App\Domain\Catalog\Actions\Offers\DeleteOfferAction;
use App\Domain\Catalog\Actions\Offers\ReplaceOfferAction;
use App\Http\ApiV1\Modules\Catalog\Requests\Offers\CreateOrReplaceOfferRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Offers\OffersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class OffersController
{
    public function create(CreateOrReplaceOfferRequest $request, CreateOfferAction $action): OffersResource
    {
        return new OffersResource($action->execute($request->validated()));
    }

    public function replace(
        int $productId,
        CreateOrReplaceOfferRequest $request,
        ReplaceOfferAction $action
    ): OffersResource {
        return new OffersResource($action->execute($productId, $request->validated()));
    }

    public function delete(int $offerId, DeleteOfferAction $action): EmptyResource
    {
        $action->execute($offerId);

        return new EmptyResource();
    }
}
