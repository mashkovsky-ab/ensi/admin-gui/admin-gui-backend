<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Attributes\CreateProductAttributeAction;
use App\Domain\Catalog\Actions\Attributes\DeleteProductAttributeAction;
use App\Domain\Catalog\Actions\Attributes\PatchProductAttributeAction;
use App\Domain\Catalog\Actions\Attributes\ReplaceProductAttributeAction;
use App\Domain\Common\Actions\AsyncLoadAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\PropertyTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Attributes\ProductAttributesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Attributes\CreateOrReplaceProductAttributeRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Attributes\PatchProductAttributeRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Attributes\ProductAttributesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PropertiesController
{
    public function create(
        CreateOrReplaceProductAttributeRequest $request,
        CreateProductAttributeAction $action
    ): ProductAttributesResource {
        return new ProductAttributesResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        CreateOrReplaceProductAttributeRequest $request,
        ReplaceProductAttributeAction $action
    ): ProductAttributesResource {
        return new ProductAttributesResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchProductAttributeRequest $request,
        PatchProductAttributeAction $action
    ): ProductAttributesResource {
        return new ProductAttributesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductAttributeAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(ProductAttributesQuery $query): AnonymousResourceCollection
    {
        return ProductAttributesResource::collectPage($query->get());
    }

    public function get(int $id, ProductAttributesQuery $query): ProductAttributesResource
    {
        return new ProductAttributesResource($query->find($id));
    }

    public function meta(AsyncLoadAction $action, PropertyTypeEnumInfo $types): ModelMetaResource
    {
        $action->execute([$types]);

        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Наименование')->sort()->filterDefault(),
            Field::keyword('code', 'Код'),
            Field::boolean('is_filterable', 'Фильтр на витрине')->filter(),
            Field::boolean('is_system', 'Системный')->filter(),
            Field::boolean('is_common', 'Общий')->filter(),
            Field::boolean('is_required', 'Обязательный')->filter(),
            Field::boolean('is_active', 'Активный')->filter()->filterDefault(),
            Field::enum('type', 'Тип данных', $types)->filter()->filterDefault(),
            Field::datetime('created_at', 'Дата создания'),
            Field::datetime('updated_at', 'Дата обновления'),
            Field::string('display_name', 'Публичное название'),
            Field::string('hint_value', 'Подсказка для значения'),
            Field::string('hint_value_name', 'Подсказка для наименования значения'),
            Field::boolean('is_multiple', 'Множественный'),
            Field::boolean('is_public', 'Выводить на витрине'),
            Field::boolean('has_directory', 'Справочник'),
        ]);
    }
}
