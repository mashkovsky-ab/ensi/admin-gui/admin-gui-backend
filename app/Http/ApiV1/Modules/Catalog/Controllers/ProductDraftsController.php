<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Products\CreateProductAction;
use App\Domain\Catalog\Actions\Products\DeleteProductAction;
use App\Domain\Catalog\Actions\Products\PatchAttributeValuesAction;
use App\Domain\Catalog\Actions\Products\PatchProductAction;
use App\Domain\Catalog\Actions\Products\PatchProductImagesAction;
use App\Domain\Catalog\Actions\Products\PreloadProductImageAction;
use App\Domain\Catalog\Actions\Products\ReplaceAttributeValuesAction;
use App\Domain\Catalog\Actions\Products\ReplaceProductAction;
use App\Domain\Catalog\Actions\Products\ReplaceProductImagesAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ProductDraftsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\CreateProductRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchOrReplaceAttributesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchOrReplaceImagesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchProductRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PreloadProductImageRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\ReplaceProductRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\PreloadFileResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductAttributeValuesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductDraftsResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductImagesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductDraftsController
{
    public function create(CreateProductRequest $request, CreateProductAction $action): ProductDraftsResource
    {
        return new ProductDraftsResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplaceProductRequest $request,
        ReplaceProductAction $action
    ): ProductDraftsResource {
        return new ProductDraftsResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchProductRequest $request,
        PatchProductAction $action
    ): ProductDraftsResource {
        return new ProductDraftsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, ProductDraftsQuery $query): ProductDraftsResource
    {
        return new ProductDraftsResource($query->find($id));
    }

    public function search(ProductDraftsQuery $query): AnonymousResourceCollection
    {
        return ProductDraftsResource::collectPage($query->get());
    }

    public function replaceAttributes(
        int $id,
        PatchOrReplaceAttributesRequest $request,
        ReplaceAttributeValuesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributeValuesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function patchAttributes(
        int $id,
        PatchOrReplaceAttributesRequest $request,
        PatchAttributeValuesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributeValuesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function replaceImages(
        int $id,
        PatchOrReplaceImagesRequest $request,
        ReplaceProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }

    public function patchImages(
        int $id,
        PatchOrReplaceImagesRequest $request,
        PatchProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }

    public function preloadImage(PreloadProductImageRequest $request, PreloadProductImageAction $action): PreloadFileResource
    {
        return new PreloadFileResource($action->execute($request->image()));
    }
}
