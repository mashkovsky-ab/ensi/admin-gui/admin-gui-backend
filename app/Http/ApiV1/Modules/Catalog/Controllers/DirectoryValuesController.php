<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Attributes\CreateDirectoryValueAction;
use App\Domain\Catalog\Actions\Attributes\DeleteDirectoryValueAction;
use App\Domain\Catalog\Actions\Attributes\PreloadDirectoryImageAction;
use App\Domain\Catalog\Actions\Attributes\ReplaceDirectoryValueAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Attributes\DirectoryValuesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Attributes\CreateOrReplaceDirectoryValueRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Attributes\PreloadDirectoryImageRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Attributes\DirectoryValuesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\PreloadFileResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DirectoryValuesController
{
    public function create(
        int $propertyId,
        CreateOrReplaceDirectoryValueRequest $request,
        CreateDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($propertyId, $request->validated()));
    }

    public function replace(
        int $directoryValueId,
        CreateOrReplaceDirectoryValueRequest $request,
        ReplaceDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($directoryValueId, $request->validated()));
    }

    public function delete(int $directoryValueId, DeleteDirectoryValueAction $action): EmptyResource
    {
        $action->execute($directoryValueId);

        return new EmptyResource();
    }

    public function search(DirectoryValuesQuery $query): AnonymousResourceCollection
    {
        return DirectoryValuesResource::collectPage($query->get());
    }

    public function get(int $directoryValueId, DirectoryValuesQuery $query): DirectoryValuesResource
    {
        return new DirectoryValuesResource($query->find($directoryValueId));
    }

    public function preloadImage(PreloadDirectoryImageRequest $request, PreloadDirectoryImageAction $action): PreloadFileResource
    {
        return new PreloadFileResource($action->execute($request->image()));
    }
}
