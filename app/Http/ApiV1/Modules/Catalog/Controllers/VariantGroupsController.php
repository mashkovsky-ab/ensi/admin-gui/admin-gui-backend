<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Variants\CreateVariantGroupAction;
use App\Domain\Catalog\Actions\Variants\DeleteVariantGroupAction;
use App\Domain\Catalog\Actions\Variants\ReplaceVariantGroupAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Variants\VariantGroupsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Variants\CreateOrReplaceVariantGroupRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Variants\VariantGroupsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VariantGroupsController
{
    public function create(
        CreateOrReplaceVariantGroupRequest $request,
        CreateVariantGroupAction $action
    ): VariantGroupsResource {
        return new VariantGroupsResource($action->execute($request->validated()));
    }

    public function replace(
        int $groupId,
        CreateOrReplaceVariantGroupRequest $request,
        ReplaceVariantGroupAction $action
    ): VariantGroupsResource {
        return new VariantGroupsResource($action->execute($groupId, $request->validated()));
    }

    public function delete(int $groupId, DeleteVariantGroupAction $action): EmptyResource
    {
        $action->execute($groupId);

        return new EmptyResource();
    }

    public function get(int $groupId, VariantGroupsQuery $query): VariantGroupsResource
    {
        return new VariantGroupsResource($query->find($groupId));
    }

    public function search(VariantGroupsQuery $query): AnonymousResourceCollection
    {
        return VariantGroupsResource::collectPage($query->get());
    }
}
