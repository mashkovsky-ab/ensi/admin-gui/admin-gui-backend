<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Categories\BindCategoryPropertiesAction;
use App\Domain\Catalog\Actions\Categories\CreateCategoryAction;
use App\Domain\Catalog\Actions\Categories\DeleteCategoryAction;
use App\Domain\Catalog\Actions\Categories\ReplaceCategoryAction;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Categories\CategoriesQuery;
use App\Http\ApiV1\Modules\Catalog\Queries\Categories\CategoriesTreeQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Categories\BindCategoryPropertiesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Categories\CreateOrReplaceCategoryRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesTreeResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CategoriesController
{
    public function create(CreateOrReplaceCategoryRequest $request, CreateCategoryAction $action): CategoriesResource
    {
        return new CategoriesResource($action->execute($request->validated()));
    }

    public function replace(
        int $categoryId,
        CreateOrReplaceCategoryRequest $request,
        ReplaceCategoryAction $action
    ): CategoriesResource {
        return new CategoriesResource($action->execute($categoryId, $request->validated()));
    }

    public function delete(int $categoryId, DeleteCategoryAction $action): EmptyResource
    {
        $action->execute($categoryId);

        return new EmptyResource();
    }

    public function search(CategoriesQuery $query): AnonymousResourceCollection
    {
        return CategoriesResource::collectPage($query->get());
    }

    public function get(int $categoryId, CategoriesQuery $query): CategoriesResource
    {
        return new CategoriesResource($query->find($categoryId));
    }

    public function tree(CategoriesTreeQuery $query): AnonymousResourceCollection
    {
        return CategoriesTreeResource::collection($query->get());
    }

    public function bindProperties(
        int $id,
        BindCategoryPropertiesRequest $request,
        BindCategoryPropertiesAction $action
    ): CategoriesResource {
        return new CategoriesResource($action->execute($id, $request->validated()));
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Наименование')->sort()->filterDefault(),
            Field::keyword('code', 'Код'),
            Field::integer('parent_id', 'ID родителя')->filter(),

            Field::boolean('is_active', 'Активна'),
            Field::boolean('is_real_active', 'Фактически активна')->filter(),
            Field::boolean('is_inherits_properties', 'Наследует атрибуты'),

            Field::datetime('created_at', 'Дата создания'),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
