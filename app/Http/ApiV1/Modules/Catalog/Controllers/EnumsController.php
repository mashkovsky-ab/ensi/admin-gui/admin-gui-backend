<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\IntegerEnumResource;
use Ensi\PimClient\Api\EnumsApi;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function productTypes(EnumsApi $api): AnonymousResourceCollection
    {
        return IntegerEnumResource::collection($api->getProductTypes()->getData());
    }
}
