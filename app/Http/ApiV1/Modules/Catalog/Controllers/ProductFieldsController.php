<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Products\PatchProductFieldAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ProductFieldsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchProductFieldRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductFieldsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductFieldsController
{
    public function search(ProductFieldsQuery $query): AnonymousResourceCollection
    {
        return ProductFieldsResource::collectPage($query->get());
    }

    public function get(int $id, ProductFieldsQuery $query): ProductFieldsResource
    {
        return new ProductFieldsResource($query->find($id));
    }

    public function patch(
        int $id,
        PatchProductFieldRequest $request,
        PatchProductFieldAction $action
    ): ProductFieldsResource {
        return new ProductFieldsResource($action->execute($id, $request->validated()));
    }
}
