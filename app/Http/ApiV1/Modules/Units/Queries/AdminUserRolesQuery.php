<?php


namespace App\Http\ApiV1\Modules\Units\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination;
use Ensi\AdminAuthClient\Dto\SearchRolesRequest as SearchAdminUserRolesRequest;
use Illuminate\Http\Request;

class AdminUserRolesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        protected Request $httpRequest,
        protected AdminUsersApi $adminUsersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchAdminUserRolesRequest::class;
    }

    protected function search($request)
    {
        return $this->adminUsersApi->searchRoles($request);
    }
}
