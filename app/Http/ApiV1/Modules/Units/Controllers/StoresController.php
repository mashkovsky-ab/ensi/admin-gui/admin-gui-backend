<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\Stores\CreateStoreAction;
use App\Domain\Units\Actions\Stores\PatchStoreAction;
use App\Domain\Units\Actions\Stores\ReplaceStoreAction;
use App\Http\ApiV1\Modules\Units\Queries\Stores\StoresQuery;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateOrReplaceStoreRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStoreRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoresResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class StoresController
{
    public function search(StoresQuery $query): AnonymousResourceCollection
    {
        return StoresResource::collectPage($query->get());
    }

    public function create(CreateOrReplaceStoreRequest $request, CreateStoreAction $action): StoresResource
    {
        return new StoresResource($action->execute($request->validated()));
    }

    public function get(int $storeId, StoresQuery $query): StoresResource
    {
        return new StoresResource($query->find($storeId));
    }

    public function patch(
        int $storeId,
        PatchStoreRequest $request,
        PatchStoreAction $action
    ): StoresResource {
        return new StoresResource($action->execute($storeId, $request->validated()));
    }

    public function replace(
        int $storeId,
        CreateOrReplaceStoreRequest $request,
        ReplaceStoreAction $action
    ): StoresResource {
        return new StoresResource($action->execute($storeId, $request->validated()));
    }
}
