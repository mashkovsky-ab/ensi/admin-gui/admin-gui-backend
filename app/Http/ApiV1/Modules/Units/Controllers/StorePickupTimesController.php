<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\Stores\CreateStorePickupTimeAction;
use App\Domain\Units\Actions\Stores\PatchStorePickupTimeAction;
use App\Domain\Units\Actions\Stores\ReplaceStorePickupTimeAction;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateOrReplaceStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StorePickupTimesResource;

class StorePickupTimesController
{
    public function create(CreateOrReplaceStorePickupTimeRequest $request, CreateStorePickupTimeAction $action): StorePickupTimesResource
    {
        return new StorePickupTimesResource($action->execute($request->validated()));
    }

    public function patch(
        int $storePickupTimeId,
        PatchStorePickupTimeRequest $request,
        PatchStorePickupTimeAction $action
    ): StorePickupTimesResource {
        return new StorePickupTimesResource($action->execute($storePickupTimeId, $request->validated()));
    }

    public function replace(
        int $storePickupTimeId,
        CreateOrReplaceStorePickupTimeRequest $request,
        ReplaceStorePickupTimeAction $action
    ): StorePickupTimesResource {
        return new StorePickupTimesResource($action->execute($storePickupTimeId, $request->validated()));
    }
}
