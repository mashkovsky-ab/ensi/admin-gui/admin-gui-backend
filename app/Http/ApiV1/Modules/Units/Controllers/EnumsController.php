<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Http\ApiV1\Modules\Units\Resources\SellerStatusesResource;
use Ensi\BuClient\Api\EnumsApi;

class EnumsController
{
    public function sellerStatuses(EnumsApi $enumsApi)
    {
        return SellerStatusesResource::collection($enumsApi->getSellerStatuses()->getData());
    }
}
