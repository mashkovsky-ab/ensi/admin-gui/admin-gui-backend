<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Http\ApiV1\Modules\Units\Queries\SellerUsers\SellerUserRolesQuery;
use App\Http\ApiV1\Modules\Units\Resources\AdminUserRolesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SellerUserRolesController
{
    public function search(SellerUserRolesQuery $query): AnonymousResourceCollection
    {
        return AdminUserRolesResource::collectPage($query->get());
    }
}
