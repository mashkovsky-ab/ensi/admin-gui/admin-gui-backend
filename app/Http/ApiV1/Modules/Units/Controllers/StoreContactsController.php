<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\Stores\CreateStoreContactAction;
use App\Domain\Units\Actions\Stores\DeleteStoreContactAction;
use App\Domain\Units\Actions\Stores\PatchStoreContactAction;
use App\Domain\Units\Actions\Stores\ReplaceStoreContactAction;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateOrReplaceStoreContactRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStoreContactRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoreContactsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class StoreContactsController
{
    public function create(
        CreateOrReplaceStoreContactRequest $request,
        CreateStoreContactAction $action
    ): StoreContactsResource {
        return new StoreContactsResource($action->execute($request->validated()));
    }

    public function patch(
        int $storeContactId,
        PatchStoreContactRequest $request,
        PatchStoreContactAction $action
    ): StoreContactsResource {
        return new StoreContactsResource($action->execute($storeContactId, $request->validated()));
    }

    public function replace(
        int $storeContactId,
        CreateOrReplaceStoreContactRequest $request,
        ReplaceStoreContactAction $action
    ): StoreContactsResource {
        return new StoreContactsResource($action->execute($storeContactId, $request->validated()));
    }

    public function delete(int $storeContactId, DeleteStoreContactAction $action): EmptyResource
    {
        $action->execute($storeContactId);

        return new EmptyResource();
    }
}
