<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\Sellers\CreateSellerAction;
use App\Domain\Units\Actions\Sellers\PatchSellerAction;
use App\Domain\Units\Actions\Sellers\ReplaceSellerAction;
use App\Http\ApiV1\Modules\Units\Queries\Sellers\SellersQuery;
use App\Http\ApiV1\Modules\Units\Requests\Sellers\CreateOrReplaceSellerRequest;
use App\Http\ApiV1\Modules\Units\Requests\Sellers\PatchSellerRequest;
use App\Http\ApiV1\Modules\Units\Resources\SellersResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SellersController
{
    public function search(SellersQuery $query): AnonymousResourceCollection
    {
        return SellersResource::collectPage($query->get());
    }

    public function create(CreateOrReplaceSellerRequest $request, CreateSellerAction $action): SellersResource
    {
        return new SellersResource($action->execute($request->validated()));
    }

    public function get(int $sellerId, SellersQuery $query): SellersResource
    {
        return new SellersResource($query->find($sellerId));
    }

    public function patch(
        int $sellerId,
        PatchSellerRequest $request,
        PatchSellerAction $action
    ): SellersResource {
        return new SellersResource($action->execute($sellerId, $request->validated()));
    }

    public function replace(
        int $sellerId,
        CreateOrReplaceSellerRequest $request,
        ReplaceSellerAction $action
    ): SellersResource {
        return new SellersResource($action->execute($sellerId, $request->validated()));
    }
}
