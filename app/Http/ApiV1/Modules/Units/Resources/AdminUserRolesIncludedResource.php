<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\AdminAuthClient\Dto\UserRole;

/**
 * @mixin UserRole
 */
class AdminUserRolesIncludedResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            "id" => $this->getId(),
            "title" => $this->getTitle(),
            "expires" => $this->dateTimeToIso($this->getExpires()),
        ];
    }
}
