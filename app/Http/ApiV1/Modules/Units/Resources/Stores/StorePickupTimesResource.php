<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Stores;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BuClient\Dto\StorePickupTime;

/** @mixin StorePickupTime */
class StorePickupTimesResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'store_id' => $this->getStoreId(),
            'day' => $this->getDay(),
            'pickup_time_code' => $this->getPickupTimeCode(),
            'pickup_time_start' => $this->getPickupTimeStart(),
            'pickup_time_end' => $this->getPickupTimeEnd(),
            'cargo_export_time' => $this->getCargoExportTime(),
            'delivery_service' => $this->getDeliveryService(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }
}
