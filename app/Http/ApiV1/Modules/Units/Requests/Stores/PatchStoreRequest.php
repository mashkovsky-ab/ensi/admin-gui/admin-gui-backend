<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStoreRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'seller_id' => ['sometimes', 'required', 'integer'],
            'xml_id' => ['nullable', 'string'],
            'active' => ['nullable', 'boolean'],
            'name' => ['sometimes', 'required', 'string'],
            'address' => ['nullable', 'array'],
            'address.address_string' => ['sometimes', 'required', 'string'],
            'address.country_code' => ['sometimes', 'required', 'string'],
            'address.post_index' => ['sometimes', 'required', 'string'],
            'address.region' => ['sometimes', 'required', 'string'],
            'address.region_guid' => ['sometimes', 'required', 'string'],
            'address.city' => ['sometimes', 'required', 'string'],
            'address.city_guid' => ['sometimes', 'required', 'string'],
            'address.street' => ['nullable', 'string'],
            'address.house' => ['nullable', 'string'],
            'address.block' => ['nullable', 'string'],
            'address.flat' => ['nullable', 'string'],
            'address.porch' => ['nullable', 'string'],
            'address.intercom' => ['nullable', 'string'],
            'address.floor' => ['nullable', 'string'],
            'address.comment' => ['nullable', 'string'],
            'address.geo_lat' => ['sometimes', 'required', 'string'],
            'address.geo_lon' => ['sometimes', 'required', 'string'],
            'timezone' => ['nullable', 'string'],
        ];
    }
}
