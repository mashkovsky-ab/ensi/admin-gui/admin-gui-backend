<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceStoreRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'seller_id' => ['required', 'integer'],
            'xml_id' => ['nullable', 'string'],
            'active' => ['nullable', 'boolean'],
            'name' => ['required', 'string'],
            'address' => ['nullable', 'array'],
            'address.address_string' => ['required', 'string'],
            'address.country_code' => ['required', 'string'],
            'address.post_index' => ['required', 'string'],
            'address.region' => ['required', 'string'],
            'address.region_guid' => ['required', 'string'],
            'address.city' => ['required', 'string'],
            'address.city_guid' => ['required', 'string'],
            'address.street' => ['nullable', 'string'],
            'address.house' => ['nullable', 'string'],
            'address.block' => ['nullable', 'string'],
            'address.flat' => ['nullable', 'string'],
            'address.porch' => ['nullable', 'string'],
            'address.intercom' => ['nullable', 'string'],
            'address.floor' => ['nullable', 'string'],
            'address.comment' => ['nullable', 'string'],
            'address.geo_lat' => ['required', 'string'],
            'address.geo_lon' => ['required', 'string'],
            'timezone' => ['nullable', 'string'],
        ];
    }
}
