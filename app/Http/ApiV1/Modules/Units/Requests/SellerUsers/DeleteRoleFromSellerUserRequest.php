<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteRoleFromSellerUserRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'role_id' => ['required', 'integer'],
        ];
    }
}
