<?php

namespace App\Http\ApiV1\Modules\Units\Requests\AdminUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchAdminUsersRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => [],
            'active' => ['boolean'],
            'cause_deactivation' => ['required_if:active,false'],
            'password' => ['nullable'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'middle_name' => ['nullable'],
            'phone' => ['regex:/^\+7\d{10}$/'],
            'email' => ['email'],
            'timezone' => ['sometimes', 'required', 'timezone'],
        ];
    }
}
