<?php

use App\Domain\Units\Tests\Factories\AdminUserFactory;
use App\Domain\Units\Tests\Factories\AdminUserRoleFactory;
use App\Http\ApiV1\Modules\Units\Tests\AdminUsers\Factories\AdminUserRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\PaginationFactory;
use Ensi\AdminAuthClient\Dto\RoleEnum;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/units/admin-users:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockAdminAuthUsersApi()->allows([
        'searchUsers' => $usersSearchResponse = AdminUserFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/admin-users:search", ['pagination' => PaginationFactory::new()->makeOffsetRequest()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $usersSearchResponse->getData()[0]->getId());
});

test('POST /api/v1/units/admin-user-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->allows([
        'searchUsers' => AdminUserFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/admin-user-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);

test('POST /api/v1/units/admin-users 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userData = AdminUserRequestFactory::new()->make();
    $this->mockAdminAuthUsersApi()->allows([
        'createUser' => AdminUserFactory::new()->makeResponseOne($userData),
    ]);

    postJson("/api/v1/units/admin-users", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.email', $userData['email']);
});

test('GET /api/v1/units/admin-users/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userData = AdminUserRequestFactory::new()->make();
    $userData['id'] = 1;
    $this->mockAdminAuthUsersApi()->allows([
        'getUser' => AdminUserFactory::new()->makeResponseOne($userData),
    ]);

    getJson("/api/v1/units/admin-users/{$userData['id']}")
        ->assertStatus(200)
        ->assertJsonPath('data.email', $userData['email'])
        ->assertJsonPath('data.id', $userData['id']);
});

test('PATCH /api/v1/units/admin-users/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $userData = AdminUserRequestFactory::new()->make(['active' => true]);
    $userData['id'] = 1;
    $this->mockAdminAuthUsersApi()->allows([
        'patchUser' => AdminUserFactory::new()->makeResponseOne($userData),
    ]);

    patchJson("/api/v1/units/admin-users/{$userData['id']}", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.email', $userData['email'])
        ->assertJsonPath('data.id', $userData['id']);
});

test('POST /api/v1/units/admin-users/{id}:add-roles 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('addRolesToUser');

    $addRolesData = [
        'roles' => [RoleEnum::ADMIN],
    ];

    postJson("/api/v1/units/admin-users/1:add-roles", $addRolesData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/{id}:delete-role 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('deleteRoleFromUser');

    $deleteRoleData = [
        'role_id' => RoleEnum::ADMIN,
    ];

    postJson("/api/v1/units/admin-users/1:delete-role", $deleteRoleData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/{id}:refresh-password-token 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('refreshPasswordToken');

    postJson("/api/v1/units/admin-users/1:refresh-password-token")
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/mass/change-active mass activate 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('massChangeActive');

    $massChangeActiveData = [
        'user_ids' => [1, 2, 3],
        'active' => true,
    ];

    postJson("/api/v1/units/admin-users/mass/change-active", $massChangeActiveData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/mass/change-active mass deactivate 400', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('massChangeActive');

    $massChangeActiveData = [
        'user_ids' => [1, 2, 3],
        'active' => false,
    ];

    postJson("/api/v1/units/admin-users/mass/change-active", $massChangeActiveData)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('POST /api/v1/units/admin-users/mass/change-active mass deactivate 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->shouldReceive('massChangeActive');

    $massChangeActiveData = [
        'user_ids' => [1, 2, 3],
        'active' => false,
        'cause_deactivation' => 'Cause Deactivation',
    ];

    postJson("/api/v1/units/admin-users/mass/change-active", $massChangeActiveData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-users/set-password 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockAdminAuthUsersApi()->allows([
        'patchUser' => $userResponse = AdminUserFactory::new()->makeResponseOne(),
        'searchUsers' => AdminUserFactory::new()->makeResponseSearch(['id' => $userResponse->getData()->getId()]),
    ]);

    $setPasswordData = [
        'token' => 'token',
        'password' => 'new password',
    ];

    postJson("/api/v1/units/admin-users/set-password", $setPasswordData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/admin-user-roles:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockAdminAuthUsersApi()->allows([
        'searchRoles' => $rolesSearchResponse = AdminUserRoleFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/admin-user-roles:search", ['pagination' => PaginationFactory::new()->makeOffsetRequest()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $rolesSearchResponse->getData()[0]->getId());
});
