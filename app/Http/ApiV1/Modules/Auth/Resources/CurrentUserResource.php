<?php


namespace App\Http\ApiV1\Modules\Auth\Resources;

use App\Domain\Auth\Models\User;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin User
 */
class CurrentUserResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            "id" => $this->getId(),
            "full_name" => $this->getFullName(),
            "short_name" => $this->getShortName(),
            "active" => $this->isActive(),
            "login" => $this->getLogin(),
            "last_name" => $this->getLastName(),
            "first_name" => $this->getFirstName(),
            "middle_name" => $this->getMiddleName(),
            "email" => $this->getEmail(),
            "phone" => $this->getPhone(),
            "roles" => $this->getRoles(),
        ];
    }
}
