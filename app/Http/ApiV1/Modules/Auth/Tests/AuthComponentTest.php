<?php

use App\Domain\Auth\Models\Tests\Factories\TokenFactory;
use App\Domain\Units\Tests\Factories\AdminUserFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\AdminAuthClient\Configuration;
use Lcobucci\JWT\Configuration as JWTConfiguration;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/auth/login 400', function () {
    postJson('/api/v1/auth/login')
        ->assertStatus(400);
});

test('POST /api/v1/auth/refresh 400', function () {
    postJson('/api/v1/auth/refresh')
        ->assertStatus(400);
});

test('GET /api/v1/auth/current-user 200', function () {
    getJson('/api/v1/auth/current-user')
        ->assertStatus(200);
});

test('POST /api/v1/auth/login 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockOauthApi()->allows([
        'createToken' => $token = TokenFactory::new()->make(),
    ]);
    $requestData = [
        'login' => 'test_login',
        'password' => 'test_password',
    ];

    postJson("/api/v1/auth/login", $requestData)
        ->assertStatus(200)
        ->assertJsonPath('data.access_token', $token['access_token']);
});

test('POST /api/v1/auth/logout 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $configuration = JWTConfiguration::forUnsecuredSigner();
    $accessToken = $configuration->builder()
        ->identifiedBy('4f1g23a12aa')
        ->getToken($configuration->signer(), $configuration->signingKey())
        ->toString();

    $this->mockAdminAuthUsersApi()->allows([
        'getCurrentUser' => AdminUserFactory::new()->makeResponseOne(),
        'getConfig' => resolve(Configuration::class),
    ])->shouldReceive('setAccessToken');

    $this->mockOauthApi()->allows([
        'getConfig' => resolve(Configuration::class),
    ])->shouldReceive('deleteToken');

    getJson("/api/v1/auth/logout", ['Authorization' => 'Bearer ' . $accessToken])
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/auth/refresh 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $configuration = JWTConfiguration::forUnsecuredSigner();

    $this->mockOauthApi()->allows([
        'createToken' => $newToken = TokenFactory::new()->make(),
    ]);

    $requestData = [
        'refresh_token' => $configuration->builder()
            ->identifiedBy('4f1g23a12aa')
            ->getToken($configuration->signer(), $configuration->signingKey())
            ->toString(),
    ];

    postJson("/api/v1/auth/refresh", $requestData)
        ->assertStatus(200)
        ->assertJsonPath('data.access_token', $newToken['access_token']);
});

test('POST /api/v1/auth/current-user 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $configuration = JWTConfiguration::forUnsecuredSigner();
    $accessToken = $configuration->builder()
        ->identifiedBy('4f1g23a12aa')
        ->getToken($configuration->signer(), $configuration->signingKey())
        ->toString();

    $this->mockOauthApi()->allows([
        'getConfig' => resolve(Configuration::class),
    ]);

    $this->mockAdminAuthUsersApi()->allows([
        'getCurrentUser' => $userResponse = AdminUserFactory::new()->makeResponseOne(),
        'getConfig' => resolve(Configuration::class),
    ])->shouldReceive('setAccessToken');

    getJson("/api/v1/auth/current-user", ['Authorization' => 'Bearer ' . $accessToken])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $userResponse->getData()->getId());
});
