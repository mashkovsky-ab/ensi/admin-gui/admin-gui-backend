<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

class LogisticDeliveryMethodEnum
{
    public const DELIVERY = 1;
    public const PICKUP = 2;

    /**
     * @return int[]
     */
    public static function cases(): array
    {
        return [
            self::DELIVERY,
            self::PICKUP,
        ];
    }
}
