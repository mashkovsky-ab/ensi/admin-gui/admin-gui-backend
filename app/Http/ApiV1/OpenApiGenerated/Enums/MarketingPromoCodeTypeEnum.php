<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

class MarketingPromoCodeTypeEnum
{
    public const TYPE_DISCOUNT = 1;
    public const TYPE_DELIVERY = 2;
    public const TYPE_GIFT = 3;
    public const TYPE_BONUS = 4;

    /**
     * @return int[]
     */
    public static function cases(): array
    {
        return [
            self::TYPE_DISCOUNT,
            self::TYPE_DELIVERY,
            self::TYPE_GIFT,
            self::TYPE_BONUS,
        ];
    }
}
